angular.module('ui.directives')
    .directive("cmdEnter", function() {
        function b(a, b, c, d) {
            function e() {
                for (var a = b.parent(); a.length && "FORM" !== a[0].tagName;) a = a.parent();
                return a }

            function f(a) { 13 === a.which && a.metaKey && (a.preventDefault(), c.bnCmdEnter ? g(a) : h()) }

            function g(b) { a.$apply(function() { a.$eval(c.bnCmdEnter, { $event: b }) }) }

            function h() { e().triggerHandler("submit") }(c.bnCmdEnter || d) && b.on("keydown", f) }
        return { link: b, require: "^?form", restrict: "A" } });
