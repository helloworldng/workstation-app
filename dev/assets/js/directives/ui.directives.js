angular.module('ui.directives', [])
    .directive('badge', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<span class="badge text-white text-capitalize" ng-class="label_class">{{status}}</span>',
            link: function($scope, element, attrs, controller) {
                $scope.status = attrs["status"] || "Unknown";
                $scope.label_class = attrs["class"] || "";
                switch ($scope.status) {
                    case "pending":
                        $scope.label_class += " btn-info"
                        break;
                    case "unpaid":
                        $scope.label_class += " btn-info"
                        break;
                    case "approved":
                        $scope.label_class += " btn-success"
                        break;
                    case "paid":
                        $scope.label_class += " btn-success"
                        break;
                    case "failed":
                        $scope.label_class += " btn-danger"
                        break;
                    case "active":
                        $scope.label_class += " btn-success"
                        break;
                    case "inactive":
                        $scope.label_class += " btn-danger"
                        break;
                    case "expired":
                        $scope.label_class += " btn-danger"
                        break;
                    default:
                        $scope.label_class += " btn-info"
                        break;
                }
            }
        };
    }).directive('status', function() {
        return {
            restrict: 'A',
            link: function($scope, element, attrs, controller) {
                var status = attrs["status"] ? attrs["status"].toLowerCase() : "Unknown";

                var success_states = ['verified', 'paid', 'active', 'success', 'subscribed', 'approved', 'resolved', 'live', 'available'];
                var failure_states = ['failed', 'unpaid', 'inactive', 'unverified', 'unsubscribed', 'expired', 'archived', 'pending', 'reversed', 'busy'];
                var test_states = ['test']

                if (_.contains(success_states, status)) {
                    element.addClass("text-success");
                } else if (_.contains(failure_states, status)) {
                    element.addClass("text-danger");
                } else if (_.contains(test_states, status)) {
                    element.addClass("text-muted");
                } else {
                    element.addClass("text-info");
                }
            }
        };
    }).directive('ngLoading', [
        function() {
            //directive to show loading state
            return {
                restrict: 'AE',
                scope: true,
                compile: function(tElem, attrs) {
                    //Add the controls to element
                    tElem.addClass('loading-button');
                    var buttonContent = tElem.html();
                    tElem.html("<span class=\"default-state\">" + buttonContent + "</span>");
                    tElem.append("<div class=\"loading-state spinner\">\r\n  <div class=\"b1 se\"><\/div>\r\n  <div class=\"b2 se\"><\/div>\r\n  <div class=\"b3 se\"><\/div>\r\n  <div class=\"b4 se\"><\/div>\r\n  <div class=\"b5 se\"><\/div>\r\n  <div class=\"b6 se\"><\/div>\r\n  <div class=\"b7 se\"><\/div>\r\n  <div class=\"b8 se\"><\/div>\r\n  <div class=\"b9 se\"><\/div>\r\n  <div class=\"b10 se\"><\/div>\r\n  <div class=\"b11 se\"><\/div>\r\n  <div class=\"b12 se\"><\/div>\r\n<\/div><span class=\"loading-success\"><i class=\"fa fa-check animated fadeInUp\"><\/i><\/span><span class=\"loading-failure\"><i class=\"fa fa-times animated fadeInUp\"><\/i><\/span>");
                    return function(scope, element, attrs) {
                        var watching;

                        var showLoading = function(val) {
                            element.addClass('ng-loading');
                            element.attr('disabled', true);
                            watching = true;
                        }

                        var clearLoading = function() {
                            element.removeClass('ng-loading-success ng-loading-failure ng-loading');
                            element.attr('disabled', false);
                        }

                        scope.$on(attrs.ngLoading, function(event, val) {
                            if (!watching) return;
                            watching = false;
                            element.removeClass('ng-loading');

                            if (val) {
                                if (val === true) element.addClass('ng-loading-success')
                                if (val === false) element.addClass('ng-loading-failure');
                                setTimeout(clearLoading, 700);
                            } else {
                                clearLoading();
                            }
                        });

                        element.on('click', function() {
                            element.addClass('ng-loading');
                            showLoading();
                        })
                    };
                }
            };
        }
    ])
    .directive('autofocus', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        }
    }])
    .directive("fileread", [function() {
        return {
            link: function(scope, element, attrs) {
                element.bind('change', function(event) {
                    scope.$apply(function() {
                        scope[attrs.fileread] = event.target.files[0];
                    });
                });

                scope.$watch(attrs.fileread, function(fileread) {
                    if (!fileread) element.val("");
                });
            }
        }
    }])
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('triggerClick', function() {
        return function(scope, element, attrs) {
            element.bind("click", function(event) {
                var id = attrs['triggerClick'];
                var input = document.getElementById(id);
                input.click();
            });
        };
    })
    .directive('closeOutsideClick', function($window) {
        return function(scope, element, attrs) {
            var trigger = attrs['closeOutsideClick'];

            var handler = function(event) {
                if (!div.contains(event.target)) {
                    scope.$apply(function() {
                        scope[trigger] = false;
                    });

                    document.removeEventListener('click', handler, false);
                }
            };

            var div = angular.element(element)[0];
            scope.$watch(trigger, function(visible) {
                if (visible) document.addEventListener('click', handler, false);
            })
        };
    })
    .directive("whenScrolled", function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                var didScroll = false;
                window.onscroll = function() {
                    didScroll = true;
                }

                setInterval(function() {
                    if (didScroll) {
                        didScroll = false;
                        var bottomOfPage = (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight;
                        if (bottomOfPage) {
                            scope.$apply(attrs.whenScrolled);
                        }
                    }
                }, 100);
            }
        }
    });
