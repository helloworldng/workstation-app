angular.module('app.notificationService', ['toaster'])
    .factory('Notification', ['toaster', function(toaster) {
        return {
            pop: function(options) {
                toaster.pop(options);
            },
            success: function(title, body) {
                toaster.pop({
                    type: 'success',
                    title: title || "Action completed succesfully",
                    body: body
                });
            },
            error: function(title, error) {
                var message;

                if (typeof error == "string") {
                    message = error;
                }

                console.log(error)

                if (typeof error == "object") {
                    if (error.data) {
                        message = error.data.errors;
                        if (message && message.hasOwnProperty('full_messages')) message = message.full_messages;
                    } else message = error.errors;

                    title = error.reason || title;
                }

                if (Array.isArray(message)) {
                    message = message[0];
                }
                
                toaster.pop({
                    type: 'error',
                    title: title || "Action could not be perfomed",
                    body: message  || "Unknown error occured"
                });
            },
            clear: function() {
                toaster.clear();
            }
        }
    }]);
