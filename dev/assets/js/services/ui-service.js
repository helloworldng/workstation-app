angular.module('app.uiService', ['restangular'])
    .factory('CalendarHelper', [function() {
        return {
            renderTooltip: function(jsEvent) {
                var overlay = $('.fc-overlay');
                overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');

                var wrap = $(jsEvent.target).closest('.fc-event');
                var cal = wrap.closest('.ui-calendar');
                var left = wrap.offset().left - cal.offset().left;
                var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
                var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
                if (right > overlay.width()) {
                    overlay.addClass('left').find('.arrow').addClass('left pull-up')
                } else if (left > overlay.width()) {
                    overlay.addClass('right').find('.arrow').addClass('right pull-up');
                } else {
                    overlay.find('.arrow').addClass('top');
                }
                if (top < overlay.height()) {
                    overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down')
                }
                (wrap.find('.fc-overlay').length == 0) && wrap.append(overlay);
            }
        }
    }])
    .factory('DatepickerHelper', [function() {
        return {
            triggerClick: function() {
                var body = document.getElementsByTagName("body")[0];
                body.click();
            }
        }
    }])
    .service('FileHelper', ['$http', function($http) {
        this.upload = function(file, preset) {
            var cloud_name = 'dcgsxfgaq';
            var upload_preset = preset || 'fileUpload';

            var fd = new FormData();
            fd.append('file', file);
            fd.append('upload_preset', upload_preset);

            return $http.post('https://api.cloudinary.com/v1_1/' + cloud_name + '/upload', fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        'X-Requested-With': 'XMLHttpRequest',
                        'If-Modified-Since': undefined
                    }
                })
        }
    }])
