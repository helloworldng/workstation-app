angular.module('app.apiFactories', ['restangular'])

.factory('$LocalAPI', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl('/api');
    });
}])


.factory('$API', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(DEFAULTS.baseAPIUrl);
    });
}])

.factory('$FullAPI', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(DEFAULTS.baseAPIUrl);
        RestangularConfigurer.setFullResponse(true);
    });
}]);
