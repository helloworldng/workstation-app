'use strict';

angular.module('app', [
        'angucomplete-alt',
        'angular-loading-bar',
        'currencyMask',
        'ellipsis',
        'ngAnimate',
        'ngDialog',
        'ngSanitize',
        'ngStorage',
        'ng-token-auth',
        'oc.lazyLoad',
        'restangular',
        'scrollglue',
        'toaster',
        'ui.bootstrap',
        'ui.paging',
        'ui.directives',
        'ui.controllers',
        'ui.calendar',
        'ui.gravatar',
        'ui.router',
        'ui.bootstrap.datetimepicker',
        'vkEmojiPicker',
        'app.localService',
        'app.historyService',
        'app.notificationService',
        'app.apiFactories',
        'app.authService',
        'app.uiService',
        'app.access',
        'app.admin',
        'app.user',
        'app.common'
    ])
    .constant('DEFAULTS', {
        'config': settings.config,
        'home': settings.config + ".home",
        'baseAPIUrl': settings.baseAPIUrl,
        'clientToken': settings.clientToken,
        'paystackKey': settings.paystackKey
    })
    .config(['$urlRouterProvider', '$httpProvider', 'RestangularProvider', '$locationProvider', '$stateProvider', 'ngDialogProvider', 'gravatarServiceProvider', 'DEFAULTS', '$authProvider', '$provide',
        function($urlRouterProvider, $httpProvider, RestangularProvider, $locationProvider, $stateProvider, ngDialogProvider, gravatarServiceProvider, DEFAULTS, $authProvider, $provide) {

            $urlRouterProvider
                .when('', ['$state', function($state) {
                    $state.go('access.login');
                }]);

            $urlRouterProvider.otherwise('/login');

            ngDialogProvider.setDefaults({
                className: 'ngdialog-theme-plain',
                showClose: false,
            });

            gravatarServiceProvider.defaults = {
                size: 100,
                "default": 'mm'
            };

            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                if (data && data.response) {
                    var returnedData = data.response.data;
                    if (data.response.meta) returnedData.meta = data.response.meta;
                    return returnedData;
                } else {
                    return data;
                };
            });

            $authProvider.configure({
                apiUrl: DEFAULTS.baseAPIUrl,
                confirmationSuccessUrl: window.location.protocol + "//" + window.location.hostname,
                passwordResetSuccessUrl: window.location.protocol + "//" + window.location.hostname
            });

            $httpProvider.interceptors.push('BearerAuthInterceptor');

            // Cache busting
            var cacheBuster = Date.now().toString();

            function templateFactoryDecorator($delegate) {
                var fromUrl = angular.bind($delegate, $delegate.fromUrl);
                $delegate.fromUrl = function(url, params) {
                    if (url !== null && angular.isDefined(url) && angular.isString(url)) {
                        url += (url.indexOf("?") === -1 ? "?" : "&");
                        url += "v=" + cacheBuster;
                    }

                    return fromUrl(url, params);
                };

                return $delegate;
            }

            $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
        }
    ])
    .run(['$rootScope', '$location', '$API', '$state', '$stateParams', 'DEFAULTS', 'LocalService', 'ngDialog', 'Session', 'Notification',
        function($rootScope, $location, $API, $state, $stateParams, DEFAULTS, LocalService, ngDialog, Session, Notification) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.Date = Date;
            $rootScope.DEFAULTS = DEFAULTS;
            $rootScope.ui = {};

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                // If it's a parent state, redirect to it's child
                if (toState.redirectTo) {
                    event.preventDefault();
                    var params = _.extend(toParams, $location.search());
                    $state.go(toState.redirectTo, params);
                    return;
                }

                if (DEFAULTS.config === "admin" && toState.access != "admin" && toState.access != "public") {
                    event.preventDefault();
                    $state.go("admin.home");
                    return;
                }

                if (DEFAULTS.config === "user" && toState.access == "admin") {
                    event.preventDefault();
                    $state.go("user.home");
                    return;
                }
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.$state.activeParams = _.some(_.values($stateParams));
                $rootScope.ui.fullHeight = toState.fullHeight;
                $rootScope.ui.skeleton = toState.skeleton;
                $rootScope.$title = toState.title;
                window.scrollTo(0, 0);
            })

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                console.log(error);
            })

            $rootScope.$on('auth:validation-success', function(ev, user) {
                Session.create(user)
                    .then(function(response) {
                        $rootScope.User = response;
                        if (DEFAULTS.config === "user") {
                            $rootScope.$broadcast('auth:validate-subscription', response);
                        }
                    });
            });

            $rootScope.$on('auth:login-success', function(ev, user) {
                Session.create(user)
                    .then(function(response) {
                        $rootScope.User = response;
                        if (DEFAULTS.config === "user") {
                            $rootScope.$broadcast('auth:validate-subscription', response);
                        }
                    })
            });

            $rootScope.$on('auth:email-confirmation-success', function(ev, user) {
                Notification.success('Verification Complete', 'Your account has now been verified');
                $state.reload();
            });

            $rootScope.$on('auth:password-reset-confirm-success', function() {
                ngDialog.openConfirm({
                    templateUrl: '/modules/access/reset-password.html',
                    controller: "LoginController",
                    appendClassName: 'ngdialog-darker',
                    closeByEscape: false,
                    closeByNavigation: false,
                    closeByDocument: false
                });
            });

            $rootScope.checkUnread = function() {
                $API.all("messaging/unread").getList().then(function(response) {
                    $rootScope.unread = response.plain();
                });
            }
        }
    ])
    .controller('HeaderController', ['$scope', '$state', function($scope, $state) {
        $scope.goTo = function(state, params) {
            $state.go(state, params, {
                reload: true,
                inherit: true,
                notify: true
            });
            $scope.query = null;
        }
    }])
    .controller('AppController', ['$scope', '$localStorage', '$window', '$state', '$stateParams', '$rootScope', '$auth',
        function($scope, $localStorage, $window, $state, $stateParams, $rootScope, $auth) {

            // Config
            $scope.app = {
                name: 'Workstation',
                version: '1.0.0'
            }

            // Filter function
            $scope.clearFilters = function() {
                $state.go($state.current, {}, {
                    reload: true,
                    inherit: false
                })
            }

            $scope.filter = function(attribute, value, options) {
                var options = options || {};
                var params = _.clone($stateParams) || {};
                if (attribute && value) {
                    if (options.date) {
                        value = new Date(value).toISOString();
                    }
                    params[attribute] = value;
                }
                if (attribute && !value) params[attribute] = undefined;
                if (params.page) params = _.omit(params, 'page');
                $state.go($state.current, params, {
                    reload: true,
                    inherit: true,
                    notify: true
                })
            }

            $scope.goToPage = function(page) {
                var params = _.clone($stateParams) || {};
                params.page = page;
                $state.go($state.current, params, {
                    reload: true,
                    inherit: true,
                    notify: true
                })
            }

            $rootScope.getThumbnail = function(url) {
                if (!url) return;
                // var index = url.indexOf("upload/") + 7;
                // var thumb = url.slice(0, index) + 'g_face,c_thumb,w_200,h_200/' + url.slice(index + Math.abs(0));
                // thumb = thumb.replace(/^http:\/\//i, 'https://');
                return url.replace(/^http:\/\//i, 'https://');
            }

            //Logout
            $scope.logout = function() {
                $auth.signOut()
                    .then(function(resp) {
                        $state.go("access.login");
                    })
                    .catch(function(resp) {
                        // handle error response
                    });
            }

            // Graph Options
            var fontFamily = '"proxima_nova", Helvetica, Arial, sans-serif';
            $scope.graphOptions = {
                scaleBeginAtZero: true,
                scaleShowVerticalLines: false,
                scaleShowLabels: false,
                responsive: true,
                maintainAspectRatio: false,
                bezierCurve: false,
                tooltipFontFamily: fontFamily,
                scaleFontFamily: fontFamily,
                pointDotRadius: 5,
                pointDotStrokeWidth: 2,
                colours: ['#2cc36b', '#E74C3C'],
                multiTooltipTemplate: function(label) {
                    return label.datasetLabel + ': ' + "N" + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                tooltipTitleFontFamily: fontFamily,
                tooltipTitleFontStyle: "normal",
                tooltipCornerRadius: 3,
                tooltipXPadding: 10,
                tooltipYPadding: 10,
            }


            // Override Primitives
            String.prototype.trunc =
                function(n) {
                    return this.substr(0, n - 1) + (this.length > n ? '…' : '');
                };

            String.prototype.capitalizeFirstLetter = function() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            }
        }
    ])
    .factory('BearerAuthInterceptor', ['$window', 'DEFAULTS', function($window, DEFAULTS) {
        return {
            request: function(config, data) {
                config.headers = config.headers || {};
                if (config.url.indexOf("workstation") > -1) {
                    config.headers.client_token = DEFAULTS.clientToken;
                }
                return config;
            }
        };
    }]);
