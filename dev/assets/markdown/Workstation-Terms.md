Workstation Terms

1. **DEFINITIONS**
  In this Terms of Use, unless the context otherwise requires, the following expressions shall have the following meanings:
  
  **Company** means Workstation International Co-Work Limited;
  
  **Member** means any individual or company who has signed up to a Membership Plan, paid the membership fees and has agreed to this Terms of Use;
  
  **Membership Plan** means the membership plans as detailed in the Products Brochure;
  
  **Products Brochure** mean the brochure detailing the different Membership Plans and the Services offered by the Company under each Membership Plan;
  
  **Services** means the provision of a designated workspace and other facilities, for the use of a Member in accordance with each Membership Plan;
  
  **Terms of Use T&Cs** mean the terms and conditions of use as set out in this Terms of Use.
  
2. **MEMBERSHIP**
  1. Your membership shall be subject to this Terms of Use. 
  2. You hereby agree to adhere to the restrictions on time and facilities which shall vary, depending on your selected Membership Plan.
  3. The Company reserves the right to update or vary this T&Cs at any time. 
    
3. **DESCRIPTION OF SERVICES**
  1. The Company shall provide the Services in accordance with your selected Membership Plan. The Services at all times are subject to this Terms of Use. 
  2. Members on flexible Membership Plans are not guaranteed availability of a designated workspace at all times. 
  3. The facilities provided by the Company shall only be utilized for office use. A violation of this provision entitles the Company to an immediate termination of your membership.
    
4. **REPRESENTATION & WARRANTIES**
  1. You hereby represent and warrant that you have all requisite legal power and authority to enter into and abide by this T&Cs and no further authorisation or approval is necessary.
  2. You further represent and warrant that your participation or use of the Services will not conflict with or result in any breach of any license, contract, agreement or other instrument or obligation to which you are a party. 
    
5. **USE OF SERVICES**
  1. You agree that your membership does not create a tenancy, leasehold or other real property interest in your favour with respect to the premises, but a prepaid license to use the provided Services in accordance with your Membership Plan.
  2. You hereby agree as follows with respect to the Services:
    1. Access cards and other such items used to gain physical access to the premises remain the property of the Company. 
    2. You hereby agree to safeguard the Company’s properties at all times. In the event that any property of the Company in your custody is stolen, lost or damaged, you will be liable to pay the replacement fees as may be determined by the Company from time to time for such properties.
    3. You are responsible for removing all your personal effects from the premises after use.
    4. You shall promptly notify the Company of any change in your contact and payment information.
    5. The Company may disclose information about you as may be necessary to satisfy any applicable law, rule, regulation, legal process or government request or if it deems it  reasonably necessary for the protection of the Company and other Members.
    6. You shall not attach or affix any items to the walls or make any other alterations to the facilities provided or install antennas, telecommunication lines or any device/ equipment in the premises or bring any additional furniture into the premises, in each case without prior consultation and approval by the Company.
    7. You shall not sublet your membership to any third party or grant any unauthorized entry into the premises using your access card or other such items used to gain physical access to the Premises.
  3. You further agree that when using the Services, you will not: 
    1. Use the Services for the advancement of any illegal purpose
    2. Violate any applicable law, regulations, code of conduct or other guidelines
    3. Use any material or information, which are made available through the Services in any manner that infringes any copyright, trademark, patent, trade secret, or other proprietary right of any party.
    4. Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, obscene, indecent or unlawful material or information on or through the Company’s servers.
    5. Use the Services in any manner that could damage, disable, overburden, or impair the Company’s facilities, or interfere with any other Member’s use and enjoyment of the Services.
    6. Attempt to gain unauthorized access to any accounts, computer systems or networks connected to the Company’s server or to any of the Services, through hacking, password mining or any other means.
    7. Upload files that contain viruses in whatever form to the Company’s server.
      
6. **INVOICING AND PAYMENTS**
  1. You will be invoiced in advance based on your selected Membership Plan. 
  2. For the monthly Membership Plans, payment is required at the beginning of each month for the ensuing month, at the date specified in the invoice. 
  3. Invoices for variables will be issued at the end of each month and will include any variable and consumable charges, such as telephone usage, printing, copying, conference room rental, mail handling and other Services that may have been incurred during the billing period. 
  4. Payment for variable charges must be paid within 10 (Ten) days of invoicing unless other arrangements have been made with the Company. 
  5. The Company reserves the right to restrict and/ or terminate any Service at any time, immediately and without notice, upon non- payment in part or in full.
    
7. **CONFIDENTIALITY**
  1. You acknowledge and agree that during the period of your Membership, you may be exposed to confidential information and business names, trademarks, service marks, logos, or other intellectual property and information (“Confidential Information”) of the Company and other Members. 
  2. You hereby agree to keep confidential and not disclose to any other person, nor use for any purpose, any such Confidential Information unless such information: 
    1. is required to be disclosed by operation of law or any binding judgement or order, or any requirement of a competent authority;
    2. is reasonably required to be disclosed in confidence to a Party’s professional advisers for use in connection with the Services and/or matters contemplated herein; 
    3. is or becomes within the public domain otherwise than through your default. 
  3. You acknowledge and agree that nothing in this Terms of Use or your Membership will be construed as granting any rights to you, by license or otherwise, to any Confidential Information of the Company, or any other Member.
    
8. **RENEWAL OF MEMBERSHIP**
  1. For the monthly Membership Plans, your Membership will be automatically renewed at the end of each month unless terminated in accordance with Clause 9c.
    
9. **TERMINATION**
  1. This Terms of Use must be adhered to at all times. Failure to abide by the Terms of Use can result in non-renewal and/ or an early termination of your membership. 
  2. The Company reserves the right to terminate any Service at any time, immediately and without notice, upon your failure to comply with this Terms of Use.
  3. The following shall apply in the case of the monthly Membership Plans:
    1. You may terminate your membership by giving 7 (Seven) days written notice of termination to the Company before the commencement of a new calendar month. 
    2. Termination shall be effected on the last working day of the month in which the notice is given.
    3. Should you not provide timely notice of termination to the Company, then your membership shall continue to the end of the following calendar month and the Service fees for that calendar month shall be payable by you.
    4. Upon termination, you will no longer be allowed access to the premises. You will also be required to remove immediately, all your personal effects from the premises failing which the Company may do so.
    5. You hereby agree that, in the event that the Company is forced to remove your personal effects from the premises, all associated costs incurred by the Company shall be for your account.
  
10. **INDEMNIFICATION**
  1. You hereby agree to release, indemnify, defend and hold harmless the Company, its respective officers and its affiliates from and against all claims, losses, damages, costs, expenses and any other liabilities based upon or arising out of your negligent actions, errors and omissions, wilful misconduct and fraud in connection with the participation in or use of the Services.
    
11. **LIMITATION OF LIABILITY**
  1. To the maximum extent permitted by law, in no event shall the Company, its officers or its affiliates, jointly or individually, be liable for any direct, special, incidental, punitive or other claims and damages arising out of or in any way related to your use of the facilities and the Company’s provision of the Services.

