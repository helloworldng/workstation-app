angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.home', {
                url: '?feed',
                templateUrl: 'modules/admin/home/index.html',
                controller: 'AdminHomeController',
                skeleton: true,
                access: 'admin',
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/getstream/';
                        var files = [];

                        files.push(folder + 'getstream.js');
                        files.push(folder + 'stream-angular.js');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    categories: ['$API', function($API) {
                        return $API.all('feed_categories').getList();
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList().then(function(bookings) {
                            return _.filter(bookings, function(booking) {
                                return moment(booking.start_time).isSame(new Date(), 'day');
                            });
                        });
                    }]
                }
            })
            .state('admin.post', {
                url: '/post/:id',
                templateUrl: 'modules/admin/home/post.html',
                controller: 'SinglePostController',
                access: 'admin',
                resolve: {
                    post: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.one('posts', $stateParams.id).get();
                    }]
                }
            })
    }
])

.controller('AdminHomeController', ['$scope', '$rootScope', '$state', '$FullAPI', 'ngDialog', 'bookings', 'categories',
    function($scope, $rootScope, $state, $FullAPI, ngDialog, bookings, categories) {
        $scope.bookings = bookings;
        $scope.categories = categories;
        $scope.activeCategory = _.find(categories, function(cat) {
            return cat.id == $state.params.feed
        });

        if (!$rootScope.member_count) {
            $rootScope.member_count = {};

            $FullAPI.all('users').getList({ status: 'active' }).then(function(members) {
                $rootScope.member_count.active = members.headers("total");
            });
            $FullAPI.all('users').getList().then(function(members) {
                $rootScope.member_count.total = members.headers("total");
            });
        }

        $scope.switchTo = function(category) {
            $state.go($state.current.name, {
                feed: category.id
            }, { reload: true });
        }

        $scope.manageCategories = function() {
            ngDialog.openConfirm({
                templateUrl: "ManageSources",
                controller: "CategoriesController",
                data: {
                    categories: categories
                }
            }).then(function() {
                $state.reload();
            });
        }

        $scope.shareLink = function() {
            ngDialog.open({
                templateUrl: "ShareLink",
                controller: "ShareLinkController",
                data: {
                    categories: categories
                }
            });
        }
    }
])

.controller('SinglePostController', ['$scope', '$rootScope', '$stateParams', '$API', 'post', 'Notification',
    function($scope, $rootScope, $stateParams, $API, post, Notification) {

        $scope.formatPost = function(post) {
            post._id = post.id;
            post.comment_body = post.body;
            post.post_body = post.body;
            post.time = post.created_at;
            post.foreign_id = "Post:" + post.id;

            post.feedable = post.feedable || {};
            post.actor_id = post.feedable_type + "-" + post.feedable.id;
            post.actor_type = post.feedable_type;
            post.actor_email = post.feedable.email;
            post.actor_first_name = post.feedable.first_name;
            post.actor_last_name = post.feedable.last_name;
            post.actor_image = post.feedable.image;
            post.actor_bio = post.feedable.bio;

            var location = _.findWhere($rootScope.locations, { id: post.feedable.home_location_id }) || {};
            post.actor_location = location.description;

            return post;
        }

        post.comments = _.map(post.comments, function(comment) {
            return $scope.formatPost(comment)
        });

        $scope.post = $scope.formatPost(post);

        $scope.delete = function(post, index) {
            $API.one('posts', post._id).remove().then(function() {
                Notification.success("Post deleted", "User's post has been removed");
                $state.go('admin.home');
            }).catch(function(response) {
                Notification.error('Unable to delete post', response);
            });
        }
    }
])

.controller('CategoriesController', ['$scope', '$rootScope', '$state', '$API',
    function($scope, $rootScope, $state, $API) {
        $scope.categories = $scope.ngDialogData.categories;

        $scope.add = function(category_name) {
            $API.all('feed_categories').post({
                name: category_name
            }).then(function(response) {
                $scope.category_name = null;
                $scope.categories.unshift(response);
                $scope.$broadcast("addedCategory", true);
            }, function() {
                Notification.error('Unable to create category', response);
            });
        }

        $scope.update = function(category) {
            category.save().then(function() {
                $scope.$broadcast("updatedCategory", true);
            }, function() {
                Notification.error('Unable to update category', response);
            });
        }

        $scope.delete = function(category) {
            category.remove().then(function() {
                $scope.categories = _.without($scope.categories, category);
                $scope.$broadcast("deletedCategory", true);
            }, function() {
                Notification.error('Unable to delete category', response);
            });
        }
    }
])

.controller('ShareLinkController', ['$scope', '$rootScope', '$state', 'Restangular', '$API', 'Notification',
    function($scope, $rootScope, $state, Restangular, $API, Notification) {
        $scope.categories = $scope.ngDialogData.categories;
        $scope.post = {};

        $scope.share = function() {
            $API.all('posts').post($scope.post).then(function(response) {
                $scope.closeThisDialog();
            }, function(response) {
                Notification.error('Unable to post to feed', response);
                $scope.$broadcast("linkShared", false);
            });
        }
    }
])
