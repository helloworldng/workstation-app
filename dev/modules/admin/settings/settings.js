angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.settings', {
                url: '/settings',
                templateUrl: 'modules/admin/settings/index.html',
                access: 'admin',
                redirectTo: 'admin.settings.general'
            })
            .state('admin.settings.general', {
                url: '/general',
                title: 'General',
                templateUrl: 'modules/admin/settings/pages/general.html',
                controller: 'AdminController',
                access: 'admin',
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                }
            })
            .state('admin.settings.locations', {
                url: '/locations',
                title: 'Locations',
                templateUrl: 'modules/admin/settings/pages/locations.html',
                controller: 'AdminLocationsController',
                access: 'admin',
                resolve: {
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }],
                    countries: ['$API', function($API) {
                        return $API.all('countries').getList();
                    }]
                }
            })
            .state('admin.settings.admins', {
                url: '/admins?:location_id',
                title: 'Admins',
                templateUrl: 'modules/admin/settings/pages/admins.html',
                controller: 'AdminListController',
                access: 'admin',
                resolve: {
                    admins: ['$API', function($API) {
                        return $API.all('admins').getList();
                    }],
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                }
            })
            .state('admin.settings.logs', {
                url: '/logs?page',
                title: 'Logs',
                templateUrl: 'modules/admin/settings/pages/logs.html',
                controller: 'LogListController',
                access: 'admin',
                resolve: {
                    trails: ['$FullAPI', '$stateParams', function($FullAPI, $stateParams) {
                        var page = $stateParams.page || 1;
                        return $FullAPI.all('trails').getList({ page: page })
                    }]
                }
            })
            .state('admin.settings.rooms', {
                url: '/rooms',
                title: 'Rooms',
                templateUrl: 'modules/admin/settings/pages/rooms.html',
                controller: 'AdminRoomsController',
                access: 'admin',
                resolve: {
                    rooms: ['$API', function($API) {
                        return $API.all('meeting_rooms').getList();
                    }],
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                }
            })
            .state('admin.settings.plans', {
                url: '/plans',
                title: 'Plans',
                templateUrl: 'modules/admin/settings/pages/plans.html',
                controller: 'AdminPlansController',
                access: 'admin',
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }]
                }
            })
            .state('admin.settings.offices', {
                url: '/offices',
                title: 'Private Offices',
                templateUrl: 'modules/admin/settings/pages/offices.html',
                controller: 'AdminOfficesController',
                access: 'admin',
                resolve: {
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }]
                }
            });
    }
])

.controller('AdminController', ['$scope', '$rootScope', '$API', '$state', '$window', 'ngDialog', 'FileHelper', 'prices',
    function($scope, $rootScope, $API, $state, $window, ngDialog, FileHelper, prices) {
        $scope.profile = _.clone($rootScope.User);

        $scope.system = {
            tokens: _.findWhere(prices.plain(), { slug: "token" }),
            rgb_printing: _.findWhere(prices.plain(), { slug: "colour-printing" }),
            bw_printing: _.findWhere(prices.plain(), { slug: "b-w-printing" })
        }

        $scope.showModal = function(name, controller, scope) {
            ngDialog.open({
                templateUrl: name,
                controller: controller || "AdminAccountController",
                scope: scope ? $scope : null
            });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $API.one('admins', $scope.profile.id).customPUT({ image: response.url }).then(function() {
                    $rootScope.User.image = response.url;
                });
            }).catch(function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });
    }
])

.controller('AdminPricesController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification',
    function($scope, $rootScope, $state, $auth, $API, Notification) {
        $scope.updatePrices = function() {
            $API.one('add_ons', $scope.system.rgb_printing.id).customPUT({ price: $scope.system.rgb_printing.price });
            $API.one('add_ons', $scope.system.bw_printing.id).customPUT({ price: $scope.system.bw_printing.price });
            $API.one('add_ons', $scope.system.tokens.id).customPUT({ price: $scope.system.tokens.price })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Prices have been updated");
                    });
                });
        }
    }
])

.controller('AdminAccountController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification',
    function($scope, $rootScope, $state, $auth, $API, Notification) {
        $scope.profile = _.clone($rootScope.User);
        $scope.profile.home_location = $scope.profile.home_location || {};
        $scope.profile.home_location_id = $scope.profile.home_location.id;
        $scope.updatePassword = {};

        $scope.updateAccount = function() {
            $API.one('admins', $scope.profile.id).customPUT($scope.profile)
                .then(function() {
                    $auth.updateAccount($scope.profile).then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Your account has been updated");
                    })
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.changePassword = function() {
            $auth.updatePassword({
                    password: $scope.password,
                    password_confirmation: $scope.password_confirmation
                })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Your password has been changed");
                    })
                }, function(response) {
                    $rootScope.$broadcast('passwordChanged', false);
                    Notification.error("Could not change your password", response);
                });
        }
    }
])

.controller('AdminLocationsController', ['$scope', '$state', 'ngDialog', 'locations', 'countries',
    function($scope, $state, ngDialog, locations, countries) {
        $scope.locations = locations.plain();

        $scope.newLocation = function() {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/newLocation.html",
                controller: "NewLocationController",
                trapFocus: false,
                data: {
                    countries: countries.plain()
                }
            });
        }

        $scope.editLocation = function(location) {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/editLocation.html",
                controller: "EditLocationController",
                trapFocus: false,
                data: {
                    location: _.clone(location),
                    countries: countries.plain()
                }
            });
        }

        $scope.deleteLocation = function(location) {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/deleteLocation.html",
                controller: "EditLocationController",
                trapFocus: false,
                data: {
                    location: location
                }
            });
        }

    }
])

.controller('NewLocationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.location = {};
        $scope.countries = $scope.ngDialogData.countries;
        $scope.statesPlaceholder = "Choose Country";

        $scope.save = function() {
            $API.all('locations').post($scope.location)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('locationCreated', false);
                    Notification.error("Could not create location", response);
                });
        }
    }
])

.controller('LocationStatesCitiesController', ['$scope', '$API',
    function($scope, $API) {
        $scope.populateCountry = function(selected) {
            if (!selected) {
                $scope.states = [];
                $scope.location.country_code = null;
                $scope.location.state_shortcode = null;
                $scope.$broadcast('angucomplete-alt:clearInput', 'states');
                return;
            }

            var country = selected.originalObject;
            $scope.location.country_code = country.code;
            $scope.loadingStates = true;

            $API.one('countries', country.code).get()
                .then(function(response) {
                    $scope.states = response.states;
                    $scope.loadingStates = false;
                });
        }

        $scope.populateState = function(selected) {
            if (!selected) {
                $scope.location.state_shortcode = null;
                return;
            }

            var state = selected.originalObject;
            $scope.location.state_shortcode = state.shortcode;
        }
    }
])

.controller('EditLocationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.countries = $scope.ngDialogData.countries;
        $scope.location = $scope.ngDialogData.location;
        $scope.location.country_code = $scope.location.country.code;
        $scope.location.state_shortcode = $scope.location.state.shortcode;

        $scope.save = function() {
            var data = _.omit($scope.location, 'country', 'state');
            $API.one('locations', $scope.location.id).customPUT(data)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('locationUpdated', false);
                    Notification.error("Could not update location", response);
                });
        }

        $scope.delete = function() {
            $API.one('locations', $scope.location.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('locationDeleted', false);
                    Notification.error("Could not delete location", response);
                });
        }
    }
])

.controller('AdminListController', ['$scope', '$state', 'ngDialog', 'admins', 'locations',
    function($scope, $state, ngDialog, admins, locations) {
        $scope.admins = admins;
        $scope.locations = locations.plain();

        $scope.newAdmin = function() {
            ngDialog.open({
                templateUrl: "NewAdmin",
                controller: "NewAdminController",
                data: {
                    locations: locations
                }
            });
        }

        $scope.openModal = function(modal, admin) {
            ngDialog.open({
                templateUrl: modal,
                controller: "EditAdminController",
                data: {
                    admin: admin
                }
            });
        }

        $scope.viewLogs = function(admin) {
            ngDialog.open({
                templateUrl: '/modules/admin/settings/modals/viewLogs.html',
                controller: "AdminLogsController",
                data: {
                    admin: admin
                }
            });
        }

    }
])

.controller('LogListController', ['$scope', '$state', 'ngDialog', 'trails',
    function($scope, $state, ngDialog, trails) {
        $scope.showAdmin = true;
        
        var meta = {
            total: trails.headers("total"),
            perPage: 25,
            page: $state.params.page,
            pageCount: Math.round(trails.headers("total") / 25)
        }
        $scope.trails = trails.data;
        $scope.trails.meta = meta;

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }
    }
])

.controller('AdminLogsController', ['$scope', '$rootScope', '$state', '$FullAPI', 'Notification',
    function($scope, $rootScope, $state, $FullAPI, Notification) {
        $scope.admin = $scope.ngDialogData.admin;

        $scope.load = function(page) {
            var page = page || 1;
            $FullAPI.one('admins', $scope.admin.id).all('trails').getList({ page: page })
                .then(function(response) {
                    var meta = {
                        total: response.headers("total"),
                        perPage: 25,
                        page: page,
                        pageCount: Math.round(response.headers("total") / 25)
                    }
                    $scope.trails = response.data;
                    $scope.trails.meta = meta;
                });
        }

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }

        $scope.load();
    }
])

.controller('NewAdminController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.admin = {};
        $scope.locations = $scope.ngDialogData.locations;

        $scope.save = function() {
            $scope.admin.password_confirmation = $scope.admin.password;
            $API.all('admins').post($scope.admin)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "New admin has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminCreated', false);
                    Notification.error("Could not create admin", response);
                });
        }
    }
])

.controller('EditAdminController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.admin = $scope.ngDialogData.admin;

        $scope.update = function() {
            $API.one('admins', $scope.admin.id).customPUT({
                    superadmin: !$scope.admin.superadmin
                })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Admin privileges have been updated");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminUpdated', false);
                    Notification.error("Could not update admin", response);
                });
        }

        $scope.delete = function() {
            $API.one('admins', $scope.admin.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Admin has been deleted");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminDeleted', false);
                    Notification.error("Could not delete admin", response);
                });
        }
    }
])

.controller('AdminRoomsController', ['$scope', '$state', 'ngDialog', 'locations', 'rooms',
    function($scope, $state, ngDialog, locations, rooms) {
        $scope.rooms = rooms.plain();

        $scope.newRoom = function() {
            ngDialog.open({
                templateUrl: "NewRoom",
                controller: "NewRoomController",
                data: {
                    locations: locations.plain()
                }
            });
        }

        $scope.editRoom = function(room) {
            ngDialog.open({
                templateUrl: "EditRoom",
                controller: "EditRoomController",
                data: {
                    room: _.clone(room),
                    locations: locations.plain()
                }
            });
        }

        $scope.deleteRoom = function(room) {
            ngDialog.open({
                templateUrl: "DeleteRoom",
                controller: "EditRoomController",
                data: {
                    room: room
                }
            });
        }

    }
])

.controller('NewRoomController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.locations = $scope.ngDialogData.locations;
        $scope.room = {
            location_id: $rootScope.User.home_location_id || $scope.locations[0].id
        };

        $scope.save = function() {
            $API.all('meeting_rooms').post($scope.room)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('roomCreated', false);
                    Notification.error("Could not create room", response);
                });
        }
    }
])

.controller('EditRoomController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.locations = $scope.ngDialogData.locations;
        $scope.room = $scope.ngDialogData.room;

        $scope.save = function() {
            $API.one('meeting_rooms', $scope.room.id).customPUT($scope.room)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('roomUpdated', false);
                    Notification.error("Could not update room", response);
                });
        }

        $scope.delete = function() {
            $API.one('meeting_rooms', $scope.room.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('roomDeleted', false);
                    Notification.error("Could not delete room", response);
                });
        }
    }
])

.controller('AdminPlansController', ['$scope', '$state', '$filter', 'ngDialog', 'locations', 'plans', 'addons',
    function($scope, $state, $filter, ngDialog, locations, plans, addons) {
        $scope.plans = _.map(plans, function(plan) {
            var durations = [];
            if (plan.daily_price) durations.push("Daily");
            if (plan.monthly_price) durations.push("Monthly");
            if (plan.yearly_price) durations.push("Yearly");

            plan.durations = durations.join(", ");
            return plan;
        });
        $scope.addons = _.map(addons, function(addon) {
            addon.is_add_on = true;
            return addon;
        });

        $scope.newPlan = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/newPlan.html",
                controller: "NewPlanController",
                data: {
                    plan: plan
                },
                width: 600
            });
        }

        $scope.view = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/viewPlan.html",
                controller: "ViewPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }

        $scope.edit = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editPlan.html",
                controller: "EditPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }

        $scope.delete = function(plan) {
            ngDialog.open({
                templateUrl: "DeletePlan",
                controller: "EditPlanController",
                data: {
                    plan: plan
                }
            });
        }

    }
])

.controller('NewPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.plan = $scope.ngDialogData.plan;
        $scope.addon = {};

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                delete $scope.plan[field];
            });
        }

        $scope.savePlan = function() {
            $API.all('plans').post($scope.plan)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Plan has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('planCreated', false);
                    Notification.error("Could not create plan", response);
                });
        }

        $scope.saveAddon = function() {
            $API.all('add_ons').post($scope.addon)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Add-on has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('addonCreated', false);
                    Notification.error("Could not create add-on", response);
                });
        }
    }
])

.controller('ViewPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, ngDialog) {
        $scope.plan = $scope.ngDialogData.plan;
        $scope.plan.has_daily_option = $scope.plan.daily_price !== null;
        $scope.plan.has_monthly_option = $scope.plan.monthly_price !== null;
        $scope.plan.has_yearly_option = $scope.plan.yearly_price !== null;

        $scope.edit = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editPlan.html",
                controller: "EditPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }
    }
])

.controller('EditPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.plan = $scope.ngDialogData.plan;

        if (!$scope.plan.is_add_on) {
            $scope.plan.has_daily_option = $scope.plan.daily_price !== null;
            $scope.plan.has_monthly_option = $scope.plan.monthly_price !== null;
            $scope.plan.has_yearly_option = $scope.plan.yearly_price !== null;
        }

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                $scope.plan[field] = null;
            });
        }

        $scope.save = function() {
            var url = $scope.plan.is_add_on ? "add_ons" : "plans";
            $API.one(url, $scope.plan.id).customPUT($scope.plan)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", ($scope.plan.is_add_on ? "Add-on" : "Plan") + " has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('planUpdated', false);
                    Notification.error("Could not update " + ($scope.plan.is_add_on ? "add-on" : "plan"), response);
                });
        }

        $scope.delete = function() {
            var url = $scope.plan.is_add_on ? "add_ons" : "plans";
            $API.one(url, $scope.plan.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", ($scope.plan.is_add_on ? "Add-on" : "Plan") + " has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('planDeleted', false);
                    Notification.error("Could not delete " + ($scope.plan.is_add_on ? "add-on" : "plan"), response);
                });
        }
    }
])

.controller('AdminOfficesController', ['$scope', '$state', '$filter', 'ngDialog', 'locations', 'offices',
    function($scope, $state, $filter, ngDialog, locations, offices) {
        $scope.offices = _.map(offices, function(office) {
            var durations = [];
            if (office.monthly_price) durations.push("Monthly");
            if (office.yearly_price) durations.push("Yearly");

            office.durations = durations.join(", ");
            return office;
        });

        $scope.newOffice = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/newOffice.html",
                controller: "NewOfficeController",
                data: {
                    office: office
                },
                width: 600
            });
        }

        $scope.view = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/viewOffice.html",
                controller: "ViewOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }

        $scope.edit = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editOffice.html",
                controller: "EditOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }

        $scope.delete = function(office) {
            ngDialog.open({
                templateUrl: "DeleteOffice",
                controller: "EditOfficeController",
                data: {
                    office: office
                }
            });
        }

    }
])

.controller('NewOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.office = $scope.ngDialogData.office;

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                delete $scope.office[field];
            });
        }

        $scope.saveOffice = function() {
            $API.all('group_plans').post($scope.office)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('officeCreated', false);
                    Notification.error("Could not create office", response);
                });
        }
    }
])

.controller('ViewOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, ngDialog) {
        $scope.office = $scope.ngDialogData.office;
        $scope.office.has_daily_option = $scope.office.daily_price !== null;
        $scope.office.has_monthly_option = $scope.office.monthly_price !== null;
        $scope.office.has_yearly_option = $scope.office.yearly_price !== null;

        $scope.edit = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editOffice.html",
                controller: "EditOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }
    }
])

.controller('EditOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.office = $scope.ngDialogData.office;

        if (!$scope.office.is_add_on) {
            $scope.office.has_daily_option = $scope.office.daily_price !== null;
            $scope.office.has_monthly_option = $scope.office.monthly_price !== null;
            $scope.office.has_yearly_option = $scope.office.yearly_price !== null;
        }

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                $scope.office[field] = null;
            });
        }

        $scope.save = function() {
            $API.one('group_plans', $scope.office.id).customPUT($scope.office)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('officeUpdated', false);
                    Notification.error("Could not update office", response);
                });
        }

        $scope.delete = function() {
            $API.one('group_plans', $scope.office.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('officeDeleted', false);
                    Notification.error("Could not delete office", response);
                });
        }
    }
])
