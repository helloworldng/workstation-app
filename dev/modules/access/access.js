angular.module('app.access', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('access', {
                url: '',
                template: '<div ui-view></div>',
                abstract: true,
                redirectTo: 'access.login',
                resolve: {
                    auth: function($auth, $state, Notification, DEFAULTS) {
                        return $auth.validateUser().then(function(response) {
                            $state.go(DEFAULTS.home);
                        }).catch(function() {
                            // Continue
                        })
                    }
                },
                access: 'public'
            })
            .state('access.login', {
                url: '/login?account_confirmation_success&next',
                templateUrl: 'modules/access/login.html',
                controller: 'LoginController',
                access: 'public'
            })
            .state('access.signup', {
                url: '/signup?plan',
                templateUrl: 'modules/access/signup.html',
                controller: 'SignupController',
                access: 'public',
                resolve: {
                    dial_codes: ['$LocalAPI', function($LocalAPI) {
                        return $LocalAPI.all('dial_codes').getList();
                    }]
                }
            })
            .state('access.activate', {
                url: '/activate',
                templateUrl: 'modules/access/activate.html',
                controller: 'ActivateController',
                resolve: {
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                },
                access: 'public'
            })
            .state('access.forgot-password', {
                url: '/forgot-password',
                templateUrl: 'modules/access/forgot-password.html',
                controller: 'LoginController',
                access: 'public'
            })
            .state('access.reset-password', {
                url: '/reset-password',
                templateUrl: 'modules/access/reset-password.html',
                controller: 'LoginController',
                access: 'public'
            });
    }
])

.controller('LoginController', ['$scope', '$rootScope', '$state', '$auth', 'Notification', 'DEFAULTS',
    function($scope, $rootScope, $state, $auth, Notification, DEFAULTS) {
        $scope.credentials = {};

        $scope.login = function() {
            $auth.submitLogin({
                email: $scope.credentials.email,
                password: $scope.credentials.password
            }).then(function(response) {
                if ($state.params.next && $rootScope.afterLogin) {
                    window.location.href = $rootScope.afterLogin;
                } else $state.go(DEFAULTS.home);
            }).catch(function(response) {
                $rootScope.$broadcast('loggingIn', false);
                Notification.error('Could not Login', response);
            });
        }

        $scope.requestReset = function() {
            $auth.requestPasswordReset({
                email: $scope.resetEmail
            }).then(function(response) {
                $scope.resetEmailSent = true;
            }).catch(function(response) {
                $rootScope.$broadcast('requestingReset', false);
                Notification.error('Password reset failed', response);
            });
        }

        $scope.changePassword = function() {
            $auth.updatePassword({
                password: $scope.password,
                password_confirmation: $scope.password_confirmation
            }).then(function() {
                Notification.success("All Done!", "Your password has been updated");
                window.location = window.location.protocol + "//" + window.location.hostname;
            }, function(response) {
                $rootScope.$broadcast('changingPassword', false);
                Notification.error("Could not change your password", response);
            });
        }
    }
])

.controller('SignupController', ['$scope', '$http', '$state', '$auth', 'Notification', 'LocalService', 'dial_codes', 'DEFAULTS',
    function($scope, $http, $state, $auth, Notification, LocalService, dial_codes, DEFAULTS) {
        $scope.credentials = {};

        $scope.dial_codes = dial_codes;
        $scope.setAreaCode = function(selected) {
            if (!selected) return;

            var country = selected.originalObject;
            $scope.selected_country = country;
        }

        if ($state.params.plan) {
            LocalService.set("workstation-selected-plan", $state.params.plan);
        }

        $scope.signup = function(credentials) {
            var phone_number = credentials.phone;
            if (phone_number.charAt(0) == '0') phone_number = phone_number.substring(1);
            phone_number = $scope.selected_country.dial_code + phone_number;

            $auth.submitRegistration({
                first_name: credentials.first_name.capitalizeFirstLetter(),
                last_name: credentials.last_name.capitalizeFirstLetter(),
                phone: phone_number,
                email: credentials.email,
                password: credentials.password,
                password_confirmation: credentials.password
            }).then(function(response) {
                $scope.signedUp = true;
                $scope.login(credentials);
            }).catch(function(response) {
                $scope.$broadcast('signedUp', false);
                Notification.error('Could not create your account', response);
            });
        }

        $scope.login = function(credentials) {
            $auth.submitLogin({
                email: credentials.email,
                password: credentials.password
            }).then(function(response) {
                $state.go(DEFAULTS.home)
            }).catch(function(response) {
                $state.go('access.login');
                Notification.error('Could not Login', response);
            });
        }
    }
])

.controller('ForgotPasswordController', ['$scope', '$http', '$state', '$auth', 'Notification',
    function($scope, $http, $state, $auth, Notification) {}
])

.controller('ActivateController', ['$scope', '$http', '$state', '$auth', 'Notification', 'locations',
    function($scope, $http, $state, $auth, Notification, locations) {
        $scope.locations = locations;
    }
]);
