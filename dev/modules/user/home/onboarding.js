angular.module('app.user')

.controller('UserOnboardingController', ['$scope', '$rootScope', '$state', 'Session', '$timeout', '$auth', '$API', 'Notification', 'FileHelper',
    function($scope, $rootScope, $state, Session, $timeout, $auth, $API, Notification, FileHelper) {
        $scope.step = 1;
        $scope.profile = $rootScope.User;

        $scope.updateAccount = function() {
            $API.one('users', $scope.profile.id).customPUT($scope.profile)
                .then(function() {
                    $auth.updateAccount($scope.profile).then(function() {
                        $scope.step = 2;
                    })
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $scope.profile.image = response.url;
                $scope.avatar = null;
            }, function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });

        $scope.close = function() {
            Session.update()
                .then(function() {
                    $scope.closeThisDialog();
                });
        }
    }
]);
