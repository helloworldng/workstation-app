angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.community', {
                url: '/community?search&network_only&page',
                templateUrl: 'modules/user/community/index.html',
                controller: 'UserCommunityController',
                skeleton: true,
                title: "Community",
                resolve: {
                    subscription: ['$rootScope', '$state', 'Subscription', function($rootScope, $state, Subscription) {
                        Subscription.verify('can_message').then(function() {}, function() {
                            $state.go('user.home');
                        })
                    }],
                    members: ['$FullAPI', '$rootScope', '$stateParams', function($FullAPI, $rootScope, $stateParams) {
                        if ($stateParams.search) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else if ($stateParams.network_only) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else {
                            return $FullAPI.all('users').getList($stateParams);
                        }
                    }]
                }
            })
    }
])

.controller('UserCommunityController', ['$scope', '$rootScope', '$state', '$API', '$FullAPI', '$ocLazyLoad', 'LocalService', 'Notification', 'ngDialog', 'members',
    function($scope, $rootScope, $state, $API, $FullAPI, $ocLazyLoad, LocalService, Notification, ngDialog, members) {

        function setMembers(response) {
            var _members = response || members;

            $scope.members = _members.data;
            $scope.meta = {
                total: _members.headers("total"),
                perPage: 25,
                page: $state.params.page,
                pageCount: Math.round(_members.headers("total") / 25)
            }

            $scope.filteredCompanies = {};
            $scope.companies = _.uniq(_.filter(_.pluck(members.data, "company")));
            $scope.companyFilter = function(member) {
                var isFiltered = _.some(_.values($scope.filteredCompanies));
                return !isFiltered || $scope.filteredCompanies[member.company];
            };
        }

        setMembers();

        $scope.reload = function(params) {
            $state.go($state.current, params, {
                reload: true,
                inherit: false,
                notify: true
            })
        }

        $scope.view = function(member) {
            ngDialog.open({
                templateUrl: "modules/user/community/modals/member.html",
                controller: "UserMemberController",
                trapFocus: false,
                data: {
                    member: member
                }
            });
        }

        $scope.find = function(query) {
            $scope.searching = true;
            $FullAPI.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.searching = false;
                setMembers(response);
            }, function() {
                $scope.searching = false;
            });
        }

        $ocLazyLoad.load('modules/common/feed/facebook.js');

        $scope.inviteMessage = "‪Join Workstation Today!, a coworking space/community where collaboration meets critical thinking";
        $scope.share = function(message, network, callback) {
            var url = "https://workstationng.com";

            switch (network) {
                case 'facebook':
                    FB.ui({
                        method: 'share',
                        href: "https://www.facebook.com/Workstation-Nigeria-1729334683992057",
                        quote: message
                    }, function(response) {});

                    break;
                case 'twitter':
                    var twitter_url = "https://twitter.com/intent/tweet/?";
                    twitter_url += "&text=" + message.trunc(120);
                    if (url) twitter_url += "&url=" + url;
                    twitter_url += "&via=workstationng";
                    window.open(twitter_url, '_blank');
                    break;
                case 'linkedin':
                    var linkedin_url = "http://www.linkedin.com/shareArticle?mini=true";
                    linkedin_url += "&title=" + message;
                    linkedin_url += "&url=" + url;
                    linkedin_url += "&source=WorkstationNigeria";
                    window.open(linkedin_url, '_blank');
                    break;
                case 'email':
                    $API.all('users/invite').post({
                        invite_email: message
                    }).then(function() {
                        callback();
                        $scope.$broadcast('emaiInviteSent', true);
                        Notification.success('Invite sent', "An invitation has been sent to " + message);
                    }, function(response) {
                        $scope.$broadcast('emaiInviteSent', false);
                        Notification.error("Could not send invite", response);
                    });
                    break;
            }
        }
    }
])

.controller('UserMemberController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        $scope.member = $scope.ngDialogData.member;
        $rootScope.title = $scope.member.first_name + " " + $scope.member.last_name;

        $scope.message = function(member) {
            $scope.closeThisDialog();
            LocalService.set('workstation-new-conversation', JSON.stringify(member));
            $state.go('user.messages', { new: member.email });
        }
    }
]);
