angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.account', {
                url: '/account',
                templateUrl: 'modules/user/account/index.html',
                controller: 'UserAccountController',
                redirectTo: 'user.account.profile',
                resolve: {
                    subscription: ['$rootScope', '$state', '$auth', 'Subscription', function($rootScope, $state, $auth, Subscription) {
                        return Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
            .state('user.account.profile', {
                url: '/profile',
                title: 'Profile',
                templateUrl: 'modules/user/account/pages/profile.html',
                controller: 'UserAccountController',
            })
            .state('user.account.social', {
                url: '/social',
                title: 'Social',
                templateUrl: 'modules/user/account/pages/social.html',
                controller: 'UserAccountController',
            })
            .state('user.account.membership', {
                url: '/membership',
                title: 'Membership',
                templateUrl: 'modules/user/account/pages/membership.html',
                controller: 'UserMembershipController',
                resolve: {
                    purchases: ['$API', function($API) {
                        return $API.all('purchases').getList();
                    }]
                }
            })
            .state('user.account.addons', {
                url: '/addons',
                title: 'Add-ons',
                templateUrl: 'modules/user/account/pages/addons.html',
                controller: 'UserAddonController',
                resolve: {
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }]
                }
            })
            .state('user.account.team', {
                url: '/team',
                title: 'Team',
                templateUrl: 'modules/user/account/pages/team.html',
                controller: 'UserTeamSettingsController',
                resolve: {
                    invitations: ['$API', function($API) {
                        return $API.all('team_invitations').getList();
                    }]
                }
            })
            .state('user.account.payment', {
                url: '/payment',
                title: 'Payment',
                templateUrl: 'modules/user/account/pages/payment.html',
                controller: 'UserPaymentController',
                resolve: {
                    inline: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('https://js.paystack.co/v1/inline.js');
                    }]
                }
            })
            .state('user.account.invoices', {
                url: '/invoices',
                title: 'Invoices',
                templateUrl: 'modules/user/account/pages/invoices.html',
                controller: 'UserBillingController'
            })
            .state('user.account.purchases', {
                url: '/purchases',
                title: 'Purchases',
                templateUrl: 'modules/user/account/pages/purchases.html',
                controller: 'UserBillingController'
            })
            .state('user.account.printing', {
                url: '/printing',
                title: 'Printing',
                templateUrl: 'modules/user/account/pages/printing.html',
                controller: 'UserPrintingController'
            })
    }
])

.controller('UserAccountController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification', 'FileHelper', 'Session',
    function($scope, $rootScope, $state, $auth, $API, Notification, FileHelper, Session) {
        $scope.profile = _.clone($rootScope.User);

        $scope.updateAccount = function() {
            var payload = _.clone($scope.profile);
            if (payload.new_email) payload.email = payload.new_email;
            $API.one('users', $scope.profile.id).customPUT(payload)
                .then(Session.update)
                .then(function() {
                    $scope.closeThisDialog();
                    $state.reload();
                    Notification.success("All Done!", "Your account has been updated");
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $API.one('users', $scope.profile.id).customPUT({ image: response.url }).then(function() {
                    $scope.profile.image = response.url;
                    $scope.avatar = null;
                });
            }).catch(function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });

        $scope.removeImage = function() {
            $API.one('users', $scope.profile.id).customPUT({ image: null }).then(function() {
                $scope.profile.image = null;
            });
        }
    }
])

.controller('UserMembershipController', ['$scope', '$rootScope', '$state', 'Session', '$auth', '$API', 'Notification', 'ngDialog', 'purchases',
    function($scope, $rootScope, $state, Session, $auth, $API, Notification, ngDialog, purchases) {
        $scope.moment = moment;
        $scope.profile = _.omit($rootScope.User, 'subscription');
        $scope.subscription = _.clone($rootScope.User.subscription);
        $scope.plan = _.clone($rootScope.User.plan);

        if ($scope.subscription && $scope.subscription.days) {
            $scope.subscription.days = JSON.parse($scope.subscription.days);
        }

        $scope.purchases = _.filter(purchases, function(purchase) {
            return purchase.active && purchase.add_on && !purchase.add_on.system;
        });

        $scope.buyTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyTokensController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.buyPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyPrintingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.cancelSubscription = function() {
            ngDialog.openConfirm({
                template: "CancelSubscription",
                controller: ['$scope', '$state', '$API', 'Session', 'Notification', function($scope, $state, $API, Session, Notification) {
                    $scope.cancel = function() {
                        $API.all('subscriptions/cancel').post()
                            .then(Session.update)
                            .then(function() {
                                Notification.success('Subscription cancelled', "Your current subscription has been cancelled");
                                $state.reload();
                                $scope.closeThisDialog();
                            })
                            .catch(function(response) {
                                $rootScope.$broadcast('cancelledSubscription', false);
                                Notification.error("Could not cancel subscription", response);
                            });
                    }
                }]
            });
        }

        $scope.changeDays = function() {
            ngDialog.openConfirm({
                templateUrl: "ChangeDays",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }]
                },
                data: { subscription: $scope.subscription },
                controller: 'ChangeDaysController'
            });
        }
    }
])

.controller('ChangeDaysController', ['$scope', '$rootScope', '$API', 'Session', 'Notification', 'plans', function($scope, $rootScope, $API, Session, Notification, plans) {
    $scope.subscription = $scope.ngDialogData.subscription;
    $scope.plan = _.findWhere(plans, { id: $scope.subscription.plan_id });

    $scope.days = { monday: false, tuesday: false, wednesday: false, thursday: false, friday: false };
    _.each($scope.subscription.days, function(day) { $scope.days[day] = true });

    $scope.addDays = function() {
        $scope.subscription.days = [];
        _.each($scope.days, function(value, key) {
            if (value) $scope.subscription.days.push(key);
        })
    }

    $scope.update = function() {
        $API.one('subscription', $scope.subscription.id)
            .customPUT({
                days: $scope.subscription.days
            })
            .then(Session.update)
            .then(function() {
                Notification.success("All Done!", "Your subscription has been updated");
                $state.reload();
                $scope.closeThisDialog();
            })
            .catch(function(response) {
                $rootScope.$broadcast('updatedSubscription', false);
                Notification.error("Could not update your subscription", response);
            });
    }
}])

.controller('BuyTokensController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'prices',
    function($scope, $rootScope, $state, Session, $API, Notification, prices) {
        var defaults = $scope.ngDialogData || {};
        var system_token = _.findWhere(prices.plain(), { slug: "token" }) || {};

        $scope.token_cost = system_token.price;
        $scope.tokens = defaults.amount;
        $scope.use_team_card = false;

        $scope.buy = function() {
            $API.all('purchases')
                .post({
                    add_on_id: system_token.id,
                    quantity: $scope.tokens,
                    use_team_card: $scope.use_team_card
                })
                .then(Session.update)
                .then(function() {
                    Notification.success('Tokens Purchased', "Your token count has been updated");
                    $state.reload();
                    $scope.closeThisDialog();
                }).catch(function(response) {
                    $rootScope.$broadcast('tokensPurchased', false);
                    Notification.error("Could not purchase tokens", response);
                });
        }
    }
])

.controller('BuyPrintingController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'prices',
    function($scope, $rootScope, $state, Session, $API, Notification, prices) {
        var defaults = $scope.ngDialogData || {};

        var system_bw_printing = _.findWhere(prices.plain(), { slug: "b-w-printing" }) || {};
        var system_rgb_printing = _.findWhere(prices.plain(), { slug: "colour-printing" }) || {};
        $scope.use_team_card = false;

        $scope.rgb_printing_cost = system_rgb_printing.price;
        $scope.bw_printing_cost = system_bw_printing.price;

        $scope.bw_printing = defaults.bw_printing;
        $scope.rgb_printing = defaults.rgb_printing;
        $scope.total = 0;

        $scope.computeTotal = function() {
            $scope.total = (($scope.bw_printing || 0) * $scope.bw_printing_cost) + (($scope.rgb_printing || 0) * $scope.rgb_printing_cost);
        }

        $scope.buy = function() {
            var tasks = 0;

            if ($scope.bw_printing) {
                tasks++;
                $API.all('purchases').post({
                    add_on_id: system_bw_printing.id,
                    quantity: $scope.bw_printing,
                    use_team_card: $scope.use_team_card
                }).then(function() {
                    onComplete();
                    Notification.success('B/W Printing Purchased', "Your printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingPurchased', false);
                    Notification.error("Could not purchase b/w printing", response);
                });
            }

            if ($scope.rgb_printing) {
                tasks++;
                $API.all('purchases').post({
                    add_on_id: system_rgb_printing.id,
                    quantity: $scope.rgb_printing,
                    use_team_card: $scope.use_team_card
                }).then(function() {
                    onComplete();
                    Notification.success('Color Printing Purchased', "Your printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingPurchased', false);
                    Notification.error("Could not purchase color printing", response);
                });
            }

            var onComplete = function() {
                tasks--;
                if (tasks == 0) {
                    $rootScope.$broadcast('printingPurchased', true);
                    Session.update().then(function() {
                        $scope.closeThisDialog();
                        $state.reload();
                    })
                }
            }
        }
    }
])

.controller('UserTeamSettingsController', ['$scope', '$rootScope', '$state', '$auth', 'Session', '$API', 'ngDialog', 'Notification', 'invitations',
    function($scope, $rootScope, $state, $auth, Session, $API, ngDialog, Notification, invitations) {
        $scope.moment = moment;
        $scope.profile = _.clone($rootScope.User);
        $scope.invitations = invitations;

        if ($scope.profile.is_team_admin) {
            $state.go('user.team');
            return;
        }

        if ($scope.profile.has_team) {
            $API.one('teams', $rootScope.User.team_id).one('members').get()
                .then(function(team) {
                    $scope.team = team;
                    $scope.team_admin = _.findWhere(team.members, { role: "is_admin" }).user;
                });
        }

        $scope.leaveTeam = function() {
            ngDialog.openConfirm({
                templateUrl: "LeaveTeam",
                scope: $scope
            }).then(function() {
                $API.one('teams', $rootScope.User.team_id).all('leave').remove()
                    .then(Session.update)
                    .then(function() {
                        Notification.success('Successful', "Your have successfully left this team");
                        $state.reload();
                    }).catch(function(response) {
                        $rootScope.$broadcast('leftTeam', false);
                        Notification.error("Could not leave team", response);
                    });
            });
        }

        $scope.createTeam = function() {
            $API.all('teams').post()
                .then(Session.update)
                .then(function() {
                    Notification.success('Team Created', "Your team has been created");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('teamCreated', false);
                    Notification.error("Could not create team", response);
                });
        }

        $scope.acceptInvitation = function(id) {
            $API.one('team_invitations/' + id, 'accept').get()
                .then(Session.update)
                .then(function() {
                    Notification.success('Invitation accepted', "You are now a member of this team");
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('invitationAccepted', false);
                    Notification.error("Could not accept invitation", response);
                });
        }

        $scope.cancelInvitation = function(id) {
            $API.one('team_invitations', id).remove()
                .then(function() {
                    Notification.success('Invitation cancelled', "You can now create your own team");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('cancelledInvitation', false);
                    Notification.error("Could not cancel invitation", response);
                });
        }
    }
])


.controller('UserAddonController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Session', 'ngDialog', 'Notification', 'addons',
    function($scope, $rootScope, $state, $auth, $API, Session, ngDialog, Notification, addons) {
        $scope.profile = _.clone($rootScope.User);
        if ($rootScope.User.subscription) $scope.profile.days = JSON.parse($rootScope.User.subscription.days);
        $scope.addons = addons;
        $scope.use_team_card = false;

        $scope.confirmPurchase = function(addon) {
            $scope.selected_addon = addon;
            ngDialog.open({
                templateUrl: "BuyAddon",
                scope: $scope
            });
        }

        $scope.buy = function(addon, callback) {
            $API.all('purchases')
                .post({
                    add_on_id: addon.id,
                    use_team_card: $scope.use_team_card
                })
                .then(Session.update)
                .then(function() {
                    callback();
                    Notification.success("Addon Purchased", addon.name + " has been added to your subscription");
                    $state.go('user.account.membership');
                })
                .catch(function(response) {
                    $rootScope.$broadcast('addonPurchased', false);
                    Notification.error("Could not purchase this addon", response);
                });
        }
    }
])

.controller('UserPaymentController', ['$scope', '$rootScope', '$state', 'ngDialog', '$API', 'Session', 'Notification', 'DEFAULTS',
    function($scope, $rootScope, $state, ngDialog, $API, Session, Notification, DEFAULTS) {
        $scope.auth = $rootScope.User.paystack_authorization;

        $scope.addCard = function() {
            ngDialog.openConfirm({
                templateUrl: "AddCard",
                scope: $scope
            }).then(function() {
                $scope.loadPaystack();
            });
        }

        var onClose = function() {
            $rootScope.$broadcast('paystackOpen', false);
        }

        var callback = function(response) {
            $API.all('users/paystack_authorizations').post({ reference: response.reference })
                .then(Session.update)
                .then(function() {
                    $rootScope.$broadcast('paystackOpen', true);
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('paystackOpen', false);
                    Notification.error("Could not add your card", response);
                });
        }

        $scope.loadPaystack = function() {
            var handler = PaystackPop.setup({
                key: DEFAULTS.paystackKey,
                email: $rootScope.User.email,
                amount: 5000,
                onClose: onClose,
                callback: callback
            });

            handler.openIframe();
        }
    }
])

.controller('UserBillingController', ['$scope', '$rootScope', '$state', '$auth', '$FullAPI', 'Notification',
    function($scope, $rootScope, $state, $auth, $FullAPI, Notification) {

        $scope.load = function(resource, page) {
            var page = page || 1;
            $FullAPI.all(resource).getList().then(function(response) {
                var meta = {
                    total: response.headers("total"),
                    perPage: 25,
                    page: page,
                    pageCount: Math.round(response.headers("total") / 25)
                }
                $scope[resource] = response.data;
                $scope[resource].meta = meta;
            })
        }

    }
])

.controller('UserPrintingController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification', 'Session', 'ngDialog',
    function($scope, $rootScope, $state, $auth, $API, Notification, Session, ngDialog) {

        $scope.setup = function(password) {
            $API.all('printing/setup_print').post({
                    password: password
                })
                .then(Session.update)
                .then(function() {
                    $state.reload();
                })
                .catch(function(response) {
                    $scope.$broadcast('setup_print', false);
                    Notification.error("Print setup failed", response);
                });
        }

        $scope.buyPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyPrintingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.updatePrintPassword = function(password, callback) {
            $API.all('printing/update_print_password').post({
                    password: password
                })
                .then(Session.update)
                .then(function() {
                    callback();                    
                    Notification.success("Password Changed", "You can now authenticate with your new password");
                    $state.reload();
                })
                .catch(function(response) {
                    $scope.$broadcast('passwordSaved', false);
                    Notification.error("Password Change failed", response);
                });
        }
    }
]);
