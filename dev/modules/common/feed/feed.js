angular.module('app.common')

.controller('FeedController', ['$scope', '$rootScope', '$state', '$timeout', '$streamService', '$ocLazyLoad', '$API', '$FullAPI', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $timeout, $streamService, $ocLazyLoad, $API, $FullAPI, Notification, ngDialog) {
        $scope.moment = moment;
        $scope.updates = [];
        $scope.meta = { page: 1, per_page: 25 }

        var user = $rootScope.User;
        $scope.userID = (user.isAdmin ? "Admin-" : "User-") + user.id;
        var user_location = user.home_location ? user.home_location.id : "";
        if (user.isAdmin && !user_location) user_location = $rootScope.locations[0].id;

        var location = _.find($rootScope.locations, function(location) {
            return location.id == user_location;
        });

        var stream = $streamService.connect('8bqdnjnbspbt', '14276');
        var timeline = stream.feed('location_timeline', location.id, location.feed_token);

        $FullAPI.all('posts').getList({
            feed_category_id: $state.params.feed,
            per_page: $scope.meta.per_page
        }).then(function(response) {
            $scope.meta.last_page = Math.round(response.headers("total") / $scope.meta.per_page);
            $scope.feed = _.map(response.data, function(post) {
                post.comments = _.map(post.comments, function(comment) {
                    return $scope.formatPost(comment)
                });
                return $scope.formatPost(post);
            });
        });

        $scope.loadMore = function() {
            if ($scope.loading || $scope.meta.last_page <= $scope.meta.page) return;

            $scope.loadingPosts = true;
            $scope.meta.page++;

            $API.all('posts').getList({
                feed_category_id: $state.params.feed,
                per_page: $scope.meta.per_page,
                page: $scope.meta.page
            }).then(function(response) {
                $scope.loadingPosts = false;
                _.each(response.plain(), function(post) {
                    post.comments = _.map(post.comments, function(comment) {
                        return $scope.formatPost(comment)
                    });
                    var formattedPost = $scope.formatPost(post);
                    var postInFeed = _.find($scope.feed, function(_post) {
                        return _post.foreign_id === post.foreign_id
                    });

                    if (!postInFeed) $scope.feed.push(formattedPost);
                });
            });
        }

        timeline.subscribe(function callback(response) {
            if (response.deleted.length) {
                _.each(response.deleted, function(postId) {
                    $scope.removeFromFeed(postId);
                });
            }

            if (response.new.length) {
                _.each(response.new, function(post) {
                    $scope.addToFeed(post);
                });
            }

            $scope.$apply();
        }).then(function() {
            console.log("connected");
        }, function(error) {
            console.log(error)
        });

        $scope.formatPost = function(post) {
            post._id = post.id;
            post.comment_body = post.body;
            post.post_body = post.body;
            post.time = post.created_at;
            post.foreign_id = "Post:" + post.id;

            post.feedable = post.feedable || {};
            post.actor_id = post.feedable_type + "-" + post.feedable.id;
            post.actor_type = post.feedable_type;
            post.actor_email = post.feedable.email;
            post.actor_company = post.feedable.company;
            post.actor_first_name = post.feedable.first_name;
            post.actor_last_name = post.feedable.last_name;
            post.actor_image = post.feedable.image;
            post.actor_bio = post.feedable.bio;

            var location = _.findWhere($rootScope.locations, { id: post.feedable.home_location_id }) || {};
            post.actor_location = location.description;

            if (post.url_meta) {
                post.meta = JSON.parse(post.url_meta);
                post.meta.image && (post.meta.thumb = 'https://carnation-town.glitch.me/?url=' + post.meta.image);
            }

            return post;
        }

        $scope.removeFromFeed = function(postId) {
            var itemInFeed = _.findIndex($scope.feed, { _id: postId });
            var itemInUpdates = _.findIndex($scope.updates, { _id: postId });

            if (itemInFeed > -1) $scope.feed.splice(itemInFeed, 1);
            if (itemInUpdates > -1) $scope.updates.splice(itemInUpdates, 1);
        }

        $scope.addToFeed = function(activity) {
            if (activity.verb === "Post") $scope.addPostToFeed(activity);
            if (activity.verb === "Like") $scope.addLikeToFeed(activity);
            if (activity.verb === "Comment") $scope.addCommentToFeed(activity);
        }

        $scope.addPostToFeed = function(post) {
            post.created_at = moment.parseZone(post.time).local();
            post._id = post.foreign_id.split(":").pop();
            post.actor_id = post.origin.split(":").pop();

            if (post.feed_category_id) {
                post.feed_category = {
                    id: post.feed_category_id,
                    name: post.feed_category_name
                }
            }

            if (post.url_meta) {
                post.meta = JSON.parse(post.url_meta);
                post.meta.image && post.meta.image.startsWith("https") && (post.meta.thumb = post.meta.image);
            }

            var byLoggedInUser = post.origin === ("user:" + $scope.userID);
            if (byLoggedInUser) {
                $scope.feed.unshift(post);
            } else {
                $scope.updates.unshift(post);
                $scope.updateNotificationCount();
            }
        }

        $scope.addLikeToFeed = function(like) {
            like.created_at = moment.parseZone(like.time).local();
            var byLoggedInUser = like.origin === ("user:" + $scope.userID);;
            var postInFeed = _.find($scope.feed, function(post) {
                return post.foreign_id === like.object
            });

            if (postInFeed && !byLoggedInUser) postInFeed.likes_count++;
        }

        $scope.addCommentToFeed = function(comment) {
            var postInFeed = _.find($scope.feed, function(post) {
                return post.foreign_id === comment.object
            });

            comment.created_at = moment.parseZone(comment.time).local();
            comment._id = comment.foreign_id.split(":").pop();
            comment.actor_id = comment.origin.split(":").pop();

            byLoggedInUser = comment.origin === ("user:" + $scope.userID);
            if (postInFeed && !byLoggedInUser) {
                postInFeed.comments = postInFeed.comments || [];
                postInFeed.comments.unshift(comment);
                $scope.$apply();
            };
        }

        $scope.updateNotificationCount = function() {
            if ($scope.updates.length) {
                $rootScope.$title = "(" + $scope.updates.length + ")";
            } else {
                $rootScope.$title = "";
            }
        }

        $scope.showNewUpdates = function() {
            $scope.feed = _.union($scope.updates, $scope.feed);
            $scope.updates = [];
            $rootScope.$title = "";
        }

        $scope.send = function(message, scope) {
            if (!message) return;

            var payload = { body: message }
            var urls = linkify.find(message);
            if (urls.length) {
                payload.link = _.last(urls).href;
            }

            $scope.postingMessage = true;
            $API.all('posts').post(payload).then(function(response) {
                $scope.$broadcast('postedMessage', true);
                $scope.$broadcast('clearMessage', scope);
                $scope.postingMessage = false;
            }, function(response) {
                Notification.error('Unable to post to feed', response);
                $scope.$broadcast('postedMessage', false);
                $scope.postingMessage = false;
            });
        }

        $scope.$on('clearMessage', function(ev, scope) {
            scope.message = null;
        });

        $scope.like = function(post) {
            $API.one('posts', post._id).all('like').post().then(function() {
                post.has_liked = true;
                post.likes_count++;
            }).catch(function(response) {
                Notification.error('Unable to like post', response);
                $scope.$broadcast('postedMessage', false);
            });
        }

        $scope.addComment = function(post) {
            var message = post.new_comment;
            $API.one('posts', post._id).all('comment').post({
                body: post.new_comment
            }).then(function(response) {
                post.comments = post.comments || [];
                var comment = $scope.formatPost(response.plain());
                post.comments.unshift(comment);
            }).catch(function(response) {
                post.new_comment = message;
                Notification.error('Unable to add comment', response);
                $scope.$broadcast('postedMessage', false);
            });
            post.new_comment = "";
        }

        $scope.unlike = function(post) {
            $API.one('posts', post._id).all('unlike').post().then(function() {
                post.has_liked = false;
                post.likes_count--;
            }).catch(function(response) {
                Notification.error('Unable to unlike post', response);
                $scope.$broadcast('postedMessage', false);
            });
        }

        $scope.delete = function(post, index) {
            $API.one('posts', post._id).remove().then(function() {
                $scope.feed = _.without($scope.feed, post);
            }).catch(function(response) {
                Notification.error('Unable to delete post', response);
            });
        }

        $scope.deleteComment = function(post, comment) {
            $API.one('posts/delete_comment', comment._id).remove().then(function() {
                post.comments = _.without(post.comments, comment);
            }).catch(function(response) {
                Notification.error('Unable to delete comment', response);
            });
        }


        // Initialize Facebook
        $ocLazyLoad.load('modules/common/feed/facebook.js');

        $scope.share = function(post, network) {
            var url, message;

            if (post.feed_category) {
                url = post.body;
                message = "Shared on Workstation";
            }

            switch (network) {
                case 'facebook':
                    FB.ui({
                        method: 'share',
                        href: url || "https://www.facebook.com/Workstation-Nigeria-1729334683992057",
                        quote: message || post.body
                    }, function(response) {});

                    break;
                case 'twitter':
                    var twitter_url = "https://twitter.com/intent/tweet/?";
                    twitter_url += "&text=" + (message || post.body).trunc(120);
                    if (url) twitter_url += "&url=" + url;
                    twitter_url += "&via=workstationng";
                    window.open(twitter_url, '_blank');
                    break;
                case 'linkedin':
                    var linkedin_url = "http://www.linkedin.com/shareArticle?mini=true";
                    linkedin_url += "&title=" + (message || post.body);
                    linkedin_url += "&url=" + (url || "http://workstationng.com");
                    linkedin_url += "&source=WorkstationNigeria";
                    window.open(linkedin_url, '_blank');
                    break;
            }
        }

        $scope.report = function(post) {
            ngDialog.openConfirm({
                template: "ConfirmReport"
            }).then(function() {
                $API.one('posts', post._id).all('report').post({
                    id: post._id,
                    home_location_id: user.home_location.id
                }).then(function() {
                    Notification.success('Post reported', 'Admins will review this post');
                }).catch(function(response) {
                    Notification.error('Unable to report post', response);
                });
            });
        }
    }
])

.controller('UserPopupController', ['$scope', '$rootScope', '$state', 'LocalService',
    function($scope, $rootScope, $state, LocalService) {
        var post = $scope.comment || $scope.post;

        $scope.profile = {
            id: parseInt(post.actor_id.split("-").pop()),
            is_admin: post.actor_type == "Admin",
            is_not_self: post.actor_id != $scope.postserID,
            first_name: post.actor_first_name,
            last_name: post.actor_last_name,
            bio: post.actor_bio,
            company: post.actor_company,
            email: post.actor_email,
            image: post.actor_image,
            location: post.actor_location
        }

        $scope.message = function() {
            LocalService.set('workstation-new-conversation', JSON.stringify($scope.profile));
            $state.go('user.messages', { new: $scope.profile.email });
        }
    }
]);
