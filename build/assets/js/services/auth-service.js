angular.module('app.authService', [])
    .service('Session', ['$rootScope', '$window', '$q', '$API', 'DEFAULTS',
        function($rootScope, $window, $q, $API, DEFAULTS) {
            function pad(num, size) {
                var s = num + "";
                while (s.length < size) s = "0" + s;
                return s;
            }

            function create(user) {
                var deferred = $q.defer();

                user.isAdmin = DEFAULTS.config === "admin";
                user.member_id = pad(user.id, 7);

                if (user.group_plan_subscription && user.subscription) {
                    var group_plan_started = new Date(user.group_plan_subscription.created_at);
                    var regular_plan_started = new Date(user.subscription.created_at);
                    var group_plan_started_later = moment(group_plan_started).isAfter(regular_plan_started);

                    if (group_plan_started_later) {
                        user.subscription = user.group_plan_subscription;
                        user.has_office = true;
                    } else {
                        user.group_plan_subscription = null;
                    }
                }

                if (user.group_plan_subscription && !user.subscription) {
                    user.subscription = user.group_plan_subscription;
                    user.has_office = true;
                }

                if (user.subscription && !user.subscription.enabled) {
                    user._subscription = user.subscription;
                    user.subscription = null;
                }

                if (user.subscription) {
                    fetchPlan(user);
                }

                if (user.team_id != null) {
                    user.has_team = true;
                    user.is_team_admin = user.team_admin;
                    fetchTeam(user);
                }

                deferred.resolve(user);
                return deferred.promise;
            }

            function fetchPlan(user) {
                var plans_url = user.has_office ? 'group_plans' : 'plans';
                var plan_id = user.has_office ? 'group_plan_id' : 'plan_id';

                return $API.one(plans_url, 'retrieve_plan').get({
                        id: user.subscription[plan_id]
                    })
                    .then(function(plan) {
                        $rootScope.User.plan = plan.plain();
                    });
            }

            function fetchTeam(user) {
                $API.one('teams', user.team_id).one('members').get()
                    .then(function(team) {
                        var team_registration = _.find(team.members, function(member) {
                            return member.user.id == user.id
                        });
                        $rootScope.User.team_registration = _.omit(team_registration, 'user');
                    });
            }

            return {
                create: create,
                update: function(response) {
                    var deferred = $q.defer();

                    $API.one('users', 'current_session').get()
                        .then(create)
                        .then(function(user) {
                            $rootScope.User = user;
                            deferred.resolve(response);
                        })
                        .catch(function() {
                            setTimeout(function() { $window.location.reload() }, 500);
                            deferred.resolve(response);
                        });

                    return deferred.promise;
                },
                fetchPlan: fetchPlan,
                fetchTeam: fetchTeam
            }
        }
    ])
    .service('Subscription', ['$rootScope', '$q', '$API', 'Session',
        function($rootScope, $q, $API, Session) {
            function checkRestriction(ability) {
                var user = $rootScope.User;
                var restriction = $q.defer();

                if (!user.subscription || !user.subscription.enabled) {
                    restriction.reject("No current subscription");
                    return restriction.promise;
                }

                user.subscription_expired = moment(user.subscription.end_date).isBefore(moment());
                if (user.subscription_expired) {
                    $API.all('subscriptions/cancel').post().then(function() {
                        var message = "Current subscription expired";
                        restriction.reject(message);
                    });
                } else if (ability && !user[ability]) {
                    restriction.reject("Current subscription does not allow this feature");
                } else {
                    restriction.resolve();
                }

                if (user.can_message) $rootScope.checkUnread();
                return restriction.promise;
            }

            return {
                verify: function(ability) {
                    var deferred = $q.defer();

                    if ($rootScope.User) {
                        checkRestriction(ability).then(function() {
                            deferred.resolve();
                        }, function(reason) {
                            deferred.reject(reason);
                        });
                    } else {
                        $rootScope.$on('auth:validate-subscription', function(ev, user) {
                            checkRestriction(ability).then(function() {
                                deferred.resolve();
                            }, function(reason) {
                                deferred.reject(reason);
                            });
                        });
                    }

                    return deferred.promise;
                }
            }
        }
    ]);
