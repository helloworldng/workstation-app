'use strict';

angular.module('app', [
        'angucomplete-alt',
        'angular-loading-bar',
        'currencyMask',
        'ellipsis',
        'ngAnimate',
        'ngDialog',
        'ngSanitize',
        'ngStorage',
        'ng-token-auth',
        'oc.lazyLoad',
        'restangular',
        'scrollglue',
        'toaster',
        'ui.bootstrap',
        'ui.paging',
        'ui.directives',
        'ui.controllers',
        'ui.calendar',
        'ui.gravatar',
        'ui.router',
        'ui.bootstrap.datetimepicker',
        'vkEmojiPicker',
        'app.localService',
        'app.historyService',
        'app.notificationService',
        'app.apiFactories',
        'app.authService',
        'app.uiService',
        'app.access',
        'app.admin',
        'app.user',
        'app.common'
    ])
    .constant('DEFAULTS', {
        'config': settings.config,
        'home': settings.config + ".home",
        'baseAPIUrl': settings.baseAPIUrl,
        'clientToken': settings.clientToken,
        'paystackKey': settings.paystackKey
    })
    .config(['$urlRouterProvider', '$httpProvider', 'RestangularProvider', '$locationProvider', '$stateProvider', 'ngDialogProvider', 'gravatarServiceProvider', 'DEFAULTS', '$authProvider', '$provide',
        function($urlRouterProvider, $httpProvider, RestangularProvider, $locationProvider, $stateProvider, ngDialogProvider, gravatarServiceProvider, DEFAULTS, $authProvider, $provide) {

            $urlRouterProvider
                .when('', ['$state', function($state) {
                    $state.go('access.login');
                }]);

            $urlRouterProvider.otherwise('/login');

            ngDialogProvider.setDefaults({
                className: 'ngdialog-theme-plain',
                showClose: false,
            });

            gravatarServiceProvider.defaults = {
                size: 100,
                "default": 'mm'
            };

            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                if (data && data.response) {
                    var returnedData = data.response.data;
                    if (data.response.meta) returnedData.meta = data.response.meta;
                    return returnedData;
                } else {
                    return data;
                };
            });

            $authProvider.configure({
                apiUrl: DEFAULTS.baseAPIUrl,
                confirmationSuccessUrl: window.location.protocol + "//" + window.location.hostname,
                passwordResetSuccessUrl: window.location.protocol + "//" + window.location.hostname
            });

            $httpProvider.interceptors.push('BearerAuthInterceptor');

            // Cache busting
            var cacheBuster = Date.now().toString();

            function templateFactoryDecorator($delegate) {
                var fromUrl = angular.bind($delegate, $delegate.fromUrl);
                $delegate.fromUrl = function(url, params) {
                    if (url !== null && angular.isDefined(url) && angular.isString(url)) {
                        url += (url.indexOf("?") === -1 ? "?" : "&");
                        url += "v=" + cacheBuster;
                    }

                    return fromUrl(url, params);
                };

                return $delegate;
            }

            $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
        }
    ])
    .run(['$rootScope', '$location', '$API', '$state', '$stateParams', 'DEFAULTS', 'LocalService', 'ngDialog', 'Session', 'Notification',
        function($rootScope, $location, $API, $state, $stateParams, DEFAULTS, LocalService, ngDialog, Session, Notification) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.Date = Date;
            $rootScope.DEFAULTS = DEFAULTS;
            $rootScope.ui = {};

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                // If it's a parent state, redirect to it's child
                if (toState.redirectTo) {
                    event.preventDefault();
                    var params = _.extend(toParams, $location.search());
                    $state.go(toState.redirectTo, params);
                    return;
                }

                if (DEFAULTS.config === "admin" && toState.access != "admin" && toState.access != "public") {
                    event.preventDefault();
                    $state.go("admin.home");
                    return;
                }

                if (DEFAULTS.config === "user" && toState.access == "admin") {
                    event.preventDefault();
                    $state.go("user.home");
                    return;
                }
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.$state.activeParams = _.some(_.values($stateParams));
                $rootScope.ui.fullHeight = toState.fullHeight;
                $rootScope.ui.skeleton = toState.skeleton;
                $rootScope.$title = toState.title;
                window.scrollTo(0, 0);
            })

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                console.log(error);
            })

            $rootScope.$on('auth:validation-success', function(ev, user) {
                Session.create(user)
                    .then(function(response) {
                        $rootScope.User = response;
                        if (DEFAULTS.config === "user") {
                            $rootScope.$broadcast('auth:validate-subscription', response);
                        }
                    });
            });

            $rootScope.$on('auth:login-success', function(ev, user) {
                Session.create(user)
                    .then(function(response) {
                        $rootScope.User = response;
                        if (DEFAULTS.config === "user") {
                            $rootScope.$broadcast('auth:validate-subscription', response);
                        }
                    })
            });

            $rootScope.$on('auth:email-confirmation-success', function(ev, user) {
                Notification.success('Verification Complete', 'Your account has now been verified');
                $state.reload();
            });

            $rootScope.$on('auth:password-reset-confirm-success', function() {
                ngDialog.openConfirm({
                    templateUrl: '/modules/access/reset-password.html',
                    controller: "LoginController",
                    appendClassName: 'ngdialog-darker',
                    closeByEscape: false,
                    closeByNavigation: false,
                    closeByDocument: false
                });
            });

            $rootScope.checkUnread = function() {
                $API.all("messaging/unread").getList().then(function(response) {
                    $rootScope.unread = response.plain();
                });
            }
        }
    ])
    .controller('HeaderController', ['$scope', '$state', function($scope, $state) {
        $scope.goTo = function(state, params) {
            $state.go(state, params, {
                reload: true,
                inherit: true,
                notify: true
            });
            $scope.query = null;
        }
    }])
    .controller('AppController', ['$scope', '$localStorage', '$window', '$state', '$stateParams', '$rootScope', '$auth',
        function($scope, $localStorage, $window, $state, $stateParams, $rootScope, $auth) {

            // Config
            $scope.app = {
                name: 'Workstation',
                version: '1.0.0'
            }

            // Filter function
            $scope.clearFilters = function() {
                $state.go($state.current, {}, {
                    reload: true,
                    inherit: false
                })
            }

            $scope.filter = function(attribute, value, options) {
                var options = options || {};
                var params = _.clone($stateParams) || {};
                if (attribute && value) {
                    if (options.date) {
                        value = new Date(value).toISOString();
                    }
                    params[attribute] = value;
                }
                if (attribute && !value) params[attribute] = undefined;
                if (params.page) params = _.omit(params, 'page');
                $state.go($state.current, params, {
                    reload: true,
                    inherit: true,
                    notify: true
                })
            }

            $scope.goToPage = function(page) {
                var params = _.clone($stateParams) || {};
                params.page = page;
                $state.go($state.current, params, {
                    reload: true,
                    inherit: true,
                    notify: true
                })
            }

            $rootScope.getThumbnail = function(url) {
                if (!url) return;
                // var index = url.indexOf("upload/") + 7;
                // var thumb = url.slice(0, index) + 'g_face,c_thumb,w_200,h_200/' + url.slice(index + Math.abs(0));
                // thumb = thumb.replace(/^http:\/\//i, 'https://');
                return url.replace(/^http:\/\//i, 'https://');
            }

            //Logout
            $scope.logout = function() {
                $auth.signOut()
                    .then(function(resp) {
                        $state.go("access.login");
                    })
                    .catch(function(resp) {
                        // handle error response
                    });
            }

            // Graph Options
            var fontFamily = '"proxima_nova", Helvetica, Arial, sans-serif';
            $scope.graphOptions = {
                scaleBeginAtZero: true,
                scaleShowVerticalLines: false,
                scaleShowLabels: false,
                responsive: true,
                maintainAspectRatio: false,
                bezierCurve: false,
                tooltipFontFamily: fontFamily,
                scaleFontFamily: fontFamily,
                pointDotRadius: 5,
                pointDotStrokeWidth: 2,
                colours: ['#2cc36b', '#E74C3C'],
                multiTooltipTemplate: function(label) {
                    return label.datasetLabel + ': ' + "N" + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                tooltipTitleFontFamily: fontFamily,
                tooltipTitleFontStyle: "normal",
                tooltipCornerRadius: 3,
                tooltipXPadding: 10,
                tooltipYPadding: 10,
            }


            // Override Primitives
            String.prototype.trunc =
                function(n) {
                    return this.substr(0, n - 1) + (this.length > n ? '…' : '');
                };

            String.prototype.capitalizeFirstLetter = function() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            }
        }
    ])
    .factory('BearerAuthInterceptor', ['$window', 'DEFAULTS', function($window, DEFAULTS) {
        return {
            request: function(config, data) {
                config.headers = config.headers || {};
                if (config.url.indexOf("workstation") > -1) {
                    config.headers.client_token = DEFAULTS.clientToken;
                }
                return config;
            }
        };
    }]);

angular.module('ui.controllers', [])
.controller('NetworkController', ['$state', '$scope',
    function($state, $scope) {}
]);

angular.module('ui.directives', [])
    .directive('badge', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<span class="badge text-white text-capitalize" ng-class="label_class">{{status}}</span>',
            link: function($scope, element, attrs, controller) {
                $scope.status = attrs["status"] || "Unknown";
                $scope.label_class = attrs["class"] || "";
                switch ($scope.status) {
                    case "pending":
                        $scope.label_class += " btn-info"
                        break;
                    case "unpaid":
                        $scope.label_class += " btn-info"
                        break;
                    case "approved":
                        $scope.label_class += " btn-success"
                        break;
                    case "paid":
                        $scope.label_class += " btn-success"
                        break;
                    case "failed":
                        $scope.label_class += " btn-danger"
                        break;
                    case "active":
                        $scope.label_class += " btn-success"
                        break;
                    case "inactive":
                        $scope.label_class += " btn-danger"
                        break;
                    case "expired":
                        $scope.label_class += " btn-danger"
                        break;
                    default:
                        $scope.label_class += " btn-info"
                        break;
                }
            }
        };
    }).directive('status', function() {
        return {
            restrict: 'A',
            link: function($scope, element, attrs, controller) {
                var status = attrs["status"] ? attrs["status"].toLowerCase() : "Unknown";

                var success_states = ['verified', 'paid', 'active', 'success', 'subscribed', 'approved', 'resolved', 'live', 'available'];
                var failure_states = ['failed', 'unpaid', 'inactive', 'unverified', 'unsubscribed', 'expired', 'archived', 'pending', 'reversed', 'busy'];
                var test_states = ['test']

                if (_.contains(success_states, status)) {
                    element.addClass("text-success");
                } else if (_.contains(failure_states, status)) {
                    element.addClass("text-danger");
                } else if (_.contains(test_states, status)) {
                    element.addClass("text-muted");
                } else {
                    element.addClass("text-info");
                }
            }
        };
    }).directive('ngLoading', [
        function() {
            //directive to show loading state
            return {
                restrict: 'AE',
                scope: true,
                compile: function(tElem, attrs) {
                    //Add the controls to element
                    tElem.addClass('loading-button');
                    var buttonContent = tElem.html();
                    tElem.html("<span class=\"default-state\">" + buttonContent + "</span>");
                    tElem.append("<div class=\"loading-state spinner\">\r\n  <div class=\"b1 se\"><\/div>\r\n  <div class=\"b2 se\"><\/div>\r\n  <div class=\"b3 se\"><\/div>\r\n  <div class=\"b4 se\"><\/div>\r\n  <div class=\"b5 se\"><\/div>\r\n  <div class=\"b6 se\"><\/div>\r\n  <div class=\"b7 se\"><\/div>\r\n  <div class=\"b8 se\"><\/div>\r\n  <div class=\"b9 se\"><\/div>\r\n  <div class=\"b10 se\"><\/div>\r\n  <div class=\"b11 se\"><\/div>\r\n  <div class=\"b12 se\"><\/div>\r\n<\/div><span class=\"loading-success\"><i class=\"fa fa-check animated fadeInUp\"><\/i><\/span><span class=\"loading-failure\"><i class=\"fa fa-times animated fadeInUp\"><\/i><\/span>");
                    return function(scope, element, attrs) {
                        var watching;

                        var showLoading = function(val) {
                            element.addClass('ng-loading');
                            element.attr('disabled', true);
                            watching = true;
                        }

                        var clearLoading = function() {
                            element.removeClass('ng-loading-success ng-loading-failure ng-loading');
                            element.attr('disabled', false);
                        }

                        scope.$on(attrs.ngLoading, function(event, val) {
                            if (!watching) return;
                            watching = false;
                            element.removeClass('ng-loading');

                            if (val) {
                                if (val === true) element.addClass('ng-loading-success')
                                if (val === false) element.addClass('ng-loading-failure');
                                setTimeout(clearLoading, 700);
                            } else {
                                clearLoading();
                            }
                        });

                        element.on('click', function() {
                            element.addClass('ng-loading');
                            showLoading();
                        })
                    };
                }
            };
        }
    ])
    .directive('autofocus', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        }
    }])
    .directive("fileread", [function() {
        return {
            link: function(scope, element, attrs) {
                element.bind('change', function(event) {
                    scope.$apply(function() {
                        scope[attrs.fileread] = event.target.files[0];
                    });
                });

                scope.$watch(attrs.fileread, function(fileread) {
                    if (!fileread) element.val("");
                });
            }
        }
    }])
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('triggerClick', function() {
        return function(scope, element, attrs) {
            element.bind("click", function(event) {
                var id = attrs['triggerClick'];
                var input = document.getElementById(id);
                input.click();
            });
        };
    })
    .directive('closeOutsideClick', function($window) {
        return function(scope, element, attrs) {
            var trigger = attrs['closeOutsideClick'];

            var handler = function(event) {
                if (!div.contains(event.target)) {
                    scope.$apply(function() {
                        scope[trigger] = false;
                    });

                    document.removeEventListener('click', handler, false);
                }
            };

            var div = angular.element(element)[0];
            scope.$watch(trigger, function(visible) {
                if (visible) document.addEventListener('click', handler, false);
            })
        };
    })
    .directive("whenScrolled", function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                var didScroll = false;
                window.onscroll = function() {
                    didScroll = true;
                }

                setInterval(function() {
                    if (didScroll) {
                        didScroll = false;
                        var bottomOfPage = (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight;
                        if (bottomOfPage) {
                            scope.$apply(attrs.whenScrolled);
                        }
                    }
                }, 100);
            }
        }
    });

angular.module('ui.directives')
    .directive("cmdEnter", function() {
        function b(a, b, c, d) {
            function e() {
                for (var a = b.parent(); a.length && "FORM" !== a[0].tagName;) a = a.parent();
                return a }

            function f(a) { 13 === a.which && a.metaKey && (a.preventDefault(), c.bnCmdEnter ? g(a) : h()) }

            function g(b) { a.$apply(function() { a.$eval(c.bnCmdEnter, { $event: b }) }) }

            function h() { e().triggerHandler("submit") }(c.bnCmdEnter || d) && b.on("keydown", f) }
        return { link: b, require: "^?form", restrict: "A" } });

angular.module('app.apiFactories', ['restangular'])

.factory('$LocalAPI', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl('/api');
    });
}])


.factory('$API', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(DEFAULTS.baseAPIUrl);
    });
}])

.factory('$FullAPI', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(DEFAULTS.baseAPIUrl);
        RestangularConfigurer.setFullResponse(true);
    });
}]);

angular.module('app.authService', [])
    .service('Session', ['$rootScope', '$window', '$q', '$API', 'DEFAULTS',
        function($rootScope, $window, $q, $API, DEFAULTS) {
            function pad(num, size) {
                var s = num + "";
                while (s.length < size) s = "0" + s;
                return s;
            }

            function create(user) {
                var deferred = $q.defer();

                user.isAdmin = DEFAULTS.config === "admin";
                user.member_id = pad(user.id, 7);

                if (user.group_plan_subscription && user.subscription) {
                    var group_plan_started = new Date(user.group_plan_subscription.created_at);
                    var regular_plan_started = new Date(user.subscription.created_at);
                    var group_plan_started_later = moment(group_plan_started).isAfter(regular_plan_started);

                    if (group_plan_started_later) {
                        user.subscription = user.group_plan_subscription;
                        user.has_office = true;
                    } else {
                        user.group_plan_subscription = null;
                    }
                }

                if (user.group_plan_subscription && !user.subscription) {
                    user.subscription = user.group_plan_subscription;
                    user.has_office = true;
                }

                if (user.subscription && !user.subscription.enabled) {
                    user._subscription = user.subscription;
                    user.subscription = null;
                }

                if (user.subscription) {
                    fetchPlan(user);
                }

                if (user.team_id != null) {
                    user.has_team = true;
                    user.is_team_admin = user.team_admin;
                    fetchTeam(user);
                }

                deferred.resolve(user);
                return deferred.promise;
            }

            function fetchPlan(user) {
                var plans_url = user.has_office ? 'group_plans' : 'plans';
                var plan_id = user.has_office ? 'group_plan_id' : 'plan_id';

                return $API.one(plans_url, 'retrieve_plan').get({
                        id: user.subscription[plan_id]
                    })
                    .then(function(plan) {
                        $rootScope.User.plan = plan.plain();
                    });
            }

            function fetchTeam(user) {
                $API.one('teams', user.team_id).one('members').get()
                    .then(function(team) {
                        var team_registration = _.find(team.members, function(member) {
                            return member.user.id == user.id
                        });
                        $rootScope.User.team_registration = _.omit(team_registration, 'user');
                    });
            }

            return {
                create: create,
                update: function(response) {
                    var deferred = $q.defer();

                    $API.one('users', 'current_session').get()
                        .then(create)
                        .then(function(user) {
                            $rootScope.User = user;
                            deferred.resolve(response);
                        })
                        .catch(function() {
                            setTimeout(function() { $window.location.reload() }, 500);
                            deferred.resolve(response);
                        });

                    return deferred.promise;
                },
                fetchPlan: fetchPlan,
                fetchTeam: fetchTeam
            }
        }
    ])
    .service('Subscription', ['$rootScope', '$q', '$API', 'Session',
        function($rootScope, $q, $API, Session) {
            function checkRestriction(ability) {
                var user = $rootScope.User;
                var restriction = $q.defer();

                if (!user.subscription || !user.subscription.enabled) {
                    restriction.reject("No current subscription");
                    return restriction.promise;
                }

                user.subscription_expired = moment(user.subscription.end_date).isBefore(moment());
                if (user.subscription_expired) {
                    $API.all('subscriptions/cancel').post().then(function() {
                        var message = "Current subscription expired";
                        restriction.reject(message);
                    });
                } else if (ability && !user[ability]) {
                    restriction.reject("Current subscription does not allow this feature");
                } else {
                    restriction.resolve();
                }

                if (user.can_message) $rootScope.checkUnread();
                return restriction.promise;
            }

            return {
                verify: function(ability) {
                    var deferred = $q.defer();

                    if ($rootScope.User) {
                        checkRestriction(ability).then(function() {
                            deferred.resolve();
                        }, function(reason) {
                            deferred.reject(reason);
                        });
                    } else {
                        $rootScope.$on('auth:validate-subscription', function(ev, user) {
                            checkRestriction(ability).then(function() {
                                deferred.resolve();
                            }, function(reason) {
                                deferred.reject(reason);
                            });
                        });
                    }

                    return deferred.promise;
                }
            }
        }
    ]);

angular.module('app.historyService', [])
    .run(['$rootScope', '$location', '$state', '$stateParams', 'DEFAULTS', 'toaster',
        function($rootScope, $location, $state, $stateParams, DEFAULTS, toaster) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                // Save previous state 
                $rootScope.history = $rootScope.history || [];

                if (toState != fromState && fromState.name) {
                    if ($rootScope.usedBackButton) {
                        $rootScope.usedBackButton = false;
                    } else {
                        var recentHistory = _.last($rootScope.history);
                        if (!recentHistory || (recentHistory.state != fromState)) {
                            $rootScope.history.push({
                                state: fromState,
                                params: fromParams
                            })
                        }
                        if ($rootScope.history.length > 10) $rootScope.history.shift();
                    }
                }

                String.prototype.capitalize = function() {
                    return this.charAt(0).toUpperCase() + this.slice(1);
                }
            })

            // Back function
            $rootScope.previousState = function() {
                var recentHistory = _.last($rootScope.history);
                $rootScope.history = _.without($rootScope.history, recentHistory);
                $rootScope.usedBackButton = true;

                var previousState, previousStateParams;

                if (recentHistory) {
                    previousState = recentHistory.state;
                    previousStateParams = recentHistory.params;
                } else {
                    var currentState = $state.current.name.split('.');
                    if (_.contains(currentState, 'subscriptions')) {
                        var currentStateParent = _.first(currentState, currentState.length - 1).join('.');
                    } else {
                        var currentStateParent = _.first(currentState, currentState.length - 2).join('.');
                    }
                    previousState = currentStateParent;
                }

                $state.go(previousState, previousStateParams);
            }
        }
    ]);

angular.module('app.localService', [])
    .factory('LocalService', function () {
        return {
            get: function (key) {
                return localStorage.getItem(key);
            },
            set: function (key, val) {
                return localStorage.setItem(key, val);
            },
            unset: function (key) {
                return localStorage.removeItem(key);
            }
        }
    });
angular.module('app.notificationService', ['toaster'])
    .factory('Notification', ['toaster', function(toaster) {
        return {
            pop: function(options) {
                toaster.pop(options);
            },
            success: function(title, body) {
                toaster.pop({
                    type: 'success',
                    title: title || "Action completed succesfully",
                    body: body
                });
            },
            error: function(title, error) {
                var message;

                if (typeof error == "string") {
                    message = error;
                }

                console.log(error)

                if (typeof error == "object") {
                    if (error.data) {
                        message = error.data.errors;
                        if (message && message.hasOwnProperty('full_messages')) message = message.full_messages;
                    } else message = error.errors;

                    title = error.reason || title;
                }

                if (Array.isArray(message)) {
                    message = message[0];
                }
                
                toaster.pop({
                    type: 'error',
                    title: title || "Action could not be perfomed",
                    body: message  || "Unknown error occured"
                });
            },
            clear: function() {
                toaster.clear();
            }
        }
    }]);

angular.module('app.uiService', ['restangular'])
    .factory('CalendarHelper', [function() {
        return {
            renderTooltip: function(jsEvent) {
                var overlay = $('.fc-overlay');
                overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');

                var wrap = $(jsEvent.target).closest('.fc-event');
                var cal = wrap.closest('.ui-calendar');
                var left = wrap.offset().left - cal.offset().left;
                var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
                var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
                if (right > overlay.width()) {
                    overlay.addClass('left').find('.arrow').addClass('left pull-up')
                } else if (left > overlay.width()) {
                    overlay.addClass('right').find('.arrow').addClass('right pull-up');
                } else {
                    overlay.find('.arrow').addClass('top');
                }
                if (top < overlay.height()) {
                    overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down')
                }
                (wrap.find('.fc-overlay').length == 0) && wrap.append(overlay);
            }
        }
    }])
    .factory('DatepickerHelper', [function() {
        return {
            triggerClick: function() {
                var body = document.getElementsByTagName("body")[0];
                body.click();
            }
        }
    }])
    .service('FileHelper', ['$http', function($http) {
        this.upload = function(file, preset) {
            var cloud_name = 'dcgsxfgaq';
            var upload_preset = preset || 'fileUpload';

            var fd = new FormData();
            fd.append('file', file);
            fd.append('upload_preset', upload_preset);

            return $http.post('https://api.cloudinary.com/v1_1/' + cloud_name + '/upload', fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        'X-Requested-With': 'XMLHttpRequest',
                        'If-Modified-Since': undefined
                    }
                })
        }
    }])

angular.module('app.access', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('access', {
                url: '',
                template: '<div ui-view></div>',
                abstract: true,
                redirectTo: 'access.login',
                resolve: {
                    auth: function($auth, $state, Notification, DEFAULTS) {
                        return $auth.validateUser().then(function(response) {
                            $state.go(DEFAULTS.home);
                        }).catch(function() {
                            // Continue
                        })
                    }
                },
                access: 'public'
            })
            .state('access.login', {
                url: '/login?account_confirmation_success&next',
                templateUrl: 'modules/access/login.html',
                controller: 'LoginController',
                access: 'public'
            })
            .state('access.signup', {
                url: '/signup?plan',
                templateUrl: 'modules/access/signup.html',
                controller: 'SignupController',
                access: 'public',
                resolve: {
                    dial_codes: ['$LocalAPI', function($LocalAPI) {
                        return $LocalAPI.all('dial_codes').getList();
                    }]
                }
            })
            .state('access.activate', {
                url: '/activate',
                templateUrl: 'modules/access/activate.html',
                controller: 'ActivateController',
                resolve: {
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                },
                access: 'public'
            })
            .state('access.forgot-password', {
                url: '/forgot-password',
                templateUrl: 'modules/access/forgot-password.html',
                controller: 'LoginController',
                access: 'public'
            })
            .state('access.reset-password', {
                url: '/reset-password',
                templateUrl: 'modules/access/reset-password.html',
                controller: 'LoginController',
                access: 'public'
            });
    }
])

.controller('LoginController', ['$scope', '$rootScope', '$state', '$auth', 'Notification', 'DEFAULTS',
    function($scope, $rootScope, $state, $auth, Notification, DEFAULTS) {
        $scope.credentials = {};

        $scope.login = function() {
            $auth.submitLogin({
                email: $scope.credentials.email,
                password: $scope.credentials.password
            }).then(function(response) {
                if ($state.params.next && $rootScope.afterLogin) {
                    window.location.href = $rootScope.afterLogin;
                } else $state.go(DEFAULTS.home);
            }).catch(function(response) {
                $rootScope.$broadcast('loggingIn', false);
                Notification.error('Could not Login', response);
            });
        }

        $scope.requestReset = function() {
            $auth.requestPasswordReset({
                email: $scope.resetEmail
            }).then(function(response) {
                $scope.resetEmailSent = true;
            }).catch(function(response) {
                $rootScope.$broadcast('requestingReset', false);
                Notification.error('Password reset failed', response);
            });
        }

        $scope.changePassword = function() {
            $auth.updatePassword({
                password: $scope.password,
                password_confirmation: $scope.password_confirmation
            }).then(function() {
                Notification.success("All Done!", "Your password has been updated");
                window.location = window.location.protocol + "//" + window.location.hostname;
            }, function(response) {
                $rootScope.$broadcast('changingPassword', false);
                Notification.error("Could not change your password", response);
            });
        }
    }
])

.controller('SignupController', ['$scope', '$http', '$state', '$auth', 'Notification', 'LocalService', 'dial_codes', 'DEFAULTS',
    function($scope, $http, $state, $auth, Notification, LocalService, dial_codes, DEFAULTS) {
        $scope.credentials = {};

        $scope.dial_codes = dial_codes;
        $scope.setAreaCode = function(selected) {
            if (!selected) return;

            var country = selected.originalObject;
            $scope.selected_country = country;
        }

        if ($state.params.plan) {
            LocalService.set("workstation-selected-plan", $state.params.plan);
        }

        $scope.signup = function(credentials) {
            var phone_number = credentials.phone;
            if (phone_number.charAt(0) == '0') phone_number = phone_number.substring(1);
            phone_number = $scope.selected_country.dial_code + phone_number;

            $auth.submitRegistration({
                first_name: credentials.first_name.capitalizeFirstLetter(),
                last_name: credentials.last_name.capitalizeFirstLetter(),
                phone: phone_number,
                email: credentials.email,
                password: credentials.password,
                password_confirmation: credentials.password
            }).then(function(response) {
                $scope.signedUp = true;
                $scope.login(credentials);
            }).catch(function(response) {
                $scope.$broadcast('signedUp', false);
                Notification.error('Could not create your account', response);
            });
        }

        $scope.login = function(credentials) {
            $auth.submitLogin({
                email: credentials.email,
                password: credentials.password
            }).then(function(response) {
                $state.go(DEFAULTS.home)
            }).catch(function(response) {
                $state.go('access.login');
                Notification.error('Could not Login', response);
            });
        }
    }
])

.controller('ForgotPasswordController', ['$scope', '$http', '$state', '$auth', 'Notification',
    function($scope, $http, $state, $auth, Notification) {}
])

.controller('ActivateController', ['$scope', '$http', '$state', '$auth', 'Notification', 'locations',
    function($scope, $http, $state, $auth, Notification, locations) {
        $scope.locations = locations;
    }
]);

angular.module('app.admin', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin', {
                url: '/admin',
                templateUrl: 'components/layouts/admin.html',
                redirectTo: 'admin.home',
                access: 'admin',
                resolve: {
                    auth: function($auth, $state, Notification) {
                        return $auth.validateUser()
                            .catch(function(response) {
                                Notification.error(response.reason, response);
                                $state.go('access.login');
                            });
                    },
                    locations: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.all('locations').getList().then(function(response) {
                            $rootScope.locations = response.plain();
                        });
                    }]
                }
            })
            .state('admin.messages', {
                url: '/messages?new',
                title: 'Messages',
                templateUrl: 'modules/common/messages/index.html',
                controller: 'MessagesController',
                fullHeight: true,
                access: 'admin',
                resolve: {
                    conversations: ['$API', function($API){
                        return $API.all('messaging/conversations').getList();
                    }],
                    unread: ['$API', function($API){
                        return $API.all('messaging/unread').getList();
                    }]
                }
            })
            .state('admin.messages.one', {
                url: '/:id',
                title: 'Messages',
                templateUrl: 'modules/common/messages/conversation.html',
                controller: 'ConversationController',
                access: 'admin',
                fullHeight: true,
                resolve: {
                    receipts: ['$API', '$stateParams', function($API, $stateParams){
                        return $API.all('messaging/receipts/' + $stateParams.id).getList();
                    }]
                }
            })
            .state('admin.bookings', {
                url: '/bookings',
                title: 'Bookings',
                templateUrl: 'modules/common/bookings/index.html',
                controller: 'BookingsController',
                access: 'admin',
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/ui-calendar/';
                        var files = [];
                        files.push(folder + 'jquery.min.js');
                        files.push(folder + 'fullcalendar.min.js');
                        files.push(folder + 'fullcalendar.min.css');
                        files.push(folder + 'fullcalendar.theme.css');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList();
                    }]
                }
            })
            .state('admin.events', {
                url: '/events',
                title: 'Events',
                templateUrl: 'modules/common/events/index.html',
                controller: 'EventsController',
                access: 'admin',
                resolve: {
                    events: ['$API', function($API) {
                        return $API.all('events').getList();
                    }]
                }
            })
    }
])

angular.module('app.common', ['ui.router']);
angular.module('app.user', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user', {
                templateUrl: 'components/layouts/user.html',
                redirectTo: 'user.home',
                resolve: {
                    auth: function($auth, $state, $rootScope, $location, Notification) {
                        return $auth.validateUser()
                            .catch(function(response) {
                                Notification.error(response.reason, response);
                                $rootScope.afterLogin = window.location.hash;
                                $state.go("access.login", { next: true });
                            });
                    },
                    locations: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.all('locations').getList().then(function(response) {
                            $rootScope.locations = response.plain();
                        });
                    }]
                }
            })
            .state('user.messages', {
                url: '/messages?new',
                title: 'Messages',
                templateUrl: 'modules/common/messages/index.html',
                controller: 'MessagesController',
                fullHeight: true,
                resolve: {
                    subscription: ['$state', 'Subscription', function($state, Subscription) {
                        return Subscription.verify('can_message').catch(function() {
                            $state.go('user.home');
                        });
                    }],
                    conversations: ['$API', function($API) {
                        return $API.all('messaging/conversations').getList();
                    }],
                    unread: ['$API', function($API) {
                        return $API.all('messaging/unread').getList();
                    }]
                }
            })
            .state('user.messages.one', {
                url: '/:id',
                title: 'Messages',
                templateUrl: 'modules/common/messages/conversation.html',
                controller: 'ConversationController',
                fullHeight: true,
                resolve: {
                    receipts: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.all('messaging/receipts/' + $stateParams.id).getList();
                    }]
                }
            })
            .state('user.bookings', {
                url: '/bookings',
                title: 'Bookings',
                templateUrl: 'modules/common/bookings/index.html',
                controller: 'BookingsController',
                resolve: {
                    subscription: ['$state', 'Subscription', function($state, Subscription) {
                        return Subscription.verify('can_book_rooms').catch(function() {
                            $state.go('user.home');
                        });
                    }],
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/ui-calendar/';
                        var files = [];
                        files.push(folder + 'jquery.min.js');
                        files.push(folder + 'fullcalendar.min.js');
                        files.push(folder + 'fullcalendar.min.css');
                        files.push(folder + 'fullcalendar.theme.css');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList();
                    }]
                }
            })
            .state('user.events', {
                url: '/events',
                title: 'Events',
                templateUrl: 'modules/common/events/index.html',
                controller: 'EventsController',
                resolve: {
                    events: ['$API', function($API) {
                        return $API.all('events').getList();
                    }]
                }
            })
    }
]);

angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.home', {
                url: '?feed',
                templateUrl: 'modules/admin/home/index.html',
                controller: 'AdminHomeController',
                skeleton: true,
                access: 'admin',
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/getstream/';
                        var files = [];

                        files.push(folder + 'getstream.js');
                        files.push(folder + 'stream-angular.js');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    categories: ['$API', function($API) {
                        return $API.all('feed_categories').getList();
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList().then(function(bookings) {
                            return _.filter(bookings, function(booking) {
                                return moment(booking.start_time).isSame(new Date(), 'day');
                            });
                        });
                    }]
                }
            })
            .state('admin.post', {
                url: '/post/:id',
                templateUrl: 'modules/admin/home/post.html',
                controller: 'SinglePostController',
                access: 'admin',
                resolve: {
                    post: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.one('posts', $stateParams.id).get();
                    }]
                }
            })
    }
])

.controller('AdminHomeController', ['$scope', '$rootScope', '$state', '$FullAPI', 'ngDialog', 'bookings', 'categories',
    function($scope, $rootScope, $state, $FullAPI, ngDialog, bookings, categories) {
        $scope.bookings = bookings;
        $scope.categories = categories;
        $scope.activeCategory = _.find(categories, function(cat) {
            return cat.id == $state.params.feed
        });

        if (!$rootScope.member_count) {
            $rootScope.member_count = {};

            $FullAPI.all('users').getList({ status: 'active' }).then(function(members) {
                $rootScope.member_count.active = members.headers("total");
            });
            $FullAPI.all('users').getList().then(function(members) {
                $rootScope.member_count.total = members.headers("total");
            });
        }

        $scope.switchTo = function(category) {
            $state.go($state.current.name, {
                feed: category.id
            }, { reload: true });
        }

        $scope.manageCategories = function() {
            ngDialog.openConfirm({
                templateUrl: "ManageSources",
                controller: "CategoriesController",
                data: {
                    categories: categories
                }
            }).then(function() {
                $state.reload();
            });
        }

        $scope.shareLink = function() {
            ngDialog.open({
                templateUrl: "ShareLink",
                controller: "ShareLinkController",
                data: {
                    categories: categories
                }
            });
        }
    }
])

.controller('SinglePostController', ['$scope', '$rootScope', '$stateParams', '$API', 'post', 'Notification',
    function($scope, $rootScope, $stateParams, $API, post, Notification) {

        $scope.formatPost = function(post) {
            post._id = post.id;
            post.comment_body = post.body;
            post.post_body = post.body;
            post.time = post.created_at;
            post.foreign_id = "Post:" + post.id;

            post.feedable = post.feedable || {};
            post.actor_id = post.feedable_type + "-" + post.feedable.id;
            post.actor_type = post.feedable_type;
            post.actor_email = post.feedable.email;
            post.actor_first_name = post.feedable.first_name;
            post.actor_last_name = post.feedable.last_name;
            post.actor_image = post.feedable.image;
            post.actor_bio = post.feedable.bio;

            var location = _.findWhere($rootScope.locations, { id: post.feedable.home_location_id }) || {};
            post.actor_location = location.description;

            return post;
        }

        post.comments = _.map(post.comments, function(comment) {
            return $scope.formatPost(comment)
        });

        $scope.post = $scope.formatPost(post);

        $scope.delete = function(post, index) {
            $API.one('posts', post._id).remove().then(function() {
                Notification.success("Post deleted", "User's post has been removed");
                $state.go('admin.home');
            }).catch(function(response) {
                Notification.error('Unable to delete post', response);
            });
        }
    }
])

.controller('CategoriesController', ['$scope', '$rootScope', '$state', '$API',
    function($scope, $rootScope, $state, $API) {
        $scope.categories = $scope.ngDialogData.categories;

        $scope.add = function(category_name) {
            $API.all('feed_categories').post({
                name: category_name
            }).then(function(response) {
                $scope.category_name = null;
                $scope.categories.unshift(response);
                $scope.$broadcast("addedCategory", true);
            }, function() {
                Notification.error('Unable to create category', response);
            });
        }

        $scope.update = function(category) {
            category.save().then(function() {
                $scope.$broadcast("updatedCategory", true);
            }, function() {
                Notification.error('Unable to update category', response);
            });
        }

        $scope.delete = function(category) {
            category.remove().then(function() {
                $scope.categories = _.without($scope.categories, category);
                $scope.$broadcast("deletedCategory", true);
            }, function() {
                Notification.error('Unable to delete category', response);
            });
        }
    }
])

.controller('ShareLinkController', ['$scope', '$rootScope', '$state', 'Restangular', '$API', 'Notification',
    function($scope, $rootScope, $state, Restangular, $API, Notification) {
        $scope.categories = $scope.ngDialogData.categories;
        $scope.post = {};

        $scope.share = function() {
            $API.all('posts').post($scope.post).then(function(response) {
                $scope.closeThisDialog();
            }, function(response) {
                Notification.error('Unable to post to feed', response);
                $scope.$broadcast("linkShared", false);
            });
        }
    }
])

angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.members', {
                url: '/members?search&status&page',
                templateUrl: 'modules/admin/members/index.html',
                skeleton: true,
                controller: 'AdminMembersController',
                access: 'admin',
                resolve: {
                    members: ['$FullAPI', '$rootScope', '$stateParams', function($FullAPI, $rootScope, $stateParams) {
                        if ($stateParams.search) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else {
                            return $FullAPI.all('users').getList($stateParams);
                        }
                    }]
                }
            })
            .state('admin.member', {
                url: '/member/:id',
                templateUrl: 'modules/admin/members/one.html',
                skeleton: true,
                access: 'admin',
                controller: 'AdminMemberController',
                resolve: {
                    member: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.one('users', $stateParams.id).get();
                    }],
                    plans: ['MemberSubscription', '$API', 'member', function(MemberSubscription, $API, member) {
                        if (MemberSubscription.check(member).has_office) {
                            return $API.all('group_plans').getList();
                        } else if (member.subscription) {
                            return $API.all('plans').getList();
                        } else {
                            return []
                        }
                    }]
                }
            })
    }
])

.service('MemberSubscription', [function() {
    return {
        check: function(member, plans) {
            function pad(num, size) {
                var s = num + "";
                while (s.length < size) s = "0" + s;
                return s;
            }

            member.member_id = pad(member.id, 7);
            member.has_team = member.team_id != null;
            member.is_team_admin = member.team_admin;

            if (member.group_plan_subscription && member.subscription) {
                var group_plan_started = new Date(member.group_plan_subscription.created_at);
                var regular_plan_started = new Date(member.subscription.created_at);
                var group_plan_started_later = moment(group_plan_started).isAfter(regular_plan_started);

                if (group_plan_started_later) {
                    member.subscription = member.group_plan_subscription;
                    member.has_office = true;
                } else {
                    member.group_plan_subscription = null;
                }
            }

            if (member.group_plan_subscription && !member.subscription) {
                member.subscription = member.group_plan_subscription;
                member.has_office = true;
            }

            member.active_subscription = member.subscription && member.subscription.enabled && !member.is_owing;

            return member;
        }
    }
}])

.controller('AdminMembersController', ['$scope', '$rootScope', '$state', '$API', '$FullAPI', 'LocalService', 'MemberSubscription', 'Notification', 'ngDialog', 'members',
    function($scope, $rootScope, $state, $API, $FullAPI, LocalService, MemberSubscription, Notification, ngDialog, members) {

        function setMembers(response) {
            var _members = response || members;
            $scope.members = _.map(_members.data, function(member) {
                return MemberSubscription.check(member);
            });

            $scope.meta = {
                total: _members.headers("total"),
                perPage: 25,
                page: $state.params.page,
                pageCount: Math.round(_members.headers("total") / 25)
            }
        }

        setMembers(members);

        $scope.find = function(query) {
            $scope.searching = true;
            $FullAPI.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.searching = false;
                setMembers(response);
            }, function() {
                $scope.searching = false;
            });
        }

        $scope.message = function(member) {
            LocalService.set('workstation-new-conversation', JSON.stringify(member));
            $state.go('admin.messages', { new: member.email });
        }

        $scope.reload = function(params) {
            $state.go($state.current, params, {
                reload: true,
                inherit: false,
                notify: true
            })
        }

        $scope.goToUser = function(user_id) {
            var id = parseInt(user_id, 10);
            $state.go("admin.member", { id: id });
        }
    }
])

.controller('AdminMemberController', ['$scope', '$rootScope', '$state', '$window', '$API', '$FullAPI', 'LocalService', 'MemberSubscription', 'Notification', 'ngDialog', 'member', 'plans',
    function($scope, $rootScope, $state, $window, $API, $FullAPI, LocalService, MemberSubscription, Notification, ngDialog, member, plans) {
        $scope.member = MemberSubscription.check(member.plain());

        $scope.subscription = $scope.member.subscription || {};
        if ($scope.subscription.days) $scope.subscription.days = JSON.parse($scope.subscription.days);
        if ($scope.member.subscription) {
            var plan_id = $scope.member.has_office ? "group_plan_id" : "plan_id";
            $scope.member.plan = _.findWhere(plans, { id: $scope.subscription[plan_id] });
        }

        $scope.message = function() {
            LocalService.set('workstation-new-conversation', JSON.stringify($scope.member));
            $state.go('admin.messages', { new: $scope.member.email });
        }

        $scope.load = function(resource, page, callback) {
            var page = page || 1;

            var url_promise;
            if (resource === "team") {
                url_promise = $FullAPI.one('teams/' + member.team_id, 'members').get();
            } else {
                url_promise = $FullAPI.one('users', member.id).all(resource).getList();
            }

            url_promise.then(function(response) {
                var meta = {
                    total: response.headers("total"),
                    perPage: 25,
                    page: page,
                    pageCount: Math.round(response.headers("total") / 25)
                }
                $scope[resource] = response.data;
                $scope[resource].meta = meta;

                if (callback) callback(resource, page, response);
            })
        }

        $scope.checkAddons = function(resource, page, response) {
            if (page === 1) {
                var addons = _.filter($scope.purchases, function(purchase) {
                    return purchase.active && purchase.add_on && !purchase.add_on.system;
                });
                $scope.addons = _.map(addons, function(purchase) {
                    return purchase.add_on.name
                });
            }
        }

        $scope.addTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddTokensController"
            });
        }

        $scope.addPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddPrintingController"
            });
        }

        $scope.addAddon = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-addon.html",
                resolve: {
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddAddonController"
            });
        }

        $scope.cancelSubscription = function() {
            ngDialog.openConfirm({
                template: "CancelSubscription"
            }).then(function(reason) {
                var SubscriptionCancel;

                if ($scope.member.has_office) {
                    SubscriptionCancel = $API.all('group_plan_subscriptions/cancel').post({ reason: reason, user_id: $scope.member.id });
                } else {
                    SubscriptionCancel = $API.one('users', $scope.member.id).all('cancel').post({ reason: reason });
                }

                SubscriptionCancel.then(function() {
                    Notification.success('Subscription cancelled', "User's current subscription has been cancelled");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('cancelledSubscription', false);
                    Notification.error("Could not cancel subscription", response);
                });
            });
        }

        $scope.subscribe = function() {
            ngDialog.open({
                templateUrl: "/modules/admin/members/modals/subscribe.html",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AdminSubscriptionController",
                width: 600
            });
        }

        $scope.subscribeOffice = function() {
            ngDialog.open({
                templateUrl: "/modules/admin/members/modals/subscribe-office.html",
                resolve: {
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AdminOfficeSubscriptionController",
                width: 600
            });
        }

        $scope.suspend = function(reason) {
            $FullAPI.one('users', member.id).all("suspend").post({
                reason: reason
            }).then(function() {
                Notification.success('User suspended', "Access to the application has been revoked");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userSuspended', false);
                Notification.error("Could not suspend user", response);
            });
        }

        $scope.unsuspend = function(reason) {
            $FullAPI.one('users', member.id).all("unsuspend").post({
                reason: reason
            }).then(function() {
                Notification.success('User unsuspended', "Access to the application has been restored");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userUnsuspended', false);
                Notification.error("Could not unsuspend user", response);
            });
        }

        $scope.changeDays = function() {
            ngDialog.openConfirm({
                templateUrl: "ChangeDays",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: 'AdminSubscriptionController'
            });
        }

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }

        $scope.delete = function(callback) {
            $FullAPI.one('users', member.id).remove().then(function() {
                Notification.success('User deleted', "User has been removed from Workstation");
                callback();
                $state.go('admin.members');
            }, function(response) {
                $rootScope.$broadcast('userDeleted', false);
                Notification.error("Could not delete user", response);
            });
        }
    }
])

.controller('AddTokensController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'prices', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, prices, member) {
        var defaults = $scope.ngDialogData || {};
        var system_token = _.findWhere(prices.plain(), { slug: "token" }) || {};

        $scope.token_cost = system_token.price;
        $scope.tokens = defaults.amount;

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            $API.one('users', member.id).all('purchase').post({
                add_on_id: system_token.id,
                quantity: $scope.tokens,
                reason: reason
            }).then(function() {
                Notification.success('Tokens Added', "Member token count has been updated");
                $state.reload().then(function() { $scope.closeThisDialog() });
            }, function(response) {
                $rootScope.$broadcast('tokensAdded', false);
                Notification.error("Could not add tokens", response);
            });
        }
    }
])

.controller('AddPrintingController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'prices', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, prices, member) {
        var defaults = $scope.ngDialogData || {};

        var system_bw_printing = _.findWhere(prices.plain(), { slug: "b-w-printing" }) || {};
        var system_rgb_printing = _.findWhere(prices.plain(), { slug: "colour-printing" }) || {};

        $scope.rgb_printing_cost = system_rgb_printing.price;
        $scope.bw_printing_cost = system_bw_printing.price;

        $scope.bw_printing = defaults.bw_printing;
        $scope.rgb_printing = defaults.rgb_printing;
        $scope.total = 0;

        $scope.computeTotal = function() {
            $scope.total = (($scope.bw_printing || 0) * $scope.bw_printing_cost) + (($scope.rgb_printing || 0) * $scope.rgb_printing_cost);
        }

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            var tasks = 0;

            if ($scope.bw_printing) {
                tasks++;
                $API.one('users', member.id).all('purchase').post({
                    add_on_id: system_bw_printing.id,
                    quantity: $scope.bw_printing,
                    reason: reason
                }).then(function() {
                    onComplete();
                    Notification.success('B/W Printing Added', "Member printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingAdded', false);
                    Notification.error("Could not purchase b/w printing", response);
                });
            }

            if ($scope.rgb_printing) {
                tasks++;
                $API.one('users', member.id).all('purchase').post({
                    add_on_id: system_rgb_printing.id,
                    quantity: $scope.rgb_printing,
                    reason: reason
                }).then(function() {
                    onComplete();
                    Notification.success('Color Printing Added', "Member printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingAdded', false);
                    Notification.error("Could not purchase color printing", response);
                });
            }

            var onComplete = function() {
                tasks--;
                if (tasks == 0) {
                    $rootScope.$broadcast('printingAdded', true);
                    $state.reload().then(function() { $scope.closeThisDialog() });
                }
            }
        }
    }
])

.controller('AddAddonController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'addons', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, addons, member) {
        $scope.addons = addons;

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            $API.one('users', member.id).all('purchase').post({
                add_on_id: $scope.addon_id,
                quantity: $scope.tokens,
                reason: reason
            }).then(function() {
                Notification.success('Add-on Added', "Addon has been added to member subscription");
                $state.reload().then(function() { $scope.closeThisDialog() });
            }, function(response) {
                $rootScope.$broadcast('addonAdded', false);
                Notification.error("Could not add add-on", response);
            });
        }
    }
])

.controller('AdminSubscriptionController', ['$scope', '$rootScope', '$state', '$API', '$window', 'LocalService', 'Notification', 'ngDialog', 'plans', 'member',
    function($scope, $rootScope, $state, $API, $window, LocalService, Notification, ngDialog, plans, member) {
        $scope.member = member;

        if (member.subscription) {
            $scope.current_plan = _.findWhere(plans, { id: member.subscription.plan_id });

            if ($scope.current_plan) {
                $scope.current_subscription = member.subscription;
                $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
            }

            $scope.renew = function() {
                $scope.complete($scope.current_subscription);
            }
        }

        $scope.plans = _.filter(plans, function(plan) {
            return plan.limit === null || plan.limit > 0
        });

        $scope.plans = _.map($scope.plans, function(plan) {
            plan.intervals = [];
            if (plan.daily_price) plan.intervals.push({ name: "Daily", slug: "daily", price: plan.daily_price });
            if (plan.monthly_price) plan.intervals.push({ name: "Monthly", slug: "monthly", price: plan.monthly_price });
            if (plan.yearly_price) plan.intervals.push({ name: "Yearly", slug: "yearly", price: plan.yearly_price });

            return plan;
        });

        $scope.subscribeTo = function(plan) {
            $scope.subscription = {
                plan_id: plan.id,
                interval: plan.intervals[0].slug,
                days: []
            }

            $scope.active_plan = plan;
            if (plan.days < 5) {
                $scope.days = {
                    monday: false,
                    tuesday: false,
                    wednesday: false,
                    thursday: false,
                    friday: false
                }
            } else {
                $scope.subscription.days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
            }
        }

        $scope.addDays = function() {
            $scope.subscription.days = [];
            _.each($scope.days, function(value, key) {
                if (value) $scope.subscription.days.push(key);
            })
        }

        $scope.toggleDay = function(day) {
            if (_.contains($scope.subscription.days, day)) {
                $scope.subscription.days = _.without($scope.subscription.days, day);
            } else {
                $scope.subscription.days.push(day);
            }
        }

        $scope.complete = function(subscription) {
            subscription.reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            var payload = _.pick(subscription, ['interval', 'plan_id', 'days', 'reason']);

            $API.one('users', member.id).all('subscribe').post(payload).then(function() {
                $scope.closeThisDialog();
                Notification.success('Whoop!', "User subscription was successful");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('subscriptionDone', false);
                Notification.error("Could not subscribe to this plan", response);
            });
        }

        if (member.subscription) {
            $scope.days = { monday: false, tuesday: false, wednesday: false, thursday: false, friday: false };
            $scope.subscription = _.clone(member.subscription);
            _.each($scope.subscription.days, function(day) { $scope.days[day] = true });

            $scope.update = function() {
                $API.one('subscriptions', $scope.subscription.id)
                    .customPUT({
                        reason: $scope.reason,
                        days: $scope.subscription.days
                    })
                    .then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "User subscription has been updated");
                        $state.reload();
                    }, function(response) {
                        $rootScope.$broadcast('updatedSubscription', false);
                        Notification.error("Could not update user subscription", response);
                    });
            }
        }
    }
])

.controller('AdminOfficeSubscriptionController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'offices', 'member',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, offices, member) {
        $scope.member = member;
        $scope.offices = _.filter(offices, function(office) {
            return office.limit === null || office.limit > 0
        });

        $scope.team_sizes = {
            max: _.max($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces;
            }),
            min: _.min($scope.offices, function(_office) {
                return _office.seats_no;
            })
        }

        $scope.checkAvailability = function() {
            $scope.office = null;
            var suitable_offices = _.filter($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces >= $scope.office_seats;
            });

            if (suitable_offices.length) {
                $scope.office = _.min(suitable_offices, function(_office) {
                    return _office.seats_no;
                });

                var intervals = [];
                if ($scope.office.monthly_price) intervals.push({ name: "Monthly", slug: "monthly", price: $scope.office.monthly_price });
                if ($scope.office.yearly_price) intervals.push({ name: "Yearly", slug: "yearly", price: $scope.office.yearly_price });
                $scope.office.intervals = intervals;

                $scope.office_interval = $scope.office.intervals[0].slug;
                $scope.calculateCost($scope.office_interval);
            }
        }

        $scope.calculateCost = function(interval) {
            var interval = _.find($scope.office.intervals, { slug: interval });
            $scope.office.seat_cost = interval.price;
            $scope.office.total_cost = $scope.office_seats * interval.price;
            if ($scope.office.setup_fee) {
                $scope.office.total_cost += ($scope.office.setup_fee * $scope.office_seats);
            }
        }

        $scope.subscribe = function(office) {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;

            $API.all('group_plan_subscriptions')
                .post({
                    group_plan_id: office.id,
                    interval: $scope.office_interval,
                    no_of_seats: $scope.office_seats,
                    user_id: $scope.member.id,
                    reason: reason
                })
                .then(function() {
                    $scope.closeThisDialog();
                    Notification.success('Whoop!', "Office subscription was successful");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe for this office", response);
                });
        }
    }
])

angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.settings', {
                url: '/settings',
                templateUrl: 'modules/admin/settings/index.html',
                access: 'admin',
                redirectTo: 'admin.settings.general'
            })
            .state('admin.settings.general', {
                url: '/general',
                title: 'General',
                templateUrl: 'modules/admin/settings/pages/general.html',
                controller: 'AdminController',
                access: 'admin',
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                }
            })
            .state('admin.settings.locations', {
                url: '/locations',
                title: 'Locations',
                templateUrl: 'modules/admin/settings/pages/locations.html',
                controller: 'AdminLocationsController',
                access: 'admin',
                resolve: {
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }],
                    countries: ['$API', function($API) {
                        return $API.all('countries').getList();
                    }]
                }
            })
            .state('admin.settings.admins', {
                url: '/admins?:location_id',
                title: 'Admins',
                templateUrl: 'modules/admin/settings/pages/admins.html',
                controller: 'AdminListController',
                access: 'admin',
                resolve: {
                    admins: ['$API', function($API) {
                        return $API.all('admins').getList();
                    }],
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                }
            })
            .state('admin.settings.logs', {
                url: '/logs?page',
                title: 'Logs',
                templateUrl: 'modules/admin/settings/pages/logs.html',
                controller: 'LogListController',
                access: 'admin',
                resolve: {
                    trails: ['$FullAPI', '$stateParams', function($FullAPI, $stateParams) {
                        var page = $stateParams.page || 1;
                        return $FullAPI.all('trails').getList({ page: page })
                    }]
                }
            })
            .state('admin.settings.rooms', {
                url: '/rooms',
                title: 'Rooms',
                templateUrl: 'modules/admin/settings/pages/rooms.html',
                controller: 'AdminRoomsController',
                access: 'admin',
                resolve: {
                    rooms: ['$API', function($API) {
                        return $API.all('meeting_rooms').getList();
                    }],
                    locations: ['$API', function($API) {
                        return $API.all('locations').getList();
                    }]
                }
            })
            .state('admin.settings.plans', {
                url: '/plans',
                title: 'Plans',
                templateUrl: 'modules/admin/settings/pages/plans.html',
                controller: 'AdminPlansController',
                access: 'admin',
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }]
                }
            })
            .state('admin.settings.offices', {
                url: '/offices',
                title: 'Private Offices',
                templateUrl: 'modules/admin/settings/pages/offices.html',
                controller: 'AdminOfficesController',
                access: 'admin',
                resolve: {
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }]
                }
            });
    }
])

.controller('AdminController', ['$scope', '$rootScope', '$API', '$state', '$window', 'ngDialog', 'FileHelper', 'prices',
    function($scope, $rootScope, $API, $state, $window, ngDialog, FileHelper, prices) {
        $scope.profile = _.clone($rootScope.User);

        $scope.system = {
            tokens: _.findWhere(prices.plain(), { slug: "token" }),
            rgb_printing: _.findWhere(prices.plain(), { slug: "colour-printing" }),
            bw_printing: _.findWhere(prices.plain(), { slug: "b-w-printing" })
        }

        $scope.showModal = function(name, controller, scope) {
            ngDialog.open({
                templateUrl: name,
                controller: controller || "AdminAccountController",
                scope: scope ? $scope : null
            });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $API.one('admins', $scope.profile.id).customPUT({ image: response.url }).then(function() {
                    $rootScope.User.image = response.url;
                });
            }).catch(function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });
    }
])

.controller('AdminPricesController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification',
    function($scope, $rootScope, $state, $auth, $API, Notification) {
        $scope.updatePrices = function() {
            $API.one('add_ons', $scope.system.rgb_printing.id).customPUT({ price: $scope.system.rgb_printing.price });
            $API.one('add_ons', $scope.system.bw_printing.id).customPUT({ price: $scope.system.bw_printing.price });
            $API.one('add_ons', $scope.system.tokens.id).customPUT({ price: $scope.system.tokens.price })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Prices have been updated");
                    });
                });
        }
    }
])

.controller('AdminAccountController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification',
    function($scope, $rootScope, $state, $auth, $API, Notification) {
        $scope.profile = _.clone($rootScope.User);
        $scope.profile.home_location = $scope.profile.home_location || {};
        $scope.profile.home_location_id = $scope.profile.home_location.id;
        $scope.updatePassword = {};

        $scope.updateAccount = function() {
            $API.one('admins', $scope.profile.id).customPUT($scope.profile)
                .then(function() {
                    $auth.updateAccount($scope.profile).then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Your account has been updated");
                    })
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.changePassword = function() {
            $auth.updatePassword({
                    password: $scope.password,
                    password_confirmation: $scope.password_confirmation
                })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Your password has been changed");
                    })
                }, function(response) {
                    $rootScope.$broadcast('passwordChanged', false);
                    Notification.error("Could not change your password", response);
                });
        }
    }
])

.controller('AdminLocationsController', ['$scope', '$state', 'ngDialog', 'locations', 'countries',
    function($scope, $state, ngDialog, locations, countries) {
        $scope.locations = locations.plain();

        $scope.newLocation = function() {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/newLocation.html",
                controller: "NewLocationController",
                trapFocus: false,
                data: {
                    countries: countries.plain()
                }
            });
        }

        $scope.editLocation = function(location) {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/editLocation.html",
                controller: "EditLocationController",
                trapFocus: false,
                data: {
                    location: _.clone(location),
                    countries: countries.plain()
                }
            });
        }

        $scope.deleteLocation = function(location) {
            ngDialog.open({
                templateUrl: "modules/admin/settings/modals/deleteLocation.html",
                controller: "EditLocationController",
                trapFocus: false,
                data: {
                    location: location
                }
            });
        }

    }
])

.controller('NewLocationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.location = {};
        $scope.countries = $scope.ngDialogData.countries;
        $scope.statesPlaceholder = "Choose Country";

        $scope.save = function() {
            $API.all('locations').post($scope.location)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('locationCreated', false);
                    Notification.error("Could not create location", response);
                });
        }
    }
])

.controller('LocationStatesCitiesController', ['$scope', '$API',
    function($scope, $API) {
        $scope.populateCountry = function(selected) {
            if (!selected) {
                $scope.states = [];
                $scope.location.country_code = null;
                $scope.location.state_shortcode = null;
                $scope.$broadcast('angucomplete-alt:clearInput', 'states');
                return;
            }

            var country = selected.originalObject;
            $scope.location.country_code = country.code;
            $scope.loadingStates = true;

            $API.one('countries', country.code).get()
                .then(function(response) {
                    $scope.states = response.states;
                    $scope.loadingStates = false;
                });
        }

        $scope.populateState = function(selected) {
            if (!selected) {
                $scope.location.state_shortcode = null;
                return;
            }

            var state = selected.originalObject;
            $scope.location.state_shortcode = state.shortcode;
        }
    }
])

.controller('EditLocationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.countries = $scope.ngDialogData.countries;
        $scope.location = $scope.ngDialogData.location;
        $scope.location.country_code = $scope.location.country.code;
        $scope.location.state_shortcode = $scope.location.state.shortcode;

        $scope.save = function() {
            var data = _.omit($scope.location, 'country', 'state');
            $API.one('locations', $scope.location.id).customPUT(data)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('locationUpdated', false);
                    Notification.error("Could not update location", response);
                });
        }

        $scope.delete = function() {
            $API.one('locations', $scope.location.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Location has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('locationDeleted', false);
                    Notification.error("Could not delete location", response);
                });
        }
    }
])

.controller('AdminListController', ['$scope', '$state', 'ngDialog', 'admins', 'locations',
    function($scope, $state, ngDialog, admins, locations) {
        $scope.admins = admins;
        $scope.locations = locations.plain();

        $scope.newAdmin = function() {
            ngDialog.open({
                templateUrl: "NewAdmin",
                controller: "NewAdminController",
                data: {
                    locations: locations
                }
            });
        }

        $scope.openModal = function(modal, admin) {
            ngDialog.open({
                templateUrl: modal,
                controller: "EditAdminController",
                data: {
                    admin: admin
                }
            });
        }

        $scope.viewLogs = function(admin) {
            ngDialog.open({
                templateUrl: '/modules/admin/settings/modals/viewLogs.html',
                controller: "AdminLogsController",
                data: {
                    admin: admin
                }
            });
        }

    }
])

.controller('LogListController', ['$scope', '$state', 'ngDialog', 'trails',
    function($scope, $state, ngDialog, trails) {
        $scope.showAdmin = true;
        
        var meta = {
            total: trails.headers("total"),
            perPage: 25,
            page: $state.params.page,
            pageCount: Math.round(trails.headers("total") / 25)
        }
        $scope.trails = trails.data;
        $scope.trails.meta = meta;

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }
    }
])

.controller('AdminLogsController', ['$scope', '$rootScope', '$state', '$FullAPI', 'Notification',
    function($scope, $rootScope, $state, $FullAPI, Notification) {
        $scope.admin = $scope.ngDialogData.admin;

        $scope.load = function(page) {
            var page = page || 1;
            $FullAPI.one('admins', $scope.admin.id).all('trails').getList({ page: page })
                .then(function(response) {
                    var meta = {
                        total: response.headers("total"),
                        perPage: 25,
                        page: page,
                        pageCount: Math.round(response.headers("total") / 25)
                    }
                    $scope.trails = response.data;
                    $scope.trails.meta = meta;
                });
        }

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }

        $scope.load();
    }
])

.controller('NewAdminController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.admin = {};
        $scope.locations = $scope.ngDialogData.locations;

        $scope.save = function() {
            $scope.admin.password_confirmation = $scope.admin.password;
            $API.all('admins').post($scope.admin)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "New admin has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminCreated', false);
                    Notification.error("Could not create admin", response);
                });
        }
    }
])

.controller('EditAdminController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.admin = $scope.ngDialogData.admin;

        $scope.update = function() {
            $API.one('admins', $scope.admin.id).customPUT({
                    superadmin: !$scope.admin.superadmin
                })
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Admin privileges have been updated");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminUpdated', false);
                    Notification.error("Could not update admin", response);
                });
        }

        $scope.delete = function() {
            $API.one('admins', $scope.admin.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Admin has been deleted");
                    })
                }, function(response) {
                    $rootScope.$broadcast('adminDeleted', false);
                    Notification.error("Could not delete admin", response);
                });
        }
    }
])

.controller('AdminRoomsController', ['$scope', '$state', 'ngDialog', 'locations', 'rooms',
    function($scope, $state, ngDialog, locations, rooms) {
        $scope.rooms = rooms.plain();

        $scope.newRoom = function() {
            ngDialog.open({
                templateUrl: "NewRoom",
                controller: "NewRoomController",
                data: {
                    locations: locations.plain()
                }
            });
        }

        $scope.editRoom = function(room) {
            ngDialog.open({
                templateUrl: "EditRoom",
                controller: "EditRoomController",
                data: {
                    room: _.clone(room),
                    locations: locations.plain()
                }
            });
        }

        $scope.deleteRoom = function(room) {
            ngDialog.open({
                templateUrl: "DeleteRoom",
                controller: "EditRoomController",
                data: {
                    room: room
                }
            });
        }

    }
])

.controller('NewRoomController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.locations = $scope.ngDialogData.locations;
        $scope.room = {
            location_id: $rootScope.User.home_location_id || $scope.locations[0].id
        };

        $scope.save = function() {
            $API.all('meeting_rooms').post($scope.room)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('roomCreated', false);
                    Notification.error("Could not create room", response);
                });
        }
    }
])

.controller('EditRoomController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.locations = $scope.ngDialogData.locations;
        $scope.room = $scope.ngDialogData.room;

        $scope.save = function() {
            $API.one('meeting_rooms', $scope.room.id).customPUT($scope.room)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('roomUpdated', false);
                    Notification.error("Could not update room", response);
                });
        }

        $scope.delete = function() {
            $API.one('meeting_rooms', $scope.room.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Room has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('roomDeleted', false);
                    Notification.error("Could not delete room", response);
                });
        }
    }
])

.controller('AdminPlansController', ['$scope', '$state', '$filter', 'ngDialog', 'locations', 'plans', 'addons',
    function($scope, $state, $filter, ngDialog, locations, plans, addons) {
        $scope.plans = _.map(plans, function(plan) {
            var durations = [];
            if (plan.daily_price) durations.push("Daily");
            if (plan.monthly_price) durations.push("Monthly");
            if (plan.yearly_price) durations.push("Yearly");

            plan.durations = durations.join(", ");
            return plan;
        });
        $scope.addons = _.map(addons, function(addon) {
            addon.is_add_on = true;
            return addon;
        });

        $scope.newPlan = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/newPlan.html",
                controller: "NewPlanController",
                data: {
                    plan: plan
                },
                width: 600
            });
        }

        $scope.view = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/viewPlan.html",
                controller: "ViewPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }

        $scope.edit = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editPlan.html",
                controller: "EditPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }

        $scope.delete = function(plan) {
            ngDialog.open({
                templateUrl: "DeletePlan",
                controller: "EditPlanController",
                data: {
                    plan: plan
                }
            });
        }

    }
])

.controller('NewPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.plan = $scope.ngDialogData.plan;
        $scope.addon = {};

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                delete $scope.plan[field];
            });
        }

        $scope.savePlan = function() {
            $API.all('plans').post($scope.plan)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Plan has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('planCreated', false);
                    Notification.error("Could not create plan", response);
                });
        }

        $scope.saveAddon = function() {
            $API.all('add_ons').post($scope.addon)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Add-on has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('addonCreated', false);
                    Notification.error("Could not create add-on", response);
                });
        }
    }
])

.controller('ViewPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, ngDialog) {
        $scope.plan = $scope.ngDialogData.plan;
        $scope.plan.has_daily_option = $scope.plan.daily_price !== null;
        $scope.plan.has_monthly_option = $scope.plan.monthly_price !== null;
        $scope.plan.has_yearly_option = $scope.plan.yearly_price !== null;

        $scope.edit = function(plan) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editPlan.html",
                controller: "EditPlanController",
                data: {
                    plan: _.clone(plan)
                },
                width: 600
            });
        }
    }
])

.controller('EditPlanController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.plan = $scope.ngDialogData.plan;

        if (!$scope.plan.is_add_on) {
            $scope.plan.has_daily_option = $scope.plan.daily_price !== null;
            $scope.plan.has_monthly_option = $scope.plan.monthly_price !== null;
            $scope.plan.has_yearly_option = $scope.plan.yearly_price !== null;
        }

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                $scope.plan[field] = null;
            });
        }

        $scope.save = function() {
            var url = $scope.plan.is_add_on ? "add_ons" : "plans";
            $API.one(url, $scope.plan.id).customPUT($scope.plan)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", ($scope.plan.is_add_on ? "Add-on" : "Plan") + " has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('planUpdated', false);
                    Notification.error("Could not update " + ($scope.plan.is_add_on ? "add-on" : "plan"), response);
                });
        }

        $scope.delete = function() {
            var url = $scope.plan.is_add_on ? "add_ons" : "plans";
            $API.one(url, $scope.plan.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", ($scope.plan.is_add_on ? "Add-on" : "Plan") + " has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('planDeleted', false);
                    Notification.error("Could not delete " + ($scope.plan.is_add_on ? "add-on" : "plan"), response);
                });
        }
    }
])

.controller('AdminOfficesController', ['$scope', '$state', '$filter', 'ngDialog', 'locations', 'offices',
    function($scope, $state, $filter, ngDialog, locations, offices) {
        $scope.offices = _.map(offices, function(office) {
            var durations = [];
            if (office.monthly_price) durations.push("Monthly");
            if (office.yearly_price) durations.push("Yearly");

            office.durations = durations.join(", ");
            return office;
        });

        $scope.newOffice = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/newOffice.html",
                controller: "NewOfficeController",
                data: {
                    office: office
                },
                width: 600
            });
        }

        $scope.view = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/viewOffice.html",
                controller: "ViewOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }

        $scope.edit = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editOffice.html",
                controller: "EditOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }

        $scope.delete = function(office) {
            ngDialog.open({
                templateUrl: "DeleteOffice",
                controller: "EditOfficeController",
                data: {
                    office: office
                }
            });
        }

    }
])

.controller('NewOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.office = $scope.ngDialogData.office;

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                delete $scope.office[field];
            });
        }

        $scope.saveOffice = function() {
            $API.all('group_plans').post($scope.office)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been created");
                    })
                }, function(response) {
                    $rootScope.$broadcast('officeCreated', false);
                    Notification.error("Could not create office", response);
                });
        }
    }
])

.controller('ViewOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, ngDialog) {
        $scope.office = $scope.ngDialogData.office;
        $scope.office.has_daily_option = $scope.office.daily_price !== null;
        $scope.office.has_monthly_option = $scope.office.monthly_price !== null;
        $scope.office.has_yearly_option = $scope.office.yearly_price !== null;

        $scope.edit = function(office) {
            ngDialog.open({
                templateUrl: "/modules/admin/settings/modals/editOffice.html",
                controller: "EditOfficeController",
                data: {
                    office: _.clone(office)
                },
                width: 600
            });
        }
    }
])

.controller('EditOfficeController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.office = $scope.ngDialogData.office;

        if (!$scope.office.is_add_on) {
            $scope.office.has_daily_option = $scope.office.daily_price !== null;
            $scope.office.has_monthly_option = $scope.office.monthly_price !== null;
            $scope.office.has_yearly_option = $scope.office.yearly_price !== null;
        }

        $scope.reset = function(fields) {
            _.each(fields, function(field) {
                $scope.office[field] = null;
            });
        }

        $scope.save = function() {
            $API.one('group_plans', $scope.office.id).customPUT($scope.office)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('officeUpdated', false);
                    Notification.error("Could not update office", response);
                });
        }

        $scope.delete = function() {
            $API.one('group_plans', $scope.office.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Office has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('officeDeleted', false);
                    Notification.error("Could not delete office", response);
                });
        }
    }
])

angular.module('app.common')

.controller('BookingsController', ['$scope', '$rootScope', '$state', '$API', 'CalendarHelper', 'ngDialog', 'Notification', 'bookings',
    function($scope, $rootScope, $state, $API, CalendarHelper, ngDialog, Notification, bookings) {

        function formatBooking(booking) {
            var grace_period = $rootScope.User.isAdmin ? '60' : '30';

            booking.start = moment(booking.start_time).local();
            booking.end = moment(booking.end_time).local();
            booking.past = moment().isAfter(booking.end);
            booking.active = moment().isBetween(booking.start, booking.end);
            booking.self = booking.booker.uid == $rootScope.User.uid && booking.booker.id == $rootScope.User.id;
            booking.editable = moment().add(grace_period, 'minutes').isBefore(booking.start);
            booking.start_string = moment(booking.start).format("ddd, MMM Do, h:mm a");
            booking.end_string = moment(booking.end).format("ddd, MMM Do, h:mm a");

            if ($rootScope.User.isAdmin) {
                booking.self = true;
            }

            if (!$rootScope.User.isAdmin && !booking.self) {
                var booker_name = booking.booker.first_name + " " + booking.booker.last_name;
                booking.title = "Booked by " + booker_name;
            }

            return booking;
        }

        bookings = _.map(bookings, function(booking) {
            return formatBooking(booking);
        });

        $scope.bookings = bookings;

        $scope.newBooking = function() {
            ngDialog.open({
                templateUrl: "NewBooking",
                controller: "NewBookingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.viewBooking = function(booking, jsEvent, view) {
            $scope.booking = booking;
            if (view.name == 'basicWeek') {
                CalendarHelper.renderTooltip(jsEvent);
            }
        }

        $scope.loadBooking = function(booking, jsEvent, view) {
            $scope.booking = formatBooking(booking);
            ngDialog.open({
                templateUrl: "ViewBooking",
                scope: $scope
            });
        }

        $scope.editBooking = function(booking) {
            $scope.booking = formatBooking(booking);
            if ($scope.booking.editable) {
                ngDialog.open({
                    templateUrl: "EditBooking",
                    controller: "EditBookingController",
                    data: {
                        booking: $scope.booking
                    }
                });
            } else {
                ngDialog.open({
                    templateUrl: "BookingNotEditable"
                });
            }
        }

        $scope.deleteBooking = function(booking) {
            ngDialog.openConfirm({
                templateUrl: "DeleteBooking",
                controller: ['$scope', function($scope) {
                    $scope.booking = booking;
                }]
            }).then(function(booking) {
                var deleteAPI;

                if (booking.delete_reason) {
                    deleteAPI = $API.one('users', booking.booker.id).all('delete_booking').post({ reason: booking.delete_reason, booking_id: booking.id });
                } else {
                    deleteAPI = $API.one('bookings', booking.id).remove();
                }

                deleteAPI.then(function() {
                    Notification.success("All Done!", "Booking has been deleted");
                    $state.reload();
                }, function(response) {
                    $scope.$broadcast('bookingDeleted', false);
                    Notification.error("Could not delete booking", response);
                });
            });
        }

        $scope.uiConfig = {
            calendar: {
                height: 600,
                header: {
                    center: 'title',
                    left: 'prev,next today',
                    right: 'basicWeek listWeek',
                },
                defaultView: 'basicWeek',
                eventClick: $scope.loadBooking,
                eventMouseover: $scope.viewBooking
            }
        };

        $scope.selfBookings = _.filter($scope.bookings, 'self');
        $scope.otherBookings = _.reject($scope.bookings, 'self');

        $scope.activeBookings = _.filter($scope.selfBookings, 'active');
        var inactiveBookings = _.reject($scope.selfBookings, 'active');

        $scope.pastBookings = _.filter(inactiveBookings, 'past');
        $scope.futureBookings = _.reject(inactiveBookings, 'past');

        $scope.bookingsSources = [{
            events: $scope.otherBookings,
            backgroundColor: '#ccc',
            borderColor: '#c0c0c0'
        }, {
            events: $scope.activeBookings,
            backgroundColor: '#F05050',
            borderColor: '#F05050'
        }, {
            events: $scope.pastBookings,
            backgroundColor: '#ccc',
            borderColor: '#c0c0c0'
        }, {
            events: $scope.futureBookings
        }];
    }
])

.controller('BookingRoomsController', ['$scope', '$rootScope', '$interval', '$API', function($scope, $rootScope, $interval, $API) {
    $API.all('meeting_rooms').getList({
        location_id: $rootScope.User.home_location_id
    }).then(function(response) {
        $scope.rooms = response.plain();
        checkAvailableRooms();
        $interval(checkAvailableRooms, 2000);
    });

    function checkAvailableRooms() {
        _.each($scope.rooms, function(room) {
            room.meetings = [];

            _.each($scope.bookings, function(booking) {
                booking.active = moment().isBetween(booking.start, booking.end);

                if (booking.active && booking.meeting_room.id == room.id) {
                    room.meetings.push(booking);
                }
            });

            if (room.meetings.length) {
                room.busy = room.meetings[0];
            } else {
                room.busy = false;
            }
        });
    }
}])

.controller('BookingTimepickerHelper', ['$scope', 'DatepickerHelper', function($scope, DatepickerHelper) {
    $scope.onTimeSet = function(newDate, oldDate) {
        var inFuture = moment(newDate).isAfter(moment());
        if (inFuture) {
            calculateEndDates(newDate);
            DatepickerHelper.triggerClick();
        } else {
            $scope.booking.start_time = null;
            Notification.error("Past date", "Please choose a date in the future");
        }
    }

    $scope.calculateEndDates = calculateEndDates;

    function calculateEndDates(date) {
        var _date = moment(date);
        // var endOfDay = moment(date).startOf('day').add(18, 'hours');
        var endOfDay = moment(date).endOf('day');

        var possibleEndDates = [];
        var bookingLength = 0;

        while (_date.isBefore(endOfDay)) {
            bookingLength++;

            _date.add(1, 'hour');
            possibleEndDates.push({
                value: _date.format('h:mm A'),
                length: bookingLength
            });
        }

        $scope.possibleEndDates = possibleEndDates;
    }

    $scope.beforeRender = function($view, $dates, $leftDate, $upDate, $rightDate) {
        _.each($dates, function($this) {
            var _date = moment($this.localDateValue());

            if ($view == "day") {
                var today = moment();
                var _dayInThePast = today.isAfter(_date, 'day');
                $this.selectable = !_dayInThePast;
            }

            if ($view == "hour") {
                var _hour = parseInt(_date.format('H'));
                var _isWorkingHour = true; // _hour >= 8 && _hour < 18;
                var _isInTheFuture = _date.isAfter(moment());

                $this.selectable = _isWorkingHour && _isInTheFuture;
            }
        })
    }
}])

.controller('NewBookingController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'ngDialog', 'Session',
    function($scope, $rootScope, $state, $window, $API, Notification, ngDialog, Session) {
        $API.all('meeting_rooms').getList({
            location_id: $rootScope.User.home_location_id
        }).then(function(response) {
            $scope.rooms = response.plain();
        });

        $scope.booking = {};

        $scope.save = function() {
            $API.all('bookings').post($scope.booking)
                .then(Session.update)
                .then(function() {
                    Notification.success("All Done!", "Booking has been added");
                    $state.reload();
                    $scope.closeThisDialog();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('bookingCreated', false);
                    Notification.error("Could not create booking", response);
                });
        }

        $scope.hasEnoughTokens = function() {
            var user = $rootScope.User;

            if (!$scope.booking.meeting_room_id || !$scope.booking.length) return false;

            var meeting_room = _.findWhere($scope.rooms, { id: $scope.booking.meeting_room_id });
            $scope.booking.tokens_needed = meeting_room.tokens_per_hour * $scope.booking.length;
            $scope.booking.token_deficit = $scope.booking.tokens_needed - user.token_allowance;

            return user.token_allowance >= $scope.booking.tokens_needed;
        }

        $scope.buyTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyTokensController",
                data: {
                    amount: $scope.booking.token_deficit
                }
            });
        }
    }
])

.controller('EditBookingController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, Session, $API, Notification, ngDialog) {
        $API.all('meeting_rooms').getList({
            location_id: $rootScope.User.home_location_id
        }).then(function(response) {
            $scope.rooms = response.plain();
        });

        $scope.booking = $scope.ngDialogData.booking;
        $scope.booking.active = moment().isBetween($scope.booking.start_date, $scope.booking.end_date);

        $scope.save = function() {
            var payload = _.pick($scope.booking, ['title', 'start_time', 'length', 'meeting_room_id']);
            if (!$rootScope.User.isAdmin) payload = _.omit(payload, ['length', 'meeting_room_id']);

            $API.one('bookings', $scope.booking.id).customPUT(payload)
                .then(Session.update)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Booking has been updated");
                    });
                })
                .catch(function(response) {
                    $scope.$broadcast('bookingUpdated', false);
                    Notification.error("Could not update booking", response);
                });
        }
    }
])

angular.module('app.common')

.controller('EventsController', ['$scope', '$rootScope', '$state', '$API', 'CalendarHelper', 'ngDialog', 'events',
    function($scope, $rootScope, $state, $API, CalendarHelper, ngDialog, events) {
        $scope.events = events.plain();

        $scope.newEvent = function() {
            ngDialog.open({
                templateUrl: "NewEvent",
                controller: "NewEventController",
                showClose: true,                
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.editEvent = function(event) {
            ngDialog.open({
                templateUrl: "EditEvent",
                controller: "EditEventController",
                data: {
                    event: event
                }
            });
        }
    }
])

.controller('NewEventController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.event = {};

        $scope.save = function() {
            $API.all('events').post($scope.event)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('eventCreated', false);
                    Notification.error("Could not create event", response);
                });
        }
    }
])

.controller('CommonEventController', ['$scope', '$rootScope', 'FileHelper', function($scope, $rootScope, FileHelper) {
	$scope.locations = $rootScope.locations;

    $scope.$watch('event_image', function(newFile, oldFile) {
        if (newFile && newFile != oldFile) {
            $scope.uploading = true;
            FileHelper.upload(newFile).success(function(response) {
                $scope.uploading = false;
                $scope.event.image_url = response.url;
            });
        }
    });

    $scope.remove = function() {
        $scope.event_image = null;
        $scope.event.image_url = null;
    }
}])

.controller('EditEventController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.event = $scope.ngDialogData.event;

        $scope.save = function() {
            $API.one('events', $scope.event.id).customPUT($scope.event)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('eventUpdated', false);
                    Notification.error("Could not update event", response);
                });
        }

        $scope.delete = function() {
            $API.one('events', $scope.event.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('eventDeleted', false);
                    Notification.error("Could not delete event", response);
                });
        }
    }
])

window.fbAsyncInit = function() {
    FB.init({
        appId: settings.facebookAppId,
        xfbml: true,
        version: 'v2.8'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

angular.module('app.common')

.controller('FeedController', ['$scope', '$rootScope', '$state', '$timeout', '$streamService', '$ocLazyLoad', '$API', '$FullAPI', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $timeout, $streamService, $ocLazyLoad, $API, $FullAPI, Notification, ngDialog) {
        $scope.moment = moment;
        $scope.updates = [];
        $scope.meta = { page: 1, per_page: 25 }

        var user = $rootScope.User;
        $scope.userID = (user.isAdmin ? "Admin-" : "User-") + user.id;
        var user_location = user.home_location ? user.home_location.id : "";
        if (user.isAdmin && !user_location) user_location = $rootScope.locations[0].id;

        var location = _.find($rootScope.locations, function(location) {
            return location.id == user_location;
        });

        var stream = $streamService.connect('8bqdnjnbspbt', '14276');
        var timeline = stream.feed('location_timeline', location.id, location.feed_token);

        $FullAPI.all('posts').getList({
            feed_category_id: $state.params.feed,
            per_page: $scope.meta.per_page
        }).then(function(response) {
            $scope.meta.last_page = Math.round(response.headers("total") / $scope.meta.per_page);
            $scope.feed = _.map(response.data, function(post) {
                post.comments = _.map(post.comments, function(comment) {
                    return $scope.formatPost(comment)
                });
                return $scope.formatPost(post);
            });
        });

        $scope.loadMore = function() {
            if ($scope.loading || $scope.meta.last_page <= $scope.meta.page) return;

            $scope.loadingPosts = true;
            $scope.meta.page++;

            $API.all('posts').getList({
                feed_category_id: $state.params.feed,
                per_page: $scope.meta.per_page,
                page: $scope.meta.page
            }).then(function(response) {
                $scope.loadingPosts = false;
                _.each(response.plain(), function(post) {
                    post.comments = _.map(post.comments, function(comment) {
                        return $scope.formatPost(comment)
                    });
                    var formattedPost = $scope.formatPost(post);
                    var postInFeed = _.find($scope.feed, function(_post) {
                        return _post.foreign_id === post.foreign_id
                    });

                    if (!postInFeed) $scope.feed.push(formattedPost);
                });
            });
        }

        timeline.subscribe(function callback(response) {
            if (response.deleted.length) {
                _.each(response.deleted, function(postId) {
                    $scope.removeFromFeed(postId);
                });
            }

            if (response.new.length) {
                _.each(response.new, function(post) {
                    $scope.addToFeed(post);
                });
            }

            $scope.$apply();
        }).then(function() {
            console.log("connected");
        }, function(error) {
            console.log(error)
        });

        $scope.formatPost = function(post) {
            post._id = post.id;
            post.comment_body = post.body;
            post.post_body = post.body;
            post.time = post.created_at;
            post.foreign_id = "Post:" + post.id;

            post.feedable = post.feedable || {};
            post.actor_id = post.feedable_type + "-" + post.feedable.id;
            post.actor_type = post.feedable_type;
            post.actor_email = post.feedable.email;
            post.actor_company = post.feedable.company;
            post.actor_first_name = post.feedable.first_name;
            post.actor_last_name = post.feedable.last_name;
            post.actor_image = post.feedable.image;
            post.actor_bio = post.feedable.bio;

            var location = _.findWhere($rootScope.locations, { id: post.feedable.home_location_id }) || {};
            post.actor_location = location.description;

            if (post.url_meta) {
                post.meta = JSON.parse(post.url_meta);
                post.meta.image && (post.meta.thumb = 'https://carnation-town.glitch.me/?url=' + post.meta.image);
            }

            return post;
        }

        $scope.removeFromFeed = function(postId) {
            var itemInFeed = _.findIndex($scope.feed, { _id: postId });
            var itemInUpdates = _.findIndex($scope.updates, { _id: postId });

            if (itemInFeed > -1) $scope.feed.splice(itemInFeed, 1);
            if (itemInUpdates > -1) $scope.updates.splice(itemInUpdates, 1);
        }

        $scope.addToFeed = function(activity) {
            if (activity.verb === "Post") $scope.addPostToFeed(activity);
            if (activity.verb === "Like") $scope.addLikeToFeed(activity);
            if (activity.verb === "Comment") $scope.addCommentToFeed(activity);
        }

        $scope.addPostToFeed = function(post) {
            post.created_at = moment.parseZone(post.time).local();
            post._id = post.foreign_id.split(":").pop();
            post.actor_id = post.origin.split(":").pop();

            if (post.feed_category_id) {
                post.feed_category = {
                    id: post.feed_category_id,
                    name: post.feed_category_name
                }
            }

            if (post.url_meta) {
                post.meta = JSON.parse(post.url_meta);
                post.meta.image && post.meta.image.startsWith("https") && (post.meta.thumb = post.meta.image);
            }

            var byLoggedInUser = post.origin === ("user:" + $scope.userID);
            if (byLoggedInUser) {
                $scope.feed.unshift(post);
            } else {
                $scope.updates.unshift(post);
                $scope.updateNotificationCount();
            }
        }

        $scope.addLikeToFeed = function(like) {
            like.created_at = moment.parseZone(like.time).local();
            var byLoggedInUser = like.origin === ("user:" + $scope.userID);;
            var postInFeed = _.find($scope.feed, function(post) {
                return post.foreign_id === like.object
            });

            if (postInFeed && !byLoggedInUser) postInFeed.likes_count++;
        }

        $scope.addCommentToFeed = function(comment) {
            var postInFeed = _.find($scope.feed, function(post) {
                return post.foreign_id === comment.object
            });

            comment.created_at = moment.parseZone(comment.time).local();
            comment._id = comment.foreign_id.split(":").pop();
            comment.actor_id = comment.origin.split(":").pop();

            byLoggedInUser = comment.origin === ("user:" + $scope.userID);
            if (postInFeed && !byLoggedInUser) {
                postInFeed.comments = postInFeed.comments || [];
                postInFeed.comments.unshift(comment);
                $scope.$apply();
            };
        }

        $scope.updateNotificationCount = function() {
            if ($scope.updates.length) {
                $rootScope.$title = "(" + $scope.updates.length + ")";
            } else {
                $rootScope.$title = "";
            }
        }

        $scope.showNewUpdates = function() {
            $scope.feed = _.union($scope.updates, $scope.feed);
            $scope.updates = [];
            $rootScope.$title = "";
        }

        $scope.send = function(message, scope) {
            if (!message) return;

            var payload = { body: message }
            var urls = linkify.find(message);
            if (urls.length) {
                payload.link = _.last(urls).href;
            }

            $scope.postingMessage = true;
            $API.all('posts').post(payload).then(function(response) {
                $scope.$broadcast('postedMessage', true);
                $scope.$broadcast('clearMessage', scope);
                $scope.postingMessage = false;
            }, function(response) {
                Notification.error('Unable to post to feed', response);
                $scope.$broadcast('postedMessage', false);
                $scope.postingMessage = false;
            });
        }

        $scope.$on('clearMessage', function(ev, scope) {
            scope.message = null;
        });

        $scope.like = function(post) {
            $API.one('posts', post._id).all('like').post().then(function() {
                post.has_liked = true;
                post.likes_count++;
            }).catch(function(response) {
                Notification.error('Unable to like post', response);
                $scope.$broadcast('postedMessage', false);
            });
        }

        $scope.addComment = function(post) {
            var message = post.new_comment;
            $API.one('posts', post._id).all('comment').post({
                body: post.new_comment
            }).then(function(response) {
                post.comments = post.comments || [];
                var comment = $scope.formatPost(response.plain());
                post.comments.unshift(comment);
            }).catch(function(response) {
                post.new_comment = message;
                Notification.error('Unable to add comment', response);
                $scope.$broadcast('postedMessage', false);
            });
            post.new_comment = "";
        }

        $scope.unlike = function(post) {
            $API.one('posts', post._id).all('unlike').post().then(function() {
                post.has_liked = false;
                post.likes_count--;
            }).catch(function(response) {
                Notification.error('Unable to unlike post', response);
                $scope.$broadcast('postedMessage', false);
            });
        }

        $scope.delete = function(post, index) {
            $API.one('posts', post._id).remove().then(function() {
                $scope.feed = _.without($scope.feed, post);
            }).catch(function(response) {
                Notification.error('Unable to delete post', response);
            });
        }

        $scope.deleteComment = function(post, comment) {
            $API.one('posts/delete_comment', comment._id).remove().then(function() {
                post.comments = _.without(post.comments, comment);
            }).catch(function(response) {
                Notification.error('Unable to delete comment', response);
            });
        }


        // Initialize Facebook
        $ocLazyLoad.load('modules/common/feed/facebook.js');

        $scope.share = function(post, network) {
            var url, message;

            if (post.feed_category) {
                url = post.body;
                message = "Shared on Workstation";
            }

            switch (network) {
                case 'facebook':
                    FB.ui({
                        method: 'share',
                        href: url || "https://www.facebook.com/Workstation-Nigeria-1729334683992057",
                        quote: message || post.body
                    }, function(response) {});

                    break;
                case 'twitter':
                    var twitter_url = "https://twitter.com/intent/tweet/?";
                    twitter_url += "&text=" + (message || post.body).trunc(120);
                    if (url) twitter_url += "&url=" + url;
                    twitter_url += "&via=workstationng";
                    window.open(twitter_url, '_blank');
                    break;
                case 'linkedin':
                    var linkedin_url = "http://www.linkedin.com/shareArticle?mini=true";
                    linkedin_url += "&title=" + (message || post.body);
                    linkedin_url += "&url=" + (url || "http://workstationng.com");
                    linkedin_url += "&source=WorkstationNigeria";
                    window.open(linkedin_url, '_blank');
                    break;
            }
        }

        $scope.report = function(post) {
            ngDialog.openConfirm({
                template: "ConfirmReport"
            }).then(function() {
                $API.one('posts', post._id).all('report').post({
                    id: post._id,
                    home_location_id: user.home_location.id
                }).then(function() {
                    Notification.success('Post reported', 'Admins will review this post');
                }).catch(function(response) {
                    Notification.error('Unable to report post', response);
                });
            });
        }
    }
])

.controller('UserPopupController', ['$scope', '$rootScope', '$state', 'LocalService',
    function($scope, $rootScope, $state, LocalService) {
        var post = $scope.comment || $scope.post;

        $scope.profile = {
            id: parseInt(post.actor_id.split("-").pop()),
            is_admin: post.actor_type == "Admin",
            is_not_self: post.actor_id != $scope.postserID,
            first_name: post.actor_first_name,
            last_name: post.actor_last_name,
            bio: post.actor_bio,
            company: post.actor_company,
            email: post.actor_email,
            image: post.actor_image,
            location: post.actor_location
        }

        $scope.message = function() {
            LocalService.set('workstation-new-conversation', JSON.stringify($scope.profile));
            $state.go('user.messages', { new: $scope.profile.email });
        }
    }
]);

angular.module('app.common')

.controller('MessagesController', ['$scope', '$rootScope', '$state', '$API', 'conversations', 'unread', 'ngDialog', 'LocalService',
    function($scope, $rootScope, $state, $API, conversations, unread, ngDialog, LocalService) {
        var _conversations = _.map(conversations, function(conversation) {
            var userId = $rootScope.User.id;
            if (conversation.last_receiver && (conversation.last_receiver.id == userId)) {
                conversation.receiver = conversation.last_sender;
            } else {
                conversation.receiver = conversation.last_receiver;
            }

            conversation.unread = _.filter(unread, function(message) {
                return message.conversation_id == conversation.id;
            });

            return conversation;
        });

        $scope.conversations = _.filter(_conversations, function(conversation) {
            return conversation.receiver;
        });

        $scope.clearUnread = function(conversation) {
            if (conversation.unread) {
                $rootScope.unread = _.without($rootScope.unread, conversation.unread);
                conversation.unread = [];
            }
        }

        $scope.isAttachment = function(message) {
            return message && message.startsWith("is_attachment");
        }

        // Clear unread on load route
        $API.all('messaging/mark_receipts_as_read').post();

        $scope.newConversation = function(user) {
            ngDialog.open({
                templateUrl: "NewConversation",
                controller: "NewConversationController",
                data: {
                    user: user,
                    conversations: $scope.conversations
                }
            }).closePromise.then(function(data) {
                var conversation = data.value;
                if (conversation == "$document") return;
                $state.reload().then(function() {
                    var prefix = $state.current.name.split(".")[0];
                    $state.go(prefix + '.messages.one', { id: conversation.id }, { reload: true, inherit: false });
                })
            });
        }

        if ($state.params.new && LocalService.get("workstation-new-conversation")) {
            var userToMessage = JSON.parse(LocalService.get("workstation-new-conversation"));
            var conversation = _.find($scope.conversations, function(conversation) {
                return conversation.receiver.id == userToMessage.id && conversation.receiver.email == userToMessage.email;
            });

            if (conversation) {
                var prefix = $state.current.name.split(".")[0];
                $state.go(prefix + '.messages.one', { id: conversation.id }, { reload: true, inherit: false });
            } else {
                $scope.newConversation(userToMessage);
            }

            LocalService.unset("workstation-new-conversation");
        }
    }
])

.controller('NewConversationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.users = [];
        var userToMessage = $scope.ngDialogData.user;
        var conversations = $scope.ngDialogData.conversations;
        var userId = $rootScope.User.id;

        if (userToMessage) {
            $scope.compose = {
                recipient: userToMessage
            }
        }

        $scope.search = function(query) {
            $scope.searching = true;
            $API.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                populateConversations(response.plain());
                $scope.searching = false;
            }, function() {
                $scope.searching = false;
            });
        }

        function populateConversations(response) {
            $scope.users = _.map(response, function(user) {
                var existingConversation = _.find(conversations, function(conversation) {
                    return conversation.receiver.id == user.id;
                });

                if (existingConversation) {
                    user.conversation = existingConversation;
                }

                return user;
            })
        }

        $scope.converseWith = function(user) {
            if (user.conversation) {
                $scope.closeThisDialog(user.conversation);
            } else {
                $scope.compose = {
                    recipient: user
                }
            }
        }

        $scope.start = function(body) {
            var recipient_type = $scope.compose.recipient.is_admin ? "Admin" : "User";
            $API.all('messaging/send_message').post({
                subject: 'Hi',
                message: body,
                recipient_id: $scope.compose.recipient.id,
                recipient_type: recipient_type
            }).then(function(conversation) {
                $scope.closeThisDialog(conversation);
            }, function(response) {
                $rootScope.$broadcast('conversationStarted', false);
                Notification.error("Could not start conversation", response);
            });
        }
    }
])

.controller('ConversationController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'conversations', 'receipts', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, conversations, receipts, ngDialog) {
        $scope.receipts = _.map(receipts, function(receipt) {
            if (receipt.message.body.startsWith("is_attachment")) {
                var attachment = receipt.message.body.substring(14, receipt.message.body.length);
                receipt.attachment = JSON.parse(attachment);
            } else {
                receipt.attachment = null;
            }

            return receipt;
        });

        $scope.conversation = _.find(conversations, function(c) {
            return c.id == $state.params.id;
        });

        if (!$scope.conversation) $state.go('^');

        $scope.send = function(message) {
            if ($scope.sending_message) return;

            $scope.sending_message = true;
            $API.all('messaging/reply_to_conversation/' + $state.params.id).post({
                message: message
            }).then(function(conversation) {
                $state.reload();
            }, function(response) {
                $scope.sending_message = false;
                $rootScope.$broadcast('sendMessage', false);
                Notification.error("Could not start conversation", response);
            });
        }

        $scope.$watch('attachment', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.attachment = null;
                return;
            };

            ngDialog.open({
                templateUrl: "UploadAttachment",
                controller: "UploadAttachment",
                data: {
                    file: newFile
                }
            })
        });

        $scope.delete = function(r) {
            $API.one('messaging/receipts', r.id).remove().then(function() {
                $state.reload();
                Notification.success("Deleted", "Your message was successfully deleted");
            }, function(response) {
                $rootScope.$broadcast('deletedMessage', false);
                Notification.error("Could not delete message", response);
            });
        }

        $scope.leaveConversation = function() {
            $API.one('messaging', $state.params.id).remove().then(function() {
                $state.reload();
                Notification.success("All done", "You have been removed from this conversation");
            })
        }
    }
])

.controller('UploadAttachment', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'FileHelper',
    function($scope, $rootScope, $state, $API, Notification, FileHelper) {
        $scope.file = $scope.ngDialogData.file;

        FileHelper.upload($scope.file).success(function(response) {
            sendUpload(response);
        }, function(response) {
            $rootScope.$broadcast('uploadedAttachment', false);
            Notification.error("Could not upload file", response);
        });

        var sendUpload = function(upload) {
            var message = "is_attachment:" + JSON.stringify(upload);
            $API.all('messaging/reply_to_conversation/' + $state.params.id).post({
                message: message
            }).then(function(conversation) {
                $state.reload().then(function() {
                    $scope.closeThisDialog();
                });
            }, function(response) {
                $rootScope.$broadcast('uploadedAttachment', false);
                Notification.error("Could not upload file", response);
            });
        }
    }
])

angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.account', {
                url: '/account',
                templateUrl: 'modules/user/account/index.html',
                controller: 'UserAccountController',
                redirectTo: 'user.account.profile',
                resolve: {
                    subscription: ['$rootScope', '$state', '$auth', 'Subscription', function($rootScope, $state, $auth, Subscription) {
                        return Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
            .state('user.account.profile', {
                url: '/profile',
                title: 'Profile',
                templateUrl: 'modules/user/account/pages/profile.html',
                controller: 'UserAccountController',
            })
            .state('user.account.social', {
                url: '/social',
                title: 'Social',
                templateUrl: 'modules/user/account/pages/social.html',
                controller: 'UserAccountController',
            })
            .state('user.account.membership', {
                url: '/membership',
                title: 'Membership',
                templateUrl: 'modules/user/account/pages/membership.html',
                controller: 'UserMembershipController',
                resolve: {
                    purchases: ['$API', function($API) {
                        return $API.all('purchases').getList();
                    }]
                }
            })
            .state('user.account.addons', {
                url: '/addons',
                title: 'Add-ons',
                templateUrl: 'modules/user/account/pages/addons.html',
                controller: 'UserAddonController',
                resolve: {
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }]
                }
            })
            .state('user.account.team', {
                url: '/team',
                title: 'Team',
                templateUrl: 'modules/user/account/pages/team.html',
                controller: 'UserTeamSettingsController',
                resolve: {
                    invitations: ['$API', function($API) {
                        return $API.all('team_invitations').getList();
                    }]
                }
            })
            .state('user.account.payment', {
                url: '/payment',
                title: 'Payment',
                templateUrl: 'modules/user/account/pages/payment.html',
                controller: 'UserPaymentController',
                resolve: {
                    inline: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('https://js.paystack.co/v1/inline.js');
                    }]
                }
            })
            .state('user.account.invoices', {
                url: '/invoices',
                title: 'Invoices',
                templateUrl: 'modules/user/account/pages/invoices.html',
                controller: 'UserBillingController'
            })
            .state('user.account.purchases', {
                url: '/purchases',
                title: 'Purchases',
                templateUrl: 'modules/user/account/pages/purchases.html',
                controller: 'UserBillingController'
            })
            .state('user.account.printing', {
                url: '/printing',
                title: 'Printing',
                templateUrl: 'modules/user/account/pages/printing.html',
                controller: 'UserPrintingController'
            })
    }
])

.controller('UserAccountController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification', 'FileHelper', 'Session',
    function($scope, $rootScope, $state, $auth, $API, Notification, FileHelper, Session) {
        $scope.profile = _.clone($rootScope.User);

        $scope.updateAccount = function() {
            var payload = _.clone($scope.profile);
            if (payload.new_email) payload.email = payload.new_email;
            $API.one('users', $scope.profile.id).customPUT(payload)
                .then(Session.update)
                .then(function() {
                    $scope.closeThisDialog();
                    $state.reload();
                    Notification.success("All Done!", "Your account has been updated");
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $API.one('users', $scope.profile.id).customPUT({ image: response.url }).then(function() {
                    $scope.profile.image = response.url;
                    $scope.avatar = null;
                });
            }).catch(function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });

        $scope.removeImage = function() {
            $API.one('users', $scope.profile.id).customPUT({ image: null }).then(function() {
                $scope.profile.image = null;
            });
        }
    }
])

.controller('UserMembershipController', ['$scope', '$rootScope', '$state', 'Session', '$auth', '$API', 'Notification', 'ngDialog', 'purchases',
    function($scope, $rootScope, $state, Session, $auth, $API, Notification, ngDialog, purchases) {
        $scope.moment = moment;
        $scope.profile = _.omit($rootScope.User, 'subscription');
        $scope.subscription = _.clone($rootScope.User.subscription);
        $scope.plan = _.clone($rootScope.User.plan);

        if ($scope.subscription && $scope.subscription.days) {
            $scope.subscription.days = JSON.parse($scope.subscription.days);
        }

        $scope.purchases = _.filter(purchases, function(purchase) {
            return purchase.active && purchase.add_on && !purchase.add_on.system;
        });

        $scope.buyTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyTokensController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.buyPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyPrintingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.cancelSubscription = function() {
            ngDialog.openConfirm({
                template: "CancelSubscription",
                controller: ['$scope', '$state', '$API', 'Session', 'Notification', function($scope, $state, $API, Session, Notification) {
                    $scope.cancel = function() {
                        $API.all('subscriptions/cancel').post()
                            .then(Session.update)
                            .then(function() {
                                Notification.success('Subscription cancelled', "Your current subscription has been cancelled");
                                $state.reload();
                                $scope.closeThisDialog();
                            })
                            .catch(function(response) {
                                $rootScope.$broadcast('cancelledSubscription', false);
                                Notification.error("Could not cancel subscription", response);
                            });
                    }
                }]
            });
        }

        $scope.changeDays = function() {
            ngDialog.openConfirm({
                templateUrl: "ChangeDays",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }]
                },
                data: { subscription: $scope.subscription },
                controller: 'ChangeDaysController'
            });
        }
    }
])

.controller('ChangeDaysController', ['$scope', '$rootScope', '$API', 'Session', 'Notification', 'plans', function($scope, $rootScope, $API, Session, Notification, plans) {
    $scope.subscription = $scope.ngDialogData.subscription;
    $scope.plan = _.findWhere(plans, { id: $scope.subscription.plan_id });

    $scope.days = { monday: false, tuesday: false, wednesday: false, thursday: false, friday: false };
    _.each($scope.subscription.days, function(day) { $scope.days[day] = true });

    $scope.addDays = function() {
        $scope.subscription.days = [];
        _.each($scope.days, function(value, key) {
            if (value) $scope.subscription.days.push(key);
        })
    }

    $scope.update = function() {
        $API.one('subscription', $scope.subscription.id)
            .customPUT({
                days: $scope.subscription.days
            })
            .then(Session.update)
            .then(function() {
                Notification.success("All Done!", "Your subscription has been updated");
                $state.reload();
                $scope.closeThisDialog();
            })
            .catch(function(response) {
                $rootScope.$broadcast('updatedSubscription', false);
                Notification.error("Could not update your subscription", response);
            });
    }
}])

.controller('BuyTokensController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'prices',
    function($scope, $rootScope, $state, Session, $API, Notification, prices) {
        var defaults = $scope.ngDialogData || {};
        var system_token = _.findWhere(prices.plain(), { slug: "token" }) || {};

        $scope.token_cost = system_token.price;
        $scope.tokens = defaults.amount;
        $scope.use_team_card = false;

        $scope.buy = function() {
            $API.all('purchases')
                .post({
                    add_on_id: system_token.id,
                    quantity: $scope.tokens,
                    use_team_card: $scope.use_team_card
                })
                .then(Session.update)
                .then(function() {
                    Notification.success('Tokens Purchased', "Your token count has been updated");
                    $state.reload();
                    $scope.closeThisDialog();
                }).catch(function(response) {
                    $rootScope.$broadcast('tokensPurchased', false);
                    Notification.error("Could not purchase tokens", response);
                });
        }
    }
])

.controller('BuyPrintingController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'prices',
    function($scope, $rootScope, $state, Session, $API, Notification, prices) {
        var defaults = $scope.ngDialogData || {};

        var system_bw_printing = _.findWhere(prices.plain(), { slug: "b-w-printing" }) || {};
        var system_rgb_printing = _.findWhere(prices.plain(), { slug: "colour-printing" }) || {};
        $scope.use_team_card = false;

        $scope.rgb_printing_cost = system_rgb_printing.price;
        $scope.bw_printing_cost = system_bw_printing.price;

        $scope.bw_printing = defaults.bw_printing;
        $scope.rgb_printing = defaults.rgb_printing;
        $scope.total = 0;

        $scope.computeTotal = function() {
            $scope.total = (($scope.bw_printing || 0) * $scope.bw_printing_cost) + (($scope.rgb_printing || 0) * $scope.rgb_printing_cost);
        }

        $scope.buy = function() {
            var tasks = 0;

            if ($scope.bw_printing) {
                tasks++;
                $API.all('purchases').post({
                    add_on_id: system_bw_printing.id,
                    quantity: $scope.bw_printing,
                    use_team_card: $scope.use_team_card
                }).then(function() {
                    onComplete();
                    Notification.success('B/W Printing Purchased', "Your printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingPurchased', false);
                    Notification.error("Could not purchase b/w printing", response);
                });
            }

            if ($scope.rgb_printing) {
                tasks++;
                $API.all('purchases').post({
                    add_on_id: system_rgb_printing.id,
                    quantity: $scope.rgb_printing,
                    use_team_card: $scope.use_team_card
                }).then(function() {
                    onComplete();
                    Notification.success('Color Printing Purchased', "Your printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingPurchased', false);
                    Notification.error("Could not purchase color printing", response);
                });
            }

            var onComplete = function() {
                tasks--;
                if (tasks == 0) {
                    $rootScope.$broadcast('printingPurchased', true);
                    Session.update().then(function() {
                        $scope.closeThisDialog();
                        $state.reload();
                    })
                }
            }
        }
    }
])

.controller('UserTeamSettingsController', ['$scope', '$rootScope', '$state', '$auth', 'Session', '$API', 'ngDialog', 'Notification', 'invitations',
    function($scope, $rootScope, $state, $auth, Session, $API, ngDialog, Notification, invitations) {
        $scope.moment = moment;
        $scope.profile = _.clone($rootScope.User);
        $scope.invitations = invitations;

        if ($scope.profile.is_team_admin) {
            $state.go('user.team');
            return;
        }

        if ($scope.profile.has_team) {
            $API.one('teams', $rootScope.User.team_id).one('members').get()
                .then(function(team) {
                    $scope.team = team;
                    $scope.team_admin = _.findWhere(team.members, { role: "is_admin" }).user;
                });
        }

        $scope.leaveTeam = function() {
            ngDialog.openConfirm({
                templateUrl: "LeaveTeam",
                scope: $scope
            }).then(function() {
                $API.one('teams', $rootScope.User.team_id).all('leave').remove()
                    .then(Session.update)
                    .then(function() {
                        Notification.success('Successful', "Your have successfully left this team");
                        $state.reload();
                    }).catch(function(response) {
                        $rootScope.$broadcast('leftTeam', false);
                        Notification.error("Could not leave team", response);
                    });
            });
        }

        $scope.createTeam = function() {
            $API.all('teams').post()
                .then(Session.update)
                .then(function() {
                    Notification.success('Team Created', "Your team has been created");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('teamCreated', false);
                    Notification.error("Could not create team", response);
                });
        }

        $scope.acceptInvitation = function(id) {
            $API.one('team_invitations/' + id, 'accept').get()
                .then(Session.update)
                .then(function() {
                    Notification.success('Invitation accepted', "You are now a member of this team");
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('invitationAccepted', false);
                    Notification.error("Could not accept invitation", response);
                });
        }

        $scope.cancelInvitation = function(id) {
            $API.one('team_invitations', id).remove()
                .then(function() {
                    Notification.success('Invitation cancelled', "You can now create your own team");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('cancelledInvitation', false);
                    Notification.error("Could not cancel invitation", response);
                });
        }
    }
])


.controller('UserAddonController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Session', 'ngDialog', 'Notification', 'addons',
    function($scope, $rootScope, $state, $auth, $API, Session, ngDialog, Notification, addons) {
        $scope.profile = _.clone($rootScope.User);
        if ($rootScope.User.subscription) $scope.profile.days = JSON.parse($rootScope.User.subscription.days);
        $scope.addons = addons;
        $scope.use_team_card = false;

        $scope.confirmPurchase = function(addon) {
            $scope.selected_addon = addon;
            ngDialog.open({
                templateUrl: "BuyAddon",
                scope: $scope
            });
        }

        $scope.buy = function(addon, callback) {
            $API.all('purchases')
                .post({
                    add_on_id: addon.id,
                    use_team_card: $scope.use_team_card
                })
                .then(Session.update)
                .then(function() {
                    callback();
                    Notification.success("Addon Purchased", addon.name + " has been added to your subscription");
                    $state.go('user.account.membership');
                })
                .catch(function(response) {
                    $rootScope.$broadcast('addonPurchased', false);
                    Notification.error("Could not purchase this addon", response);
                });
        }
    }
])

.controller('UserPaymentController', ['$scope', '$rootScope', '$state', 'ngDialog', '$API', 'Session', 'Notification', 'DEFAULTS',
    function($scope, $rootScope, $state, ngDialog, $API, Session, Notification, DEFAULTS) {
        $scope.auth = $rootScope.User.paystack_authorization;

        $scope.addCard = function() {
            ngDialog.openConfirm({
                templateUrl: "AddCard",
                scope: $scope
            }).then(function() {
                $scope.loadPaystack();
            });
        }

        var onClose = function() {
            $rootScope.$broadcast('paystackOpen', false);
        }

        var callback = function(response) {
            $API.all('users/paystack_authorizations').post({ reference: response.reference })
                .then(Session.update)
                .then(function() {
                    $rootScope.$broadcast('paystackOpen', true);
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('paystackOpen', false);
                    Notification.error("Could not add your card", response);
                });
        }

        $scope.loadPaystack = function() {
            var handler = PaystackPop.setup({
                key: DEFAULTS.paystackKey,
                email: $rootScope.User.email,
                amount: 5000,
                onClose: onClose,
                callback: callback
            });

            handler.openIframe();
        }
    }
])

.controller('UserBillingController', ['$scope', '$rootScope', '$state', '$auth', '$FullAPI', 'Notification',
    function($scope, $rootScope, $state, $auth, $FullAPI, Notification) {

        $scope.load = function(resource, page) {
            var page = page || 1;
            $FullAPI.all(resource).getList().then(function(response) {
                var meta = {
                    total: response.headers("total"),
                    perPage: 25,
                    page: page,
                    pageCount: Math.round(response.headers("total") / 25)
                }
                $scope[resource] = response.data;
                $scope[resource].meta = meta;
            })
        }

    }
])

.controller('UserPrintingController', ['$scope', '$rootScope', '$state', '$auth', '$API', 'Notification', 'Session', 'ngDialog',
    function($scope, $rootScope, $state, $auth, $API, Notification, Session, ngDialog) {

        $scope.setup = function(password) {
            $API.all('printing/setup_print').post({
                    password: password
                })
                .then(Session.update)
                .then(function() {
                    $state.reload();
                })
                .catch(function(response) {
                    $scope.$broadcast('setup_print', false);
                    Notification.error("Print setup failed", response);
                });
        }

        $scope.buyPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyPrintingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.updatePrintPassword = function(password, callback) {
            $API.all('printing/update_print_password').post({
                    password: password
                })
                .then(Session.update)
                .then(function() {
                    callback();                    
                    Notification.success("Password Changed", "You can now authenticate with your new password");
                    $state.reload();
                })
                .catch(function(response) {
                    $scope.$broadcast('passwordSaved', false);
                    Notification.error("Password Change failed", response);
                });
        }
    }
]);

angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.community', {
                url: '/community?search&network_only&page',
                templateUrl: 'modules/user/community/index.html',
                controller: 'UserCommunityController',
                skeleton: true,
                title: "Community",
                resolve: {
                    subscription: ['$rootScope', '$state', 'Subscription', function($rootScope, $state, Subscription) {
                        Subscription.verify('can_message').then(function() {}, function() {
                            $state.go('user.home');
                        })
                    }],
                    members: ['$FullAPI', '$rootScope', '$stateParams', function($FullAPI, $rootScope, $stateParams) {
                        if ($stateParams.search) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else if ($stateParams.network_only) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else {
                            return $FullAPI.all('users').getList($stateParams);
                        }
                    }]
                }
            })
    }
])

.controller('UserCommunityController', ['$scope', '$rootScope', '$state', '$API', '$FullAPI', '$ocLazyLoad', 'LocalService', 'Notification', 'ngDialog', 'members',
    function($scope, $rootScope, $state, $API, $FullAPI, $ocLazyLoad, LocalService, Notification, ngDialog, members) {

        function setMembers(response) {
            var _members = response || members;

            $scope.members = _members.data;
            $scope.meta = {
                total: _members.headers("total"),
                perPage: 25,
                page: $state.params.page,
                pageCount: Math.round(_members.headers("total") / 25)
            }

            $scope.filteredCompanies = {};
            $scope.companies = _.uniq(_.filter(_.pluck(members.data, "company")));
            $scope.companyFilter = function(member) {
                var isFiltered = _.some(_.values($scope.filteredCompanies));
                return !isFiltered || $scope.filteredCompanies[member.company];
            };
        }

        setMembers();

        $scope.reload = function(params) {
            $state.go($state.current, params, {
                reload: true,
                inherit: false,
                notify: true
            })
        }

        $scope.view = function(member) {
            ngDialog.open({
                templateUrl: "modules/user/community/modals/member.html",
                controller: "UserMemberController",
                trapFocus: false,
                data: {
                    member: member
                }
            });
        }

        $scope.find = function(query) {
            $scope.searching = true;
            $FullAPI.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.searching = false;
                setMembers(response);
            }, function() {
                $scope.searching = false;
            });
        }

        $ocLazyLoad.load('modules/common/feed/facebook.js');

        $scope.inviteMessage = "‪Join Workstation Today!, a coworking space/community where collaboration meets critical thinking";
        $scope.share = function(message, network, callback) {
            var url = "https://workstationng.com";

            switch (network) {
                case 'facebook':
                    FB.ui({
                        method: 'share',
                        href: "https://www.facebook.com/Workstation-Nigeria-1729334683992057",
                        quote: message
                    }, function(response) {});

                    break;
                case 'twitter':
                    var twitter_url = "https://twitter.com/intent/tweet/?";
                    twitter_url += "&text=" + message.trunc(120);
                    if (url) twitter_url += "&url=" + url;
                    twitter_url += "&via=workstationng";
                    window.open(twitter_url, '_blank');
                    break;
                case 'linkedin':
                    var linkedin_url = "http://www.linkedin.com/shareArticle?mini=true";
                    linkedin_url += "&title=" + message;
                    linkedin_url += "&url=" + url;
                    linkedin_url += "&source=WorkstationNigeria";
                    window.open(linkedin_url, '_blank');
                    break;
                case 'email':
                    $API.all('users/invite').post({
                        invite_email: message
                    }).then(function() {
                        callback();
                        $scope.$broadcast('emaiInviteSent', true);
                        Notification.success('Invite sent', "An invitation has been sent to " + message);
                    }, function(response) {
                        $scope.$broadcast('emaiInviteSent', false);
                        Notification.error("Could not send invite", response);
                    });
                    break;
            }
        }
    }
])

.controller('UserMemberController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        $scope.member = $scope.ngDialogData.member;
        $rootScope.title = $scope.member.first_name + " " + $scope.member.last_name;

        $scope.message = function(member) {
            $scope.closeThisDialog();
            LocalService.set('workstation-new-conversation', JSON.stringify(member));
            $state.go('user.messages', { new: member.email });
        }
    }
]);

angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.home', {
                url: '/?feed',
                templateUrl: 'modules/user/home/index.html',
                controller: 'UserHomeController',
                skeleton: true,
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/getstream/';
                        var files = [];

                        files.push(folder + 'getstream.js');
                        files.push(folder + 'stream-angular.js');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    categories: ['$API', function($API) {
                        return $API.all('feed_categories').getList();
                    }],
                    subscription: ['$rootScope', '$state', '$auth', '$window', 'Subscription', function($rootScope, $state, $auth, $window, Subscription) {
                        Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
            .state('user.home.subscribe', {
                url: 'subscribe',
                templateUrl: 'modules/user/home/subscription.html',
                controller: 'UserHomeSubscriptionController',
                skeleton: true,
                resolve: {
                    verified: ['$rootScope', 'ngDialog', 'Notification', '$state', '$q', 'Subscription', function($rootScope, ngDialog, Notification, $state, $q, Subscription) {
                        var deferred = $q.defer();

                        Subscription.verify().then(function() {
                            deferred.reject();
                            $state.go('user.home');
                        }, function() {
                            if ($rootScope.User.verified) {
                                deferred.resolve();
                            } else {
                                ngDialog.open({
                                    templateUrl: "/modules/user/home/modals/verify.html",
                                    controller: ['$API', 'Notification', '$scope', function($API, Notification, $scope) {
                                        $scope.resend = function() {
                                            $API.all('users/resend_confirmation').post()
                                                .then(function() {
                                                    Notification.success('Sent', "Your verification email was resent");
                                                    $scope.closeThisDialog();
                                                })
                                                .catch(function(response) {
                                                    $rootScope.$broadcast('verificationResent', false);
                                                    Notification.error("Could not resend verification email", response);
                                                });
                                        }
                                    }]
                                });
                            }
                        });

                        return deferred.promise;
                    }],
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }],
                    subscription_logs: ['$API', function($API) {
                        return $API.all('subscription_logs').getList();
                    }]
                }
            })
    }
])

.controller('UserHomeController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'categories',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, categories) {
        $scope.categories = categories;
        $scope.activeCategory = _.find(categories, function(cat) {
            return cat.id == $state.params.feed
        });

        $scope.switchTo = function(category) {
            $state.go($state.current.name, {
                feed: category.id
            }, { reload: true });
        }

        var hasHomeLocation = $rootScope.User.home_location || $rootScope.User.home_location_id;
        if (!hasHomeLocation) {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/home/onboarding.html",
                controller: "UserOnboardingController",
                className: 'ngdialog-fullscreen',
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        };

        $scope.contactAdmin = function() {
            ngDialog.open({
                templateUrl: "/modules/user/home/modals/admins.html",
                resolve: {
                    admins: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.one('users', $rootScope.User.id).all('admins').getList();
                    }]
                },
                controller: ['$state', '$scope', 'LocalService', 'admins', function($state, $scope, LocalService, admins) {
                    admins = _.map(admins, function(admin) {
                        admin.is_admin = true;
                        return admin
                    });

                    $scope.admins = _.reject(admins, 'superadmin');

                    $scope.sendMessage = function(admin) {
                        LocalService.set('workstation-new-conversation', JSON.stringify(admin));
                        $state.go('user.messages', { new: admin.email });
                        $scope.closeThisDialog();
                    }
                }]
            });
        }

        $scope.reportBug = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/home/modals/report-bug.html",
                controller: ['$state', '$scope', 'LocalService', 'FileHelper', function($state, $scope, LocalService, FileHelper) {
                    $scope.report = {};
                    $scope.$watch('attachment', function(newFile, oldFile) {
                        if (!newFile || newFile == oldFile) return;

                        $scope.uploading = true;

                        var fileSize = newFile.size / 1024 / 1024;
                        if (fileSize > 5) {
                            Notification.error("File limit exceeded", "Please upload a file below 5MB");
                            $scope.attachment = null;
                            $scope.uploading = false;
                            return;
                        };

                        FileHelper.upload(newFile).success(function(response) {
                            $scope.report.attachment = response.url;
                            $scope.uploading = false;
                        }, function(response) {
                            $scope.uploading = false;
                            Notification.error("Could not upload file", response);
                        });
                    });
                }]
            }).then(function(report) {
                $API.all('users/report_bug').post(report).then(function() {
                    Notification.success('Thanks!', "We'll review this bug report as soon as possible.");
                });
            });
        }
    }
])

.controller('TodayController', ['$scope', '$rootScope', '$state', '$API', '$filter', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, $filter, LocalService, Notification, ngDialog) {

        var isToday = function(time) {
            var today = new Date();
            var eventDay = new Date(time);
            return today.toDateString() === eventDay.toDateString();
        }

        $API.all('bookings').getList().then(function(bookings) {
            bookings = _.filter(bookings, 'booker');
            _.each(bookings, function(booking) {
                booking.self = booking.booker.uid == $rootScope.User.uid && booking.booker.id == $rootScope.User.id;

                if (booking.self && isToday(booking.start_time)) {
                    $scope.events = $scope.events || [];
                    $scope.events.push({
                        is_booking: true,
                        title: booking.title,
                        time: $filter('date')(booking.start_time, 'h:mma') + "-" + $filter('date')(booking.end_time, 'h:mma'),
                        // location: booking.location.description
                    });
                }
            })
        });

        $API.all('events').getList().then(function(events) {
            _.each(events, function(event) {
                if (isToday(event.time)) {
                    $scope.events = $scope.events || [];
                    $scope.events.push({
                        title: event.name,
                        time: $filter('date')(event.time, 'h:mma'),
                        location: event.location.description
                    });
                }
            });
            $scope.events = $scope.events || [];
        });
    }
]);

angular.module('app.user')

.controller('UserOnboardingController', ['$scope', '$rootScope', '$state', 'Session', '$timeout', '$auth', '$API', 'Notification', 'FileHelper',
    function($scope, $rootScope, $state, Session, $timeout, $auth, $API, Notification, FileHelper) {
        $scope.step = 1;
        $scope.profile = $rootScope.User;

        $scope.updateAccount = function() {
            $API.one('users', $scope.profile.id).customPUT($scope.profile)
                .then(function() {
                    $auth.updateAccount($scope.profile).then(function() {
                        $scope.step = 2;
                    })
                }, function(response) {
                    $rootScope.$broadcast('accountUpdated', false);
                    Notification.error("Could not update your account", response);
                });
        }

        $scope.$watch('avatar', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.avatar = null;
                return;
            };

            FileHelper.upload(newFile, 'imageUpload').success(function(response) {
                $scope.profile.image = response.url;
                $scope.avatar = null;
            }, function(response) {
                $scope.avatar = null;
                Notification.error("Avatar upload failed", response);
            });
        });

        $scope.close = function() {
            Session.update()
                .then(function() {
                    $scope.closeThisDialog();
                });
        }
    }
]);

angular.module('app.user')

.controller('UserHomeSubscriptionController', ['$scope', '$state', '$rootScope', 'Session', '$API', 'plans', 'offices', 'subscription_logs', 'Notification', 'ngDialog',
    function($scope, $state, $rootScope, Session, $API, plans, offices, subscription_logs, Notification, ngDialog) {
        if ($rootScope.User._subscription) {
            $scope.current_plan = _.findWhere(plans, { id: $rootScope.User._subscription.plan_id });
            if ($scope.current_plan) {
                $scope.current_subscription = $rootScope.User._subscription;
                $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
                $scope.renew = function() {
                    $scope.pay($scope.current_subscription);
                }
            }
        }

        $scope.offices = _.filter(offices, function(office) {
            return office.limit === null || office.limit > 0
        });

        $scope.plans = _.filter(plans, function(plan) {
            return plan.limit === null || plan.limit > 0
        });

        $scope.subscription_logs = subscription_logs;

        $scope.pay = function(subscription) {
            $API.all('subscriptions').post(subscription)
                .then(Session.update)
                .then(function() {
                    Notification.success('Whoop!', "Your subscription was successful");
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe to this plan", response);
                });
        }
    }
])

.controller('UserSubscriptionHelpers', ['$scope', '$rootScope', '$state', '$filter', function($scope, $rootScope, $state, $filter) {
    $scope.plans = _.map($scope.plans, function(plan) {
        plan.intervals = [];

        function formatPrice(price) {
            return $filter('currency')(price, 'NGN ', 2);
        }

        if (plan.daily_price) plan.intervals.push({ name: "Daily (" + formatPrice(plan.daily_price) + ")", slug: "daily", price: plan.daily_price });
        if (plan.monthly_price) plan.intervals.push({ name: "Monthly (" + formatPrice(plan.monthly_price) + ")", slug: "monthly", price: plan.monthly_price });
        if (plan.yearly_price) plan.intervals.push({ name: "Yearly (" + formatPrice(plan.yearly_price) + ")", slug: "yearly", price: plan.yearly_price });

        var ints = ['daily_price', 'monthly_price', 'yearly_price', 'setup_fee'];
        _.each(ints, function(int) {
            plan[int] = parseInt(plan[int]);
        });

        return plan;
    });

    $scope.toPaySetupFee = function(user_id, plan) {
        if (!plan.setup_fee) return false;
        return _.findWhere($scope.subscription_logs, { user_id: user_id, plan_id: plan.id }) == null;
    }

    $scope.subscribeTo = function(plan, team_member) {
        $scope.subscription = {
            plan_id: plan.id,
            interval: plan.intervals[0].slug,
            days: []
        }

        if (team_member) {
            $scope.subscription.user_id = team_member.id;
        }

        $scope.active_plan = plan;
        if (plan.days < 5) {
            $scope.days = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false
            }
        } else {
            $scope.subscription.days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
        }
    }

    $scope.addDays = function() {
        $scope.subscription.days = [];
        _.each($scope.days, function(value, key) {
            if (value) $scope.subscription.days.push(key);
        })
    }

    $scope.toggleDay = function(day) {
        if (_.contains($scope.subscription.days, day)) {
            $scope.subscription.days = _.without($scope.subscription.days, day);
        } else {
            $scope.subscription.days.push(day);
        }
    }
}])

.controller('UserOfficeSubscriptionController', ['$scope', '$rootScope', '$state', '$API', 'Session', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Session, LocalService, Notification, ngDialog) {
        $scope.team_sizes = {
            max: _.max($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces;
            }),
            min: _.min($scope.offices, function(_office) {
                return _office.seats_no;
            })
        }


        $scope.checkAvailability = function() {
            $scope.office = null;
            var suitable_offices = _.filter($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces >= $scope.office_seats;
            });

            if (suitable_offices.length) {
                $scope.office = _.min(suitable_offices, function(_office) {
                    return _office.seats_no;
                });

                var intervals = [];
                if ($scope.office.monthly_price) intervals.push({ name: "Monthly", slug: "monthly", price: $scope.office.monthly_price });
                if ($scope.office.yearly_price) intervals.push({ name: "Yearly", slug: "yearly", price: $scope.office.yearly_price });
                $scope.office.intervals = intervals;

                $scope.office_interval = $scope.office.intervals[0].slug;
                $scope.calculateCost($scope.office_interval);
            }
        }

        $scope.calculateCost = function(interval) {
            var interval = _.find($scope.office.intervals, { slug: interval });
            $scope.office.seat_cost = interval.price;
            $scope.office.total_cost = $scope.office_seats * interval.price;
            if ($scope.office.setup_fee) {
                $scope.office.total_cost += ($scope.office.setup_fee * $scope.office_seats);
            }
        }

        $scope.subscribe = function(office) {
            $API.all('group_plan_subscriptions')
                .post({
                    group_plan_id: office.id,
                    interval: $scope.office_interval,
                    no_of_seats: $scope.office_seats
                })
                .then(Session.update)
                .then(function() {
                    Notification.success('Whoop!', "Your subscription was successful");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe for this office", response);
                });
        }

        $scope.book = function(office) {
            $API.all('users/request_plan')
                .post({
                    group_plan_id: office.id
                })
                .then(function() {
                    $scope.$broadcast('adminNotified', true);
                    Notification.success('Office booked', 'An admin has been notified and will be in touch.');
                }).catch(function(response) {
                    $rootScope.$broadcast('adminNotified', false);
                    Notification.error("Could not book this office", response);
                });
        }
    }
]);

angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.team', {
                url: '/team?search&network_only&page',
                templateUrl: 'modules/user/team/index.html',
                controller: 'UserTeamController',
                resolve: {
                    subscription: ['$rootScope', '$state', '$auth', 'Subscription', function($rootScope, $state, $auth, Subscription) {
                        return Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
    }
])

.controller('UserTeamController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        if (!$rootScope.User.has_team || !$rootScope.User.is_team_admin) {
            $state.go('user.account.team');
            return;
        };

        $API.one('teams', $rootScope.User.team_id).one('members').get().then(function(team) {
            var office = _.filter(team.members, function(registration) {
                return registration.role === "has_seat"
            });
            var organization = _.filter(team.members, function(registration) {
                return registration.role === "is_member"
            });
            var admin = _.findWhere(team.members, { role: "is_admin" });

            if ($rootScope.User.has_office) {
                office.unshift(admin);
            } else {
                organization.unshift(admin);
            }

            $scope.office = office;
            $scope.organization = organization;
            $scope.team = team;
        });

        $API.one('teams', $rootScope.User.team_id).all('invitations').getList().then(function(invitations) {
            $scope.invitations = invitations;
        });

        $scope.format = function(member) {
            member.team_admin = member.user.id == $rootScope.User.id;

            if (!member.user.subscription || !member.user.subscription.enabled) {
                member.subscription = "No current subscription";
            } else if (member.user.group_plan_subscription) {
                member.subscription = "Shared Office";
            } else {
                member.subscription = member.user.subscription_plan_name + " " + member.user.subscription.interval;
            }

            if (member.team_admin) {
                member.privilege = "Team admin";
            } else {
                member.privilege = member.use_team_card ? "Can " : "Cannot ";
                member.privilege += "use team card";
            }

            return member;
        }

        $scope.available_seats = function() {
            var seat_invitations = _.filter($scope.invitations, function(invite) {
                return invite.role == "has_seat";
            });
            var available_seats = $rootScope.User.subscription.seats_left - seat_invitations.length;
            return new Array(available_seats);
        }

        $scope.cancelInvite = function(id) {
            $API.one('team_invitations', id).remove().then(function() {
                Notification.success('Invitation deleted', "User will not longer be part of team");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('deletedInvitation', false);
                Notification.error("Could not delete invitation", response);
            });
        }

        $scope.manage = function(member) {
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/member.html",
                resolve: {
                    member: [function() {
                        return member
                    }]
                },
                controller: "TeamMemberController",
                width: 600
            });
        }

        $scope.changeName = function(id, name) {
            $API.one('teams', id).customPUT({
                name: name
            }).then(function() {
                Notification.success('Update Complete', "Team name has been updated");
                $state.reload();
            }, function(response) {
                Notification.error("Could not rename team", response);
            });
        }

        $scope.cancelOffice = function() {
            ngDialog.openConfirm({
                template: "CancelOffice",
                controller: ['$scope', '$API', 'Session', 'Notification', function($scope, $API, Session, Notification) {
                    $scope.cancel = function() {
                        $API.all('users/request_cancellation').post().then(function() {
                            Notification.success('Office Cancellation Requested', "A Workstation admin will be in touch");
                            $scope.closeThisDialog();
                        }, function(response) {
                            Notification.error("Could not notify admin", response);
                        });
                    }
                }]
            });
        }
    }
])

.controller('TeamMemberController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'member',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, member) {
        $scope.member = member;

        $scope.subscribe = function() {
            $scope.closeThisDialog();
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/subscribe.html",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return member.user
                    }],
                    subscription_logs: ['$API', function($API) {
                        return $API.one('users', member.user.id).all('subscription_logs').getList();
                    }]
                },
                controller: "TeamSubscriptionController",
                width: 600
            });
        }

        $scope.removeFromTeam = function(user) {
            $scope.closeThisDialog();
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/remove-from-team.html",
                controller: ['$scope', '$state', '$rootScope', 'Session', '$API', 'Notification', function($scope, $state, $rootScope, Session, $API, Notification) {
                    $scope.user = user;
                    $scope.remove = function() {
                        $API.one('teams', $rootScope.User.team_id).one('remove_member', user.id).remove()
                            .then(Session.update)
                            .then(function() {
                                Notification.success('Successful', "User successfully removed from team");
                                $state.reload();
                                $scope.closeThisDialog();
                            }).catch(function(response) {
                                $rootScope.$broadcast('removedFromTeam', false);
                                Notification.error("Could not remove from team", response);
                            });
                    }
                }]
            });
        }

        $scope.updateRole = function(member) {
            $API.one('teams', $rootScope.User.team_id).all('update_member').customPUT({
                member_id: member.user.id,
                use_team_card: member.use_team_card
            }).then(function() {
                Notification.success('Update successful', "User roles have been updated");
                $state.reload();
            });
        }
    }
])

.controller('TeamInvitationController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        $scope.use_team_card = false;
        $scope.invite = function(options) {
            options = options || {};
            $scope.not_a_member = false;

            var invitation_url = options.has_seat ? 'seat_invitations' : 'invitations';
            $API.one('teams', $rootScope.User.team_id).all(invitation_url).post({
                invite_email: $scope.invite_email,
                use_team_card: $scope.use_team_card
            }).then(function() {
                Notification.success('User Invited', "An invitation has been sent to " + $scope.invite_email);
                $scope.closeThisDialog();
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userInvited', false);
                if (response.data && response.data.errors == "User is not a Workstation member") {
                    $scope.not_a_member = true;
                } else {
                    Notification.error("Could not invite user", response);
                }
            });
        }

        $scope.sendInvite = function() {
            $API.all('users/invite').post({
                invite_email: $scope.invite_email
            }).then(function() {
                Notification.success('Invite sent', "An invitation has been sent to " + $scope.invite_email);
                $scope.closeThisDialog();
                $state.reload();
            }, function(response) {
                Notification.error("Could not send invite", response);
            });
        }

        $scope.search = function(query) {
            $scope.searching = true;
            $scope.users = [];
            $API.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.users = response.plain();
                $scope.searching = false;
            }, function() {
                $scope.searching = false;
            });
        }

        $scope.choose = function(user) {
            $scope.invite_email = user.email;
            $scope.users = [];
        }
    }
])

.controller('TeamSubscriptionController', ['$scope', '$state', '$rootScope', 'Session', '$API', 'plans', 'member', 'subscription_logs', 'Notification',
    function($scope, $state, $rootScope, Session, $API, plans, member, subscription_logs, Notification) {
        $scope.member = member;

        if (member.subscription) {
            $scope.current_plan = _.findWhere(plans, { id: member.subscription.plan_id });
            $scope.current_subscription = member.subscription;
            $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
            $scope.current_subscription.user_id = member.id;

            $scope.renew = function() {
                $scope.pay($scope.current_subscription);
            }
        }

        $scope.plans = plans;
        $scope.subscription_logs = subscription_logs;

        $scope.pay = function(subscription) {
            $API.all('subscriptions').post(subscription)
                .then(function() {
                    Notification.success('All Done!', "Subscription was successful");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Subscription failed", response);
                });
        }
    }
])
