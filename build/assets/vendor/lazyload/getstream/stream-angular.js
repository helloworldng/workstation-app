(function (angular) {

  // Config
  angular.module('streamAngular.config', [])
    .value('streamAngularConfig', {
      debug: true
    });

  // Modules
  angular.module('streamAngular.directives', []);
  angular.module('streamAngular.filters', []);
  angular.module('streamAngular.services', []);
  angular.module('streamAngular',
    [
      'streamAngular.config',
      'streamAngular.directives',
      'streamAngular.filters',
      'streamAngular.services'
    ]);

})(angular);

(function() {
  'use strict';

  /**
   * @name $streamService
   * @description Angular service wrapper for the stream-js library
   * @author Erik August Johnson (erik@getstream.io)
   */
  angular.module('streamAngular.services').factory('$streamService', $streamService);

  $streamService.$inject = [
    '$log',
    '$window'
  ];

  function $streamService($log, $window) {

    var service = {
      connect: connect
    };
    return service;

    ///////////////

    /**
     * @name connect
     * @description Wrapper method for low level library connect method
     * @param apiKey
     * @returns {*}
     */
    function connect(apiKey, appID) {
      if (typeof $window.stream !== 'undefined') {
        return $window.stream.connect(apiKey, null, appID);
      } else {
        $log.error(
          'Error: stream-js library not found. Hint: Use \'bower install getstream\' to install.'
        );
      }
    }
  }
}());
