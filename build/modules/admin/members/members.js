angular.module('app.admin')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin.members', {
                url: '/members?search&status&page',
                templateUrl: 'modules/admin/members/index.html',
                skeleton: true,
                controller: 'AdminMembersController',
                access: 'admin',
                resolve: {
                    members: ['$FullAPI', '$rootScope', '$stateParams', function($FullAPI, $rootScope, $stateParams) {
                        if ($stateParams.search) {
                            return $FullAPI.all('users/search').getList({
                                query: $stateParams.search,
                                page: $stateParams.page || 1
                            });
                        } else {
                            return $FullAPI.all('users').getList($stateParams);
                        }
                    }]
                }
            })
            .state('admin.member', {
                url: '/member/:id',
                templateUrl: 'modules/admin/members/one.html',
                skeleton: true,
                access: 'admin',
                controller: 'AdminMemberController',
                resolve: {
                    member: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.one('users', $stateParams.id).get();
                    }],
                    plans: ['MemberSubscription', '$API', 'member', function(MemberSubscription, $API, member) {
                        if (MemberSubscription.check(member).has_office) {
                            return $API.all('group_plans').getList();
                        } else if (member.subscription) {
                            return $API.all('plans').getList();
                        } else {
                            return []
                        }
                    }]
                }
            })
    }
])

.service('MemberSubscription', [function() {
    return {
        check: function(member, plans) {
            function pad(num, size) {
                var s = num + "";
                while (s.length < size) s = "0" + s;
                return s;
            }

            member.member_id = pad(member.id, 7);
            member.has_team = member.team_id != null;
            member.is_team_admin = member.team_admin;

            if (member.group_plan_subscription && member.subscription) {
                var group_plan_started = new Date(member.group_plan_subscription.created_at);
                var regular_plan_started = new Date(member.subscription.created_at);
                var group_plan_started_later = moment(group_plan_started).isAfter(regular_plan_started);

                if (group_plan_started_later) {
                    member.subscription = member.group_plan_subscription;
                    member.has_office = true;
                } else {
                    member.group_plan_subscription = null;
                }
            }

            if (member.group_plan_subscription && !member.subscription) {
                member.subscription = member.group_plan_subscription;
                member.has_office = true;
            }

            member.active_subscription = member.subscription && member.subscription.enabled && !member.is_owing;

            return member;
        }
    }
}])

.controller('AdminMembersController', ['$scope', '$rootScope', '$state', '$API', '$FullAPI', 'LocalService', 'MemberSubscription', 'Notification', 'ngDialog', 'members',
    function($scope, $rootScope, $state, $API, $FullAPI, LocalService, MemberSubscription, Notification, ngDialog, members) {

        function setMembers(response) {
            var _members = response || members;
            $scope.members = _.map(_members.data, function(member) {
                return MemberSubscription.check(member);
            });

            $scope.meta = {
                total: _members.headers("total"),
                perPage: 25,
                page: $state.params.page,
                pageCount: Math.round(_members.headers("total") / 25)
            }
        }

        setMembers(members);

        $scope.find = function(query) {
            $scope.searching = true;
            $FullAPI.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.searching = false;
                setMembers(response);
            }, function() {
                $scope.searching = false;
            });
        }

        $scope.message = function(member) {
            LocalService.set('workstation-new-conversation', JSON.stringify(member));
            $state.go('admin.messages', { new: member.email });
        }

        $scope.reload = function(params) {
            $state.go($state.current, params, {
                reload: true,
                inherit: false,
                notify: true
            })
        }

        $scope.goToUser = function(user_id) {
            var id = parseInt(user_id, 10);
            $state.go("admin.member", { id: id });
        }
    }
])

.controller('AdminMemberController', ['$scope', '$rootScope', '$state', '$window', '$API', '$FullAPI', 'LocalService', 'MemberSubscription', 'Notification', 'ngDialog', 'member', 'plans',
    function($scope, $rootScope, $state, $window, $API, $FullAPI, LocalService, MemberSubscription, Notification, ngDialog, member, plans) {
        $scope.member = MemberSubscription.check(member.plain());

        $scope.subscription = $scope.member.subscription || {};
        if ($scope.subscription.days) $scope.subscription.days = JSON.parse($scope.subscription.days);
        if ($scope.member.subscription) {
            var plan_id = $scope.member.has_office ? "group_plan_id" : "plan_id";
            $scope.member.plan = _.findWhere(plans, { id: $scope.subscription[plan_id] });
        }

        $scope.message = function() {
            LocalService.set('workstation-new-conversation', JSON.stringify($scope.member));
            $state.go('admin.messages', { new: $scope.member.email });
        }

        $scope.load = function(resource, page, callback) {
            var page = page || 1;

            var url_promise;
            if (resource === "team") {
                url_promise = $FullAPI.one('teams/' + member.team_id, 'members').get();
            } else {
                url_promise = $FullAPI.one('users', member.id).all(resource).getList();
            }

            url_promise.then(function(response) {
                var meta = {
                    total: response.headers("total"),
                    perPage: 25,
                    page: page,
                    pageCount: Math.round(response.headers("total") / 25)
                }
                $scope[resource] = response.data;
                $scope[resource].meta = meta;

                if (callback) callback(resource, page, response);
            })
        }

        $scope.checkAddons = function(resource, page, response) {
            if (page === 1) {
                var addons = _.filter($scope.purchases, function(purchase) {
                    return purchase.active && purchase.add_on && !purchase.add_on.system;
                });
                $scope.addons = _.map(addons, function(purchase) {
                    return purchase.add_on.name
                });
            }
        }

        $scope.addTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddTokensController"
            });
        }

        $scope.addPrinting = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-printing.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddPrintingController"
            });
        }

        $scope.addAddon = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/admin/members/modals/add-addon.html",
                resolve: {
                    addons: ['$API', function($API) {
                        return $API.all('add_ons').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AddAddonController"
            });
        }

        $scope.cancelSubscription = function() {
            ngDialog.openConfirm({
                template: "CancelSubscription"
            }).then(function(reason) {
                var SubscriptionCancel;

                if ($scope.member.has_office) {
                    SubscriptionCancel = $API.all('group_plan_subscriptions/cancel').post({ reason: reason, user_id: $scope.member.id });
                } else {
                    SubscriptionCancel = $API.one('users', $scope.member.id).all('cancel').post({ reason: reason });
                }

                SubscriptionCancel.then(function() {
                    Notification.success('Subscription cancelled', "User's current subscription has been cancelled");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('cancelledSubscription', false);
                    Notification.error("Could not cancel subscription", response);
                });
            });
        }

        $scope.subscribe = function() {
            ngDialog.open({
                templateUrl: "/modules/admin/members/modals/subscribe.html",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AdminSubscriptionController",
                width: 600
            });
        }

        $scope.subscribeOffice = function() {
            ngDialog.open({
                templateUrl: "/modules/admin/members/modals/subscribe-office.html",
                resolve: {
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: "AdminOfficeSubscriptionController",
                width: 600
            });
        }

        $scope.suspend = function(reason) {
            $FullAPI.one('users', member.id).all("suspend").post({
                reason: reason
            }).then(function() {
                Notification.success('User suspended', "Access to the application has been revoked");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userSuspended', false);
                Notification.error("Could not suspend user", response);
            });
        }

        $scope.unsuspend = function(reason) {
            $FullAPI.one('users', member.id).all("unsuspend").post({
                reason: reason
            }).then(function() {
                Notification.success('User unsuspended', "Access to the application has been restored");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userUnsuspended', false);
                Notification.error("Could not unsuspend user", response);
            });
        }

        $scope.changeDays = function() {
            ngDialog.openConfirm({
                templateUrl: "ChangeDays",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return $scope.member
                    }]
                },
                controller: 'AdminSubscriptionController'
            });
        }

        $scope.formatEditTrail = function(trail) {
            var reason = trail.reason.substr(0, trail.reason.indexOf('['));
            var change = JSON.parse(trail.reason.substr(trail.reason.indexOf('[')));
            trail.reason = reason;
            trail.change = {
                from: change[0].join(", "),
                to: change[1].join(", ")
            }
            return trail;
        }

        $scope.delete = function(callback) {
            $FullAPI.one('users', member.id).remove().then(function() {
                Notification.success('User deleted', "User has been removed from Workstation");
                callback();
                $state.go('admin.members');
            }, function(response) {
                $rootScope.$broadcast('userDeleted', false);
                Notification.error("Could not delete user", response);
            });
        }
    }
])

.controller('AddTokensController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'prices', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, prices, member) {
        var defaults = $scope.ngDialogData || {};
        var system_token = _.findWhere(prices.plain(), { slug: "token" }) || {};

        $scope.token_cost = system_token.price;
        $scope.tokens = defaults.amount;

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            $API.one('users', member.id).all('purchase').post({
                add_on_id: system_token.id,
                quantity: $scope.tokens,
                reason: reason
            }).then(function() {
                Notification.success('Tokens Added', "Member token count has been updated");
                $state.reload().then(function() { $scope.closeThisDialog() });
            }, function(response) {
                $rootScope.$broadcast('tokensAdded', false);
                Notification.error("Could not add tokens", response);
            });
        }
    }
])

.controller('AddPrintingController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'prices', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, prices, member) {
        var defaults = $scope.ngDialogData || {};

        var system_bw_printing = _.findWhere(prices.plain(), { slug: "b-w-printing" }) || {};
        var system_rgb_printing = _.findWhere(prices.plain(), { slug: "colour-printing" }) || {};

        $scope.rgb_printing_cost = system_rgb_printing.price;
        $scope.bw_printing_cost = system_bw_printing.price;

        $scope.bw_printing = defaults.bw_printing;
        $scope.rgb_printing = defaults.rgb_printing;
        $scope.total = 0;

        $scope.computeTotal = function() {
            $scope.total = (($scope.bw_printing || 0) * $scope.bw_printing_cost) + (($scope.rgb_printing || 0) * $scope.rgb_printing_cost);
        }

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            var tasks = 0;

            if ($scope.bw_printing) {
                tasks++;
                $API.one('users', member.id).all('purchase').post({
                    add_on_id: system_bw_printing.id,
                    quantity: $scope.bw_printing,
                    reason: reason
                }).then(function() {
                    onComplete();
                    Notification.success('B/W Printing Added', "Member printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingAdded', false);
                    Notification.error("Could not purchase b/w printing", response);
                });
            }

            if ($scope.rgb_printing) {
                tasks++;
                $API.one('users', member.id).all('purchase').post({
                    add_on_id: system_rgb_printing.id,
                    quantity: $scope.rgb_printing,
                    reason: reason
                }).then(function() {
                    onComplete();
                    Notification.success('Color Printing Added', "Member printing count has been updated");
                }, function(response) {
                    $rootScope.$broadcast('printingAdded', false);
                    Notification.error("Could not purchase color printing", response);
                });
            }

            var onComplete = function() {
                tasks--;
                if (tasks == 0) {
                    $rootScope.$broadcast('printingAdded', true);
                    $state.reload().then(function() { $scope.closeThisDialog() });
                }
            }
        }
    }
])

.controller('AddAddonController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'addons', 'member',
    function($scope, $rootScope, $state, $window, $API, Notification, addons, member) {
        $scope.addons = addons;

        $scope.add = function() {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            $API.one('users', member.id).all('purchase').post({
                add_on_id: $scope.addon_id,
                quantity: $scope.tokens,
                reason: reason
            }).then(function() {
                Notification.success('Add-on Added', "Addon has been added to member subscription");
                $state.reload().then(function() { $scope.closeThisDialog() });
            }, function(response) {
                $rootScope.$broadcast('addonAdded', false);
                Notification.error("Could not add add-on", response);
            });
        }
    }
])

.controller('AdminSubscriptionController', ['$scope', '$rootScope', '$state', '$API', '$window', 'LocalService', 'Notification', 'ngDialog', 'plans', 'member',
    function($scope, $rootScope, $state, $API, $window, LocalService, Notification, ngDialog, plans, member) {
        $scope.member = member;

        if (member.subscription) {
            $scope.current_plan = _.findWhere(plans, { id: member.subscription.plan_id });

            if ($scope.current_plan) {
                $scope.current_subscription = member.subscription;
                $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
            }

            $scope.renew = function() {
                $scope.complete($scope.current_subscription);
            }
        }

        $scope.plans = _.filter(plans, function(plan) {
            return plan.limit === null || plan.limit > 0
        });

        $scope.plans = _.map($scope.plans, function(plan) {
            plan.intervals = [];
            if (plan.daily_price) plan.intervals.push({ name: "Daily", slug: "daily", price: plan.daily_price });
            if (plan.monthly_price) plan.intervals.push({ name: "Monthly", slug: "monthly", price: plan.monthly_price });
            if (plan.yearly_price) plan.intervals.push({ name: "Yearly", slug: "yearly", price: plan.yearly_price });

            return plan;
        });

        $scope.subscribeTo = function(plan) {
            $scope.subscription = {
                plan_id: plan.id,
                interval: plan.intervals[0].slug,
                days: []
            }

            $scope.active_plan = plan;
            if (plan.days < 5) {
                $scope.days = {
                    monday: false,
                    tuesday: false,
                    wednesday: false,
                    thursday: false,
                    friday: false
                }
            } else {
                $scope.subscription.days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
            }
        }

        $scope.addDays = function() {
            $scope.subscription.days = [];
            _.each($scope.days, function(value, key) {
                if (value) $scope.subscription.days.push(key);
            })
        }

        $scope.toggleDay = function(day) {
            if (_.contains($scope.subscription.days, day)) {
                $scope.subscription.days = _.without($scope.subscription.days, day);
            } else {
                $scope.subscription.days.push(day);
            }
        }

        $scope.complete = function(subscription) {
            subscription.reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;
            var payload = _.pick(subscription, ['interval', 'plan_id', 'days', 'reason']);

            $API.one('users', member.id).all('subscribe').post(payload).then(function() {
                $scope.closeThisDialog();
                Notification.success('Whoop!', "User subscription was successful");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('subscriptionDone', false);
                Notification.error("Could not subscribe to this plan", response);
            });
        }

        if (member.subscription) {
            $scope.days = { monday: false, tuesday: false, wednesday: false, thursday: false, friday: false };
            $scope.subscription = _.clone(member.subscription);
            _.each($scope.subscription.days, function(day) { $scope.days[day] = true });

            $scope.update = function() {
                $API.one('subscriptions', $scope.subscription.id)
                    .customPUT({
                        reason: $scope.reason,
                        days: $scope.subscription.days
                    })
                    .then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "User subscription has been updated");
                        $state.reload();
                    }, function(response) {
                        $rootScope.$broadcast('updatedSubscription', false);
                        Notification.error("Could not update user subscription", response);
                    });
            }
        }
    }
])

.controller('AdminOfficeSubscriptionController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'offices', 'member',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, offices, member) {
        $scope.member = member;
        $scope.offices = _.filter(offices, function(office) {
            return office.limit === null || office.limit > 0
        });

        $scope.team_sizes = {
            max: _.max($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces;
            }),
            min: _.min($scope.offices, function(_office) {
                return _office.seats_no;
            })
        }

        $scope.checkAvailability = function() {
            $scope.office = null;
            var suitable_offices = _.filter($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces >= $scope.office_seats;
            });

            if (suitable_offices.length) {
                $scope.office = _.min(suitable_offices, function(_office) {
                    return _office.seats_no;
                });

                var intervals = [];
                if ($scope.office.monthly_price) intervals.push({ name: "Monthly", slug: "monthly", price: $scope.office.monthly_price });
                if ($scope.office.yearly_price) intervals.push({ name: "Yearly", slug: "yearly", price: $scope.office.yearly_price });
                $scope.office.intervals = intervals;

                $scope.office_interval = $scope.office.intervals[0].slug;
                $scope.calculateCost($scope.office_interval);
            }
        }

        $scope.calculateCost = function(interval) {
            var interval = _.find($scope.office.intervals, { slug: interval });
            $scope.office.seat_cost = interval.price;
            $scope.office.total_cost = $scope.office_seats * interval.price;
            if ($scope.office.setup_fee) {
                $scope.office.total_cost += ($scope.office.setup_fee * $scope.office_seats);
            }
        }

        $scope.subscribe = function(office) {
            var reason = $scope.reason === "cash" ? "Cash Payment" : $scope.other_reason;

            $API.all('group_plan_subscriptions')
                .post({
                    group_plan_id: office.id,
                    interval: $scope.office_interval,
                    no_of_seats: $scope.office_seats,
                    user_id: $scope.member.id,
                    reason: reason
                })
                .then(function() {
                    $scope.closeThisDialog();
                    Notification.success('Whoop!', "Office subscription was successful");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe for this office", response);
                });
        }
    }
])
