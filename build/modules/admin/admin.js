angular.module('app.admin', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('admin', {
                url: '/admin',
                templateUrl: 'components/layouts/admin.html',
                redirectTo: 'admin.home',
                access: 'admin',
                resolve: {
                    auth: function($auth, $state, Notification) {
                        return $auth.validateUser()
                            .catch(function(response) {
                                Notification.error(response.reason, response);
                                $state.go('access.login');
                            });
                    },
                    locations: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.all('locations').getList().then(function(response) {
                            $rootScope.locations = response.plain();
                        });
                    }]
                }
            })
            .state('admin.messages', {
                url: '/messages?new',
                title: 'Messages',
                templateUrl: 'modules/common/messages/index.html',
                controller: 'MessagesController',
                fullHeight: true,
                access: 'admin',
                resolve: {
                    conversations: ['$API', function($API){
                        return $API.all('messaging/conversations').getList();
                    }],
                    unread: ['$API', function($API){
                        return $API.all('messaging/unread').getList();
                    }]
                }
            })
            .state('admin.messages.one', {
                url: '/:id',
                title: 'Messages',
                templateUrl: 'modules/common/messages/conversation.html',
                controller: 'ConversationController',
                access: 'admin',
                fullHeight: true,
                resolve: {
                    receipts: ['$API', '$stateParams', function($API, $stateParams){
                        return $API.all('messaging/receipts/' + $stateParams.id).getList();
                    }]
                }
            })
            .state('admin.bookings', {
                url: '/bookings',
                title: 'Bookings',
                templateUrl: 'modules/common/bookings/index.html',
                controller: 'BookingsController',
                access: 'admin',
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/ui-calendar/';
                        var files = [];
                        files.push(folder + 'jquery.min.js');
                        files.push(folder + 'fullcalendar.min.js');
                        files.push(folder + 'fullcalendar.min.css');
                        files.push(folder + 'fullcalendar.theme.css');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList();
                    }]
                }
            })
            .state('admin.events', {
                url: '/events',
                title: 'Events',
                templateUrl: 'modules/common/events/index.html',
                controller: 'EventsController',
                access: 'admin',
                resolve: {
                    events: ['$API', function($API) {
                        return $API.all('events').getList();
                    }]
                }
            })
    }
])
