angular.module('app.user', ['ui.router'])

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user', {
                templateUrl: 'components/layouts/user.html',
                redirectTo: 'user.home',
                resolve: {
                    auth: function($auth, $state, $rootScope, $location, Notification) {
                        return $auth.validateUser()
                            .catch(function(response) {
                                Notification.error(response.reason, response);
                                $rootScope.afterLogin = window.location.hash;
                                $state.go("access.login", { next: true });
                            });
                    },
                    locations: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.all('locations').getList().then(function(response) {
                            $rootScope.locations = response.plain();
                        });
                    }]
                }
            })
            .state('user.messages', {
                url: '/messages?new',
                title: 'Messages',
                templateUrl: 'modules/common/messages/index.html',
                controller: 'MessagesController',
                fullHeight: true,
                resolve: {
                    subscription: ['$state', 'Subscription', function($state, Subscription) {
                        return Subscription.verify('can_message').catch(function() {
                            $state.go('user.home');
                        });
                    }],
                    conversations: ['$API', function($API) {
                        return $API.all('messaging/conversations').getList();
                    }],
                    unread: ['$API', function($API) {
                        return $API.all('messaging/unread').getList();
                    }]
                }
            })
            .state('user.messages.one', {
                url: '/:id',
                title: 'Messages',
                templateUrl: 'modules/common/messages/conversation.html',
                controller: 'ConversationController',
                fullHeight: true,
                resolve: {
                    receipts: ['$API', '$stateParams', function($API, $stateParams) {
                        return $API.all('messaging/receipts/' + $stateParams.id).getList();
                    }]
                }
            })
            .state('user.bookings', {
                url: '/bookings',
                title: 'Bookings',
                templateUrl: 'modules/common/bookings/index.html',
                controller: 'BookingsController',
                resolve: {
                    subscription: ['$state', 'Subscription', function($state, Subscription) {
                        return Subscription.verify('can_book_rooms').catch(function() {
                            $state.go('user.home');
                        });
                    }],
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/ui-calendar/';
                        var files = [];
                        files.push(folder + 'jquery.min.js');
                        files.push(folder + 'fullcalendar.min.js');
                        files.push(folder + 'fullcalendar.min.css');
                        files.push(folder + 'fullcalendar.theme.css');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    bookings: ['$API', function($API) {
                        return $API.all('bookings').getList();
                    }]
                }
            })
            .state('user.events', {
                url: '/events',
                title: 'Events',
                templateUrl: 'modules/common/events/index.html',
                controller: 'EventsController',
                resolve: {
                    events: ['$API', function($API) {
                        return $API.all('events').getList();
                    }]
                }
            })
    }
]);
