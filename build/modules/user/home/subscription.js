angular.module('app.user')

.controller('UserHomeSubscriptionController', ['$scope', '$state', '$rootScope', 'Session', '$API', 'plans', 'offices', 'subscription_logs', 'Notification', 'ngDialog',
    function($scope, $state, $rootScope, Session, $API, plans, offices, subscription_logs, Notification, ngDialog) {
        if ($rootScope.User._subscription) {
            $scope.current_plan = _.findWhere(plans, { id: $rootScope.User._subscription.plan_id });
            if ($scope.current_plan) {
                $scope.current_subscription = $rootScope.User._subscription;
                $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
                $scope.renew = function() {
                    $scope.pay($scope.current_subscription);
                }
            }
        }

        $scope.offices = _.filter(offices, function(office) {
            return office.limit === null || office.limit > 0
        });

        $scope.plans = _.filter(plans, function(plan) {
            return plan.limit === null || plan.limit > 0
        });

        $scope.subscription_logs = subscription_logs;

        $scope.pay = function(subscription) {
            $API.all('subscriptions').post(subscription)
                .then(Session.update)
                .then(function() {
                    Notification.success('Whoop!', "Your subscription was successful");
                    $state.reload();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe to this plan", response);
                });
        }
    }
])

.controller('UserSubscriptionHelpers', ['$scope', '$rootScope', '$state', '$filter', function($scope, $rootScope, $state, $filter) {
    $scope.plans = _.map($scope.plans, function(plan) {
        plan.intervals = [];

        function formatPrice(price) {
            return $filter('currency')(price, 'NGN ', 2);
        }

        if (plan.daily_price) plan.intervals.push({ name: "Daily (" + formatPrice(plan.daily_price) + ")", slug: "daily", price: plan.daily_price });
        if (plan.monthly_price) plan.intervals.push({ name: "Monthly (" + formatPrice(plan.monthly_price) + ")", slug: "monthly", price: plan.monthly_price });
        if (plan.yearly_price) plan.intervals.push({ name: "Yearly (" + formatPrice(plan.yearly_price) + ")", slug: "yearly", price: plan.yearly_price });

        var ints = ['daily_price', 'monthly_price', 'yearly_price', 'setup_fee'];
        _.each(ints, function(int) {
            plan[int] = parseInt(plan[int]);
        });

        return plan;
    });

    $scope.toPaySetupFee = function(user_id, plan) {
        if (!plan.setup_fee) return false;
        return _.findWhere($scope.subscription_logs, { user_id: user_id, plan_id: plan.id }) == null;
    }

    $scope.subscribeTo = function(plan, team_member) {
        $scope.subscription = {
            plan_id: plan.id,
            interval: plan.intervals[0].slug,
            days: []
        }

        if (team_member) {
            $scope.subscription.user_id = team_member.id;
        }

        $scope.active_plan = plan;
        if (plan.days < 5) {
            $scope.days = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false
            }
        } else {
            $scope.subscription.days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
        }
    }

    $scope.addDays = function() {
        $scope.subscription.days = [];
        _.each($scope.days, function(value, key) {
            if (value) $scope.subscription.days.push(key);
        })
    }

    $scope.toggleDay = function(day) {
        if (_.contains($scope.subscription.days, day)) {
            $scope.subscription.days = _.without($scope.subscription.days, day);
        } else {
            $scope.subscription.days.push(day);
        }
    }
}])

.controller('UserOfficeSubscriptionController', ['$scope', '$rootScope', '$state', '$API', 'Session', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, Session, LocalService, Notification, ngDialog) {
        $scope.team_sizes = {
            max: _.max($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces;
            }),
            min: _.min($scope.offices, function(_office) {
                return _office.seats_no;
            })
        }


        $scope.checkAvailability = function() {
            $scope.office = null;
            var suitable_offices = _.filter($scope.offices, function(_office) {
                return _office.seats_no + _office.additional_spaces >= $scope.office_seats;
            });

            if (suitable_offices.length) {
                $scope.office = _.min(suitable_offices, function(_office) {
                    return _office.seats_no;
                });

                var intervals = [];
                if ($scope.office.monthly_price) intervals.push({ name: "Monthly", slug: "monthly", price: $scope.office.monthly_price });
                if ($scope.office.yearly_price) intervals.push({ name: "Yearly", slug: "yearly", price: $scope.office.yearly_price });
                $scope.office.intervals = intervals;

                $scope.office_interval = $scope.office.intervals[0].slug;
                $scope.calculateCost($scope.office_interval);
            }
        }

        $scope.calculateCost = function(interval) {
            var interval = _.find($scope.office.intervals, { slug: interval });
            $scope.office.seat_cost = interval.price;
            $scope.office.total_cost = $scope.office_seats * interval.price;
            if ($scope.office.setup_fee) {
                $scope.office.total_cost += ($scope.office.setup_fee * $scope.office_seats);
            }
        }

        $scope.subscribe = function(office) {
            $API.all('group_plan_subscriptions')
                .post({
                    group_plan_id: office.id,
                    interval: $scope.office_interval,
                    no_of_seats: $scope.office_seats
                })
                .then(Session.update)
                .then(function() {
                    Notification.success('Whoop!', "Your subscription was successful");
                    $state.reload();
                }).catch(function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Could not subscribe for this office", response);
                });
        }

        $scope.book = function(office) {
            $API.all('users/request_plan')
                .post({
                    group_plan_id: office.id
                })
                .then(function() {
                    $scope.$broadcast('adminNotified', true);
                    Notification.success('Office booked', 'An admin has been notified and will be in touch.');
                }).catch(function(response) {
                    $rootScope.$broadcast('adminNotified', false);
                    Notification.error("Could not book this office", response);
                });
        }
    }
]);
