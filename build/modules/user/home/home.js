angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.home', {
                url: '/?feed',
                templateUrl: 'modules/user/home/index.html',
                controller: 'UserHomeController',
                skeleton: true,
                resolve: {
                    files: ['$ocLazyLoad', function($ocLazyLoad) {
                        var folder = '/assets/vendor/lazyload/getstream/';
                        var files = [];

                        files.push(folder + 'getstream.js');
                        files.push(folder + 'stream-angular.js');

                        return $ocLazyLoad.load({
                            serie: true,
                            files: files
                        });
                    }],
                    categories: ['$API', function($API) {
                        return $API.all('feed_categories').getList();
                    }],
                    subscription: ['$rootScope', '$state', '$auth', '$window', 'Subscription', function($rootScope, $state, $auth, $window, Subscription) {
                        Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
            .state('user.home.subscribe', {
                url: 'subscribe',
                templateUrl: 'modules/user/home/subscription.html',
                controller: 'UserHomeSubscriptionController',
                skeleton: true,
                resolve: {
                    verified: ['$rootScope', 'ngDialog', 'Notification', '$state', '$q', 'Subscription', function($rootScope, ngDialog, Notification, $state, $q, Subscription) {
                        var deferred = $q.defer();

                        Subscription.verify().then(function() {
                            deferred.reject();
                            $state.go('user.home');
                        }, function() {
                            if ($rootScope.User.verified) {
                                deferred.resolve();
                            } else {
                                ngDialog.open({
                                    templateUrl: "/modules/user/home/modals/verify.html",
                                    controller: ['$API', 'Notification', '$scope', function($API, Notification, $scope) {
                                        $scope.resend = function() {
                                            $API.all('users/resend_confirmation').post()
                                                .then(function() {
                                                    Notification.success('Sent', "Your verification email was resent");
                                                    $scope.closeThisDialog();
                                                })
                                                .catch(function(response) {
                                                    $rootScope.$broadcast('verificationResent', false);
                                                    Notification.error("Could not resend verification email", response);
                                                });
                                        }
                                    }]
                                });
                            }
                        });

                        return deferred.promise;
                    }],
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    offices: ['$API', function($API) {
                        return $API.all('group_plans').getList();
                    }],
                    subscription_logs: ['$API', function($API) {
                        return $API.all('subscription_logs').getList();
                    }]
                }
            })
    }
])

.controller('UserHomeController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'categories',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, categories) {
        $scope.categories = categories;
        $scope.activeCategory = _.find(categories, function(cat) {
            return cat.id == $state.params.feed
        });

        $scope.switchTo = function(category) {
            $state.go($state.current.name, {
                feed: category.id
            }, { reload: true });
        }

        var hasHomeLocation = $rootScope.User.home_location || $rootScope.User.home_location_id;
        if (!hasHomeLocation) {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/home/onboarding.html",
                controller: "UserOnboardingController",
                className: 'ngdialog-fullscreen',
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        };

        $scope.contactAdmin = function() {
            ngDialog.open({
                templateUrl: "/modules/user/home/modals/admins.html",
                resolve: {
                    admins: ['$API', '$rootScope', function($API, $rootScope) {
                        return $API.one('users', $rootScope.User.id).all('admins').getList();
                    }]
                },
                controller: ['$state', '$scope', 'LocalService', 'admins', function($state, $scope, LocalService, admins) {
                    admins = _.map(admins, function(admin) {
                        admin.is_admin = true;
                        return admin
                    });

                    $scope.admins = _.reject(admins, 'superadmin');

                    $scope.sendMessage = function(admin) {
                        LocalService.set('workstation-new-conversation', JSON.stringify(admin));
                        $state.go('user.messages', { new: admin.email });
                        $scope.closeThisDialog();
                    }
                }]
            });
        }

        $scope.reportBug = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/home/modals/report-bug.html",
                controller: ['$state', '$scope', 'LocalService', 'FileHelper', function($state, $scope, LocalService, FileHelper) {
                    $scope.report = {};
                    $scope.$watch('attachment', function(newFile, oldFile) {
                        if (!newFile || newFile == oldFile) return;

                        $scope.uploading = true;

                        var fileSize = newFile.size / 1024 / 1024;
                        if (fileSize > 5) {
                            Notification.error("File limit exceeded", "Please upload a file below 5MB");
                            $scope.attachment = null;
                            $scope.uploading = false;
                            return;
                        };

                        FileHelper.upload(newFile).success(function(response) {
                            $scope.report.attachment = response.url;
                            $scope.uploading = false;
                        }, function(response) {
                            $scope.uploading = false;
                            Notification.error("Could not upload file", response);
                        });
                    });
                }]
            }).then(function(report) {
                $API.all('users/report_bug').post(report).then(function() {
                    Notification.success('Thanks!', "We'll review this bug report as soon as possible.");
                });
            });
        }
    }
])

.controller('TodayController', ['$scope', '$rootScope', '$state', '$API', '$filter', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, $filter, LocalService, Notification, ngDialog) {

        var isToday = function(time) {
            var today = new Date();
            var eventDay = new Date(time);
            return today.toDateString() === eventDay.toDateString();
        }

        $API.all('bookings').getList().then(function(bookings) {
            bookings = _.filter(bookings, 'booker');
            _.each(bookings, function(booking) {
                booking.self = booking.booker.uid == $rootScope.User.uid && booking.booker.id == $rootScope.User.id;

                if (booking.self && isToday(booking.start_time)) {
                    $scope.events = $scope.events || [];
                    $scope.events.push({
                        is_booking: true,
                        title: booking.title,
                        time: $filter('date')(booking.start_time, 'h:mma') + "-" + $filter('date')(booking.end_time, 'h:mma'),
                        // location: booking.location.description
                    });
                }
            })
        });

        $API.all('events').getList().then(function(events) {
            _.each(events, function(event) {
                if (isToday(event.time)) {
                    $scope.events = $scope.events || [];
                    $scope.events.push({
                        title: event.name,
                        time: $filter('date')(event.time, 'h:mma'),
                        location: event.location.description
                    });
                }
            });
            $scope.events = $scope.events || [];
        });
    }
]);
