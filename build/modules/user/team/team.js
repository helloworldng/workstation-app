angular.module('app.user')

.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('user.team', {
                url: '/team?search&network_only&page',
                templateUrl: 'modules/user/team/index.html',
                controller: 'UserTeamController',
                resolve: {
                    subscription: ['$rootScope', '$state', '$auth', 'Subscription', function($rootScope, $state, $auth, Subscription) {
                        return Subscription.verify().then(null, function() {
                            return true
                        });
                    }]
                }
            })
    }
])

.controller('UserTeamController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        if (!$rootScope.User.has_team || !$rootScope.User.is_team_admin) {
            $state.go('user.account.team');
            return;
        };

        $API.one('teams', $rootScope.User.team_id).one('members').get().then(function(team) {
            var office = _.filter(team.members, function(registration) {
                return registration.role === "has_seat"
            });
            var organization = _.filter(team.members, function(registration) {
                return registration.role === "is_member"
            });
            var admin = _.findWhere(team.members, { role: "is_admin" });

            if ($rootScope.User.has_office) {
                office.unshift(admin);
            } else {
                organization.unshift(admin);
            }

            $scope.office = office;
            $scope.organization = organization;
            $scope.team = team;
        });

        $API.one('teams', $rootScope.User.team_id).all('invitations').getList().then(function(invitations) {
            $scope.invitations = invitations;
        });

        $scope.format = function(member) {
            member.team_admin = member.user.id == $rootScope.User.id;

            if (!member.user.subscription || !member.user.subscription.enabled) {
                member.subscription = "No current subscription";
            } else if (member.user.group_plan_subscription) {
                member.subscription = "Shared Office";
            } else {
                member.subscription = member.user.subscription_plan_name + " " + member.user.subscription.interval;
            }

            if (member.team_admin) {
                member.privilege = "Team admin";
            } else {
                member.privilege = member.use_team_card ? "Can " : "Cannot ";
                member.privilege += "use team card";
            }

            return member;
        }

        $scope.available_seats = function() {
            var seat_invitations = _.filter($scope.invitations, function(invite) {
                return invite.role == "has_seat";
            });
            var available_seats = $rootScope.User.subscription.seats_left - seat_invitations.length;
            return new Array(available_seats);
        }

        $scope.cancelInvite = function(id) {
            $API.one('team_invitations', id).remove().then(function() {
                Notification.success('Invitation deleted', "User will not longer be part of team");
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('deletedInvitation', false);
                Notification.error("Could not delete invitation", response);
            });
        }

        $scope.manage = function(member) {
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/member.html",
                resolve: {
                    member: [function() {
                        return member
                    }]
                },
                controller: "TeamMemberController",
                width: 600
            });
        }

        $scope.changeName = function(id, name) {
            $API.one('teams', id).customPUT({
                name: name
            }).then(function() {
                Notification.success('Update Complete', "Team name has been updated");
                $state.reload();
            }, function(response) {
                Notification.error("Could not rename team", response);
            });
        }

        $scope.cancelOffice = function() {
            ngDialog.openConfirm({
                template: "CancelOffice",
                controller: ['$scope', '$API', 'Session', 'Notification', function($scope, $API, Session, Notification) {
                    $scope.cancel = function() {
                        $API.all('users/request_cancellation').post().then(function() {
                            Notification.success('Office Cancellation Requested', "A Workstation admin will be in touch");
                            $scope.closeThisDialog();
                        }, function(response) {
                            Notification.error("Could not notify admin", response);
                        });
                    }
                }]
            });
        }
    }
])

.controller('TeamMemberController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog', 'member',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog, member) {
        $scope.member = member;

        $scope.subscribe = function() {
            $scope.closeThisDialog();
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/subscribe.html",
                resolve: {
                    plans: ['$API', function($API) {
                        return $API.all('plans').getList();
                    }],
                    member: [function() {
                        return member.user
                    }],
                    subscription_logs: ['$API', function($API) {
                        return $API.one('users', member.user.id).all('subscription_logs').getList();
                    }]
                },
                controller: "TeamSubscriptionController",
                width: 600
            });
        }

        $scope.removeFromTeam = function(user) {
            $scope.closeThisDialog();
            ngDialog.open({
                templateUrl: "/modules/user/team/modals/remove-from-team.html",
                controller: ['$scope', '$state', '$rootScope', 'Session', '$API', 'Notification', function($scope, $state, $rootScope, Session, $API, Notification) {
                    $scope.user = user;
                    $scope.remove = function() {
                        $API.one('teams', $rootScope.User.team_id).one('remove_member', user.id).remove()
                            .then(Session.update)
                            .then(function() {
                                Notification.success('Successful', "User successfully removed from team");
                                $state.reload();
                                $scope.closeThisDialog();
                            }).catch(function(response) {
                                $rootScope.$broadcast('removedFromTeam', false);
                                Notification.error("Could not remove from team", response);
                            });
                    }
                }]
            });
        }

        $scope.updateRole = function(member) {
            $API.one('teams', $rootScope.User.team_id).all('update_member').customPUT({
                member_id: member.user.id,
                use_team_card: member.use_team_card
            }).then(function() {
                Notification.success('Update successful', "User roles have been updated");
                $state.reload();
            });
        }
    }
])

.controller('TeamInvitationController', ['$scope', '$rootScope', '$state', '$API', 'LocalService', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, $API, LocalService, Notification, ngDialog) {
        $scope.use_team_card = false;
        $scope.invite = function(options) {
            options = options || {};
            $scope.not_a_member = false;

            var invitation_url = options.has_seat ? 'seat_invitations' : 'invitations';
            $API.one('teams', $rootScope.User.team_id).all(invitation_url).post({
                invite_email: $scope.invite_email,
                use_team_card: $scope.use_team_card
            }).then(function() {
                Notification.success('User Invited', "An invitation has been sent to " + $scope.invite_email);
                $scope.closeThisDialog();
                $state.reload();
            }, function(response) {
                $rootScope.$broadcast('userInvited', false);
                if (response.data && response.data.errors == "User is not a Workstation member") {
                    $scope.not_a_member = true;
                } else {
                    Notification.error("Could not invite user", response);
                }
            });
        }

        $scope.sendInvite = function() {
            $API.all('users/invite').post({
                invite_email: $scope.invite_email
            }).then(function() {
                Notification.success('Invite sent', "An invitation has been sent to " + $scope.invite_email);
                $scope.closeThisDialog();
                $state.reload();
            }, function(response) {
                Notification.error("Could not send invite", response);
            });
        }

        $scope.search = function(query) {
            $scope.searching = true;
            $scope.users = [];
            $API.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                $scope.users = response.plain();
                $scope.searching = false;
            }, function() {
                $scope.searching = false;
            });
        }

        $scope.choose = function(user) {
            $scope.invite_email = user.email;
            $scope.users = [];
        }
    }
])

.controller('TeamSubscriptionController', ['$scope', '$state', '$rootScope', 'Session', '$API', 'plans', 'member', 'subscription_logs', 'Notification',
    function($scope, $state, $rootScope, Session, $API, plans, member, subscription_logs, Notification) {
        $scope.member = member;

        if (member.subscription) {
            $scope.current_plan = _.findWhere(plans, { id: member.subscription.plan_id });
            $scope.current_subscription = member.subscription;
            $scope.current_subscription.price = $scope.current_plan[$scope.current_subscription.interval + "_price"];
            $scope.current_subscription.user_id = member.id;

            $scope.renew = function() {
                $scope.pay($scope.current_subscription);
            }
        }

        $scope.plans = plans;
        $scope.subscription_logs = subscription_logs;

        $scope.pay = function(subscription) {
            $API.all('subscriptions').post(subscription)
                .then(function() {
                    Notification.success('All Done!', "Subscription was successful");
                    $state.reload();
                }, function(response) {
                    $rootScope.$broadcast('subscriptionDone', false);
                    Notification.error("Subscription failed", response);
                });
        }
    }
])
