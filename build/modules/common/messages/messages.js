angular.module('app.common')

.controller('MessagesController', ['$scope', '$rootScope', '$state', '$API', 'conversations', 'unread', 'ngDialog', 'LocalService',
    function($scope, $rootScope, $state, $API, conversations, unread, ngDialog, LocalService) {
        var _conversations = _.map(conversations, function(conversation) {
            var userId = $rootScope.User.id;
            if (conversation.last_receiver && (conversation.last_receiver.id == userId)) {
                conversation.receiver = conversation.last_sender;
            } else {
                conversation.receiver = conversation.last_receiver;
            }

            conversation.unread = _.filter(unread, function(message) {
                return message.conversation_id == conversation.id;
            });

            return conversation;
        });

        $scope.conversations = _.filter(_conversations, function(conversation) {
            return conversation.receiver;
        });

        $scope.clearUnread = function(conversation) {
            if (conversation.unread) {
                $rootScope.unread = _.without($rootScope.unread, conversation.unread);
                conversation.unread = [];
            }
        }

        $scope.isAttachment = function(message) {
            return message && message.startsWith("is_attachment");
        }

        // Clear unread on load route
        $API.all('messaging/mark_receipts_as_read').post();

        $scope.newConversation = function(user) {
            ngDialog.open({
                templateUrl: "NewConversation",
                controller: "NewConversationController",
                data: {
                    user: user,
                    conversations: $scope.conversations
                }
            }).closePromise.then(function(data) {
                var conversation = data.value;
                if (conversation == "$document") return;
                $state.reload().then(function() {
                    var prefix = $state.current.name.split(".")[0];
                    $state.go(prefix + '.messages.one', { id: conversation.id }, { reload: true, inherit: false });
                })
            });
        }

        if ($state.params.new && LocalService.get("workstation-new-conversation")) {
            var userToMessage = JSON.parse(LocalService.get("workstation-new-conversation"));
            var conversation = _.find($scope.conversations, function(conversation) {
                return conversation.receiver.id == userToMessage.id && conversation.receiver.email == userToMessage.email;
            });

            if (conversation) {
                var prefix = $state.current.name.split(".")[0];
                $state.go(prefix + '.messages.one', { id: conversation.id }, { reload: true, inherit: false });
            } else {
                $scope.newConversation(userToMessage);
            }

            LocalService.unset("workstation-new-conversation");
        }
    }
])

.controller('NewConversationController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.users = [];
        var userToMessage = $scope.ngDialogData.user;
        var conversations = $scope.ngDialogData.conversations;
        var userId = $rootScope.User.id;

        if (userToMessage) {
            $scope.compose = {
                recipient: userToMessage
            }
        }

        $scope.search = function(query) {
            $scope.searching = true;
            $API.all('users/search').getList({
                query: query,
                page: 1
            }).then(function(response) {
                populateConversations(response.plain());
                $scope.searching = false;
            }, function() {
                $scope.searching = false;
            });
        }

        function populateConversations(response) {
            $scope.users = _.map(response, function(user) {
                var existingConversation = _.find(conversations, function(conversation) {
                    return conversation.receiver.id == user.id;
                });

                if (existingConversation) {
                    user.conversation = existingConversation;
                }

                return user;
            })
        }

        $scope.converseWith = function(user) {
            if (user.conversation) {
                $scope.closeThisDialog(user.conversation);
            } else {
                $scope.compose = {
                    recipient: user
                }
            }
        }

        $scope.start = function(body) {
            var recipient_type = $scope.compose.recipient.is_admin ? "Admin" : "User";
            $API.all('messaging/send_message').post({
                subject: 'Hi',
                message: body,
                recipient_id: $scope.compose.recipient.id,
                recipient_type: recipient_type
            }).then(function(conversation) {
                $scope.closeThisDialog(conversation);
            }, function(response) {
                $rootScope.$broadcast('conversationStarted', false);
                Notification.error("Could not start conversation", response);
            });
        }
    }
])

.controller('ConversationController', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'conversations', 'receipts', 'ngDialog',
    function($scope, $rootScope, $state, $API, Notification, conversations, receipts, ngDialog) {
        $scope.receipts = _.map(receipts, function(receipt) {
            if (receipt.message.body.startsWith("is_attachment")) {
                var attachment = receipt.message.body.substring(14, receipt.message.body.length);
                receipt.attachment = JSON.parse(attachment);
            } else {
                receipt.attachment = null;
            }

            return receipt;
        });

        $scope.conversation = _.find(conversations, function(c) {
            return c.id == $state.params.id;
        });

        if (!$scope.conversation) $state.go('^');

        $scope.send = function(message) {
            if ($scope.sending_message) return;

            $scope.sending_message = true;
            $API.all('messaging/reply_to_conversation/' + $state.params.id).post({
                message: message
            }).then(function(conversation) {
                $state.reload();
            }, function(response) {
                $scope.sending_message = false;
                $rootScope.$broadcast('sendMessage', false);
                Notification.error("Could not start conversation", response);
            });
        }

        $scope.$watch('attachment', function(newFile, oldFile) {
            if (!newFile || newFile == oldFile) return;

            var fileSize = newFile.size / 1024 / 1024;
            if (fileSize > 5) {
                Notification.error("File limit exceeded", "Please upload a file below 5MB");
                $scope.attachment = null;
                return;
            };

            ngDialog.open({
                templateUrl: "UploadAttachment",
                controller: "UploadAttachment",
                data: {
                    file: newFile
                }
            })
        });

        $scope.delete = function(r) {
            $API.one('messaging/receipts', r.id).remove().then(function() {
                $state.reload();
                Notification.success("Deleted", "Your message was successfully deleted");
            }, function(response) {
                $rootScope.$broadcast('deletedMessage', false);
                Notification.error("Could not delete message", response);
            });
        }

        $scope.leaveConversation = function() {
            $API.one('messaging', $state.params.id).remove().then(function() {
                $state.reload();
                Notification.success("All done", "You have been removed from this conversation");
            })
        }
    }
])

.controller('UploadAttachment', ['$scope', '$rootScope', '$state', '$API', 'Notification', 'FileHelper',
    function($scope, $rootScope, $state, $API, Notification, FileHelper) {
        $scope.file = $scope.ngDialogData.file;

        FileHelper.upload($scope.file).success(function(response) {
            sendUpload(response);
        }, function(response) {
            $rootScope.$broadcast('uploadedAttachment', false);
            Notification.error("Could not upload file", response);
        });

        var sendUpload = function(upload) {
            var message = "is_attachment:" + JSON.stringify(upload);
            $API.all('messaging/reply_to_conversation/' + $state.params.id).post({
                message: message
            }).then(function(conversation) {
                $state.reload().then(function() {
                    $scope.closeThisDialog();
                });
            }, function(response) {
                $rootScope.$broadcast('uploadedAttachment', false);
                Notification.error("Could not upload file", response);
            });
        }
    }
])
