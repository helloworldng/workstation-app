angular.module('app.common')

.controller('EventsController', ['$scope', '$rootScope', '$state', '$API', 'CalendarHelper', 'ngDialog', 'events',
    function($scope, $rootScope, $state, $API, CalendarHelper, ngDialog, events) {
        $scope.events = events.plain();

        $scope.newEvent = function() {
            ngDialog.open({
                templateUrl: "NewEvent",
                controller: "NewEventController",
                showClose: true,                
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.editEvent = function(event) {
            ngDialog.open({
                templateUrl: "EditEvent",
                controller: "EditEventController",
                data: {
                    event: event
                }
            });
        }
    }
])

.controller('NewEventController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.event = {};

        $scope.save = function() {
            $API.all('events').post($scope.event)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been added");
                    })
                }, function(response) {
                    $rootScope.$broadcast('eventCreated', false);
                    Notification.error("Could not create event", response);
                });
        }
    }
])

.controller('CommonEventController', ['$scope', '$rootScope', 'FileHelper', function($scope, $rootScope, FileHelper) {
	$scope.locations = $rootScope.locations;

    $scope.$watch('event_image', function(newFile, oldFile) {
        if (newFile && newFile != oldFile) {
            $scope.uploading = true;
            FileHelper.upload(newFile).success(function(response) {
                $scope.uploading = false;
                $scope.event.image_url = response.url;
            });
        }
    });

    $scope.remove = function() {
        $scope.event_image = null;
        $scope.event.image_url = null;
    }
}])

.controller('EditEventController', ['$scope', '$rootScope', '$state', '$API', 'Notification',
    function($scope, $rootScope, $state, $API, Notification) {
        $scope.event = $scope.ngDialogData.event;

        $scope.save = function() {
            $API.one('events', $scope.event.id).customPUT($scope.event)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been updated");
                    });
                }, function(response) {
                    $scope.$broadcast('eventUpdated', false);
                    Notification.error("Could not update event", response);
                });
        }

        $scope.delete = function() {
            $API.one('events', $scope.event.id).remove()
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Event has been deleted");
                    });
                }, function(response) {
                    $scope.$broadcast('eventDeleted', false);
                    Notification.error("Could not delete event", response);
                });
        }
    }
])
