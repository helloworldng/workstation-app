angular.module('app.common')

.controller('BookingsController', ['$scope', '$rootScope', '$state', '$API', 'CalendarHelper', 'ngDialog', 'Notification', 'bookings',
    function($scope, $rootScope, $state, $API, CalendarHelper, ngDialog, Notification, bookings) {

        function formatBooking(booking) {
            var grace_period = $rootScope.User.isAdmin ? '60' : '30';

            booking.start = moment(booking.start_time).local();
            booking.end = moment(booking.end_time).local();
            booking.past = moment().isAfter(booking.end);
            booking.active = moment().isBetween(booking.start, booking.end);
            booking.self = booking.booker.uid == $rootScope.User.uid && booking.booker.id == $rootScope.User.id;
            booking.editable = moment().add(grace_period, 'minutes').isBefore(booking.start);
            booking.start_string = moment(booking.start).format("ddd, MMM Do, h:mm a");
            booking.end_string = moment(booking.end).format("ddd, MMM Do, h:mm a");

            if ($rootScope.User.isAdmin) {
                booking.self = true;
            }

            if (!$rootScope.User.isAdmin && !booking.self) {
                var booker_name = booking.booker.first_name + " " + booking.booker.last_name;
                booking.title = "Booked by " + booker_name;
            }

            return booking;
        }

        bookings = _.map(bookings, function(booking) {
            return formatBooking(booking);
        });

        $scope.bookings = bookings;

        $scope.newBooking = function() {
            ngDialog.open({
                templateUrl: "NewBooking",
                controller: "NewBookingController",
                closeByEscape: false,
                closeByNavigation: false,
                closeByDocument: false
            });
        }

        $scope.viewBooking = function(booking, jsEvent, view) {
            $scope.booking = booking;
            if (view.name == 'basicWeek') {
                CalendarHelper.renderTooltip(jsEvent);
            }
        }

        $scope.loadBooking = function(booking, jsEvent, view) {
            $scope.booking = formatBooking(booking);
            ngDialog.open({
                templateUrl: "ViewBooking",
                scope: $scope
            });
        }

        $scope.editBooking = function(booking) {
            $scope.booking = formatBooking(booking);
            if ($scope.booking.editable) {
                ngDialog.open({
                    templateUrl: "EditBooking",
                    controller: "EditBookingController",
                    data: {
                        booking: $scope.booking
                    }
                });
            } else {
                ngDialog.open({
                    templateUrl: "BookingNotEditable"
                });
            }
        }

        $scope.deleteBooking = function(booking) {
            ngDialog.openConfirm({
                templateUrl: "DeleteBooking",
                controller: ['$scope', function($scope) {
                    $scope.booking = booking;
                }]
            }).then(function(booking) {
                var deleteAPI;

                if (booking.delete_reason) {
                    deleteAPI = $API.one('users', booking.booker.id).all('delete_booking').post({ reason: booking.delete_reason, booking_id: booking.id });
                } else {
                    deleteAPI = $API.one('bookings', booking.id).remove();
                }

                deleteAPI.then(function() {
                    Notification.success("All Done!", "Booking has been deleted");
                    $state.reload();
                }, function(response) {
                    $scope.$broadcast('bookingDeleted', false);
                    Notification.error("Could not delete booking", response);
                });
            });
        }

        $scope.uiConfig = {
            calendar: {
                height: 600,
                header: {
                    center: 'title',
                    left: 'prev,next today',
                    right: 'basicWeek listWeek',
                },
                defaultView: 'basicWeek',
                eventClick: $scope.loadBooking,
                eventMouseover: $scope.viewBooking
            }
        };

        $scope.selfBookings = _.filter($scope.bookings, 'self');
        $scope.otherBookings = _.reject($scope.bookings, 'self');

        $scope.activeBookings = _.filter($scope.selfBookings, 'active');
        var inactiveBookings = _.reject($scope.selfBookings, 'active');

        $scope.pastBookings = _.filter(inactiveBookings, 'past');
        $scope.futureBookings = _.reject(inactiveBookings, 'past');

        $scope.bookingsSources = [{
            events: $scope.otherBookings,
            backgroundColor: '#ccc',
            borderColor: '#c0c0c0'
        }, {
            events: $scope.activeBookings,
            backgroundColor: '#F05050',
            borderColor: '#F05050'
        }, {
            events: $scope.pastBookings,
            backgroundColor: '#ccc',
            borderColor: '#c0c0c0'
        }, {
            events: $scope.futureBookings
        }];
    }
])

.controller('BookingRoomsController', ['$scope', '$rootScope', '$interval', '$API', function($scope, $rootScope, $interval, $API) {
    $API.all('meeting_rooms').getList({
        location_id: $rootScope.User.home_location_id
    }).then(function(response) {
        $scope.rooms = response.plain();
        checkAvailableRooms();
        $interval(checkAvailableRooms, 2000);
    });

    function checkAvailableRooms() {
        _.each($scope.rooms, function(room) {
            room.meetings = [];

            _.each($scope.bookings, function(booking) {
                booking.active = moment().isBetween(booking.start, booking.end);

                if (booking.active && booking.meeting_room.id == room.id) {
                    room.meetings.push(booking);
                }
            });

            if (room.meetings.length) {
                room.busy = room.meetings[0];
            } else {
                room.busy = false;
            }
        });
    }
}])

.controller('BookingTimepickerHelper', ['$scope', 'DatepickerHelper', function($scope, DatepickerHelper) {
    $scope.onTimeSet = function(newDate, oldDate) {
        var inFuture = moment(newDate).isAfter(moment());
        if (inFuture) {
            calculateEndDates(newDate);
            DatepickerHelper.triggerClick();
        } else {
            $scope.booking.start_time = null;
            Notification.error("Past date", "Please choose a date in the future");
        }
    }

    $scope.calculateEndDates = calculateEndDates;

    function calculateEndDates(date) {
        var _date = moment(date);
        // var endOfDay = moment(date).startOf('day').add(18, 'hours');
        var endOfDay = moment(date).endOf('day');

        var possibleEndDates = [];
        var bookingLength = 0;

        while (_date.isBefore(endOfDay)) {
            bookingLength++;

            _date.add(1, 'hour');
            possibleEndDates.push({
                value: _date.format('h:mm A'),
                length: bookingLength
            });
        }

        $scope.possibleEndDates = possibleEndDates;
    }

    $scope.beforeRender = function($view, $dates, $leftDate, $upDate, $rightDate) {
        _.each($dates, function($this) {
            var _date = moment($this.localDateValue());

            if ($view == "day") {
                var today = moment();
                var _dayInThePast = today.isAfter(_date, 'day');
                $this.selectable = !_dayInThePast;
            }

            if ($view == "hour") {
                var _hour = parseInt(_date.format('H'));
                var _isWorkingHour = true; // _hour >= 8 && _hour < 18;
                var _isInTheFuture = _date.isAfter(moment());

                $this.selectable = _isWorkingHour && _isInTheFuture;
            }
        })
    }
}])

.controller('NewBookingController', ['$scope', '$rootScope', '$state', '$window', '$API', 'Notification', 'ngDialog', 'Session',
    function($scope, $rootScope, $state, $window, $API, Notification, ngDialog, Session) {
        $API.all('meeting_rooms').getList({
            location_id: $rootScope.User.home_location_id
        }).then(function(response) {
            $scope.rooms = response.plain();
        });

        $scope.booking = {};

        $scope.save = function() {
            $API.all('bookings').post($scope.booking)
                .then(Session.update)
                .then(function() {
                    Notification.success("All Done!", "Booking has been added");
                    $state.reload();
                    $scope.closeThisDialog();
                })
                .catch(function(response) {
                    $rootScope.$broadcast('bookingCreated', false);
                    Notification.error("Could not create booking", response);
                });
        }

        $scope.hasEnoughTokens = function() {
            var user = $rootScope.User;

            if (!$scope.booking.meeting_room_id || !$scope.booking.length) return false;

            var meeting_room = _.findWhere($scope.rooms, { id: $scope.booking.meeting_room_id });
            $scope.booking.tokens_needed = meeting_room.tokens_per_hour * $scope.booking.length;
            $scope.booking.token_deficit = $scope.booking.tokens_needed - user.token_allowance;

            return user.token_allowance >= $scope.booking.tokens_needed;
        }

        $scope.buyTokens = function() {
            ngDialog.openConfirm({
                templateUrl: "/modules/user/account/modals/buy-tokens.html",
                resolve: {
                    prices: ['$API', function($API) {
                        return $API.all('add_ons/system').getList();
                    }]
                },
                controller: "BuyTokensController",
                data: {
                    amount: $scope.booking.token_deficit
                }
            });
        }
    }
])

.controller('EditBookingController', ['$scope', '$rootScope', '$state', 'Session', '$API', 'Notification', 'ngDialog',
    function($scope, $rootScope, $state, Session, $API, Notification, ngDialog) {
        $API.all('meeting_rooms').getList({
            location_id: $rootScope.User.home_location_id
        }).then(function(response) {
            $scope.rooms = response.plain();
        });

        $scope.booking = $scope.ngDialogData.booking;
        $scope.booking.active = moment().isBetween($scope.booking.start_date, $scope.booking.end_date);

        $scope.save = function() {
            var payload = _.pick($scope.booking, ['title', 'start_time', 'length', 'meeting_room_id']);
            if (!$rootScope.User.isAdmin) payload = _.omit(payload, ['length', 'meeting_room_id']);

            $API.one('bookings', $scope.booking.id).customPUT(payload)
                .then(Session.update)
                .then(function() {
                    $state.reload().then(function() {
                        $scope.closeThisDialog();
                        Notification.success("All Done!", "Booking has been updated");
                    });
                })
                .catch(function(response) {
                    $scope.$broadcast('bookingUpdated', false);
                    Notification.error("Could not update booking", response);
                });
        }
    }
])
