module.exports = function(grunt) {

    grunt.config.set('string-replace', {
        options: {
            replacements: [{
                pattern: /<script.*?src="assets\/js\/concat\/production(.*?)"><\/script>/g,
                replacement: '<script src="assets/js/concat/production.js"></script>'
            }, {
                pattern: /<script.*?src="assets\/js\/concat\/dependencies(.*?)"><\/script>/g,
                replacement: '<script src="assets/js/concat/dependencies.js"></script>'
            }, {
                pattern: /<link.*?href="assets\/css\/concat\/production(.*?)">/g,
                replacement: '<link href="assets/css/concat/production.css" type="text/css">'
            }]
        },
        'build/index.html': ['./build/index.html'],
    });

    grunt.loadNpmTasks('grunt-string-replace');
};
