module.exports = function(grunt) {

    grunt.config.set('cacheBust', {
        options: {
            baseDir: './build/',
            assets: ['assets/js/concat/production.js', 'assets/js/concat/dependencies.js', 'assets/css/concat/production.css'],
            length: 16,
            deleteOriginals: true
        },
        src: ['./build/index.html']
    });

    grunt.loadNpmTasks('grunt-cache-bust');
};
